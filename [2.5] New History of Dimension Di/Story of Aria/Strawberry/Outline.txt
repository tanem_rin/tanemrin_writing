- Hook with rockets flaring up the night sky
- The imperial rocket forces razing a town to the ground

####

- Introduce Strawberry as a cute girl
- Show her relationship with the Princess Cecilia Aachenbach. 
- Constantly calls her Empress, but always rebuked, saying she refuses to take the title, and when she reunites Aria, she will be stepping down from the throne.
- She is relied upon as a trused friend and aide.
- The civil war is ongoing with Ecclesia

- The Grunswald upper command is still stuck in their ways, unwilling to fight a battle of annihilation against Ecclesia, which irks Strawberry. Time and time again, she fails to convince them to be more decisive in their actions, to not pay any heed to casualties or damage - only victory.
- She is, repeadly, rebuffed, however.

>Later: Reveal that Strawberry is, as her name suggests not truly Arian, but her most hated - Lilican. She even wanted to dye her hair and change her name to Erdbeere, the Arian form of her name, but is stopped only by her best friend - Cecilia, saying that she liked Strawberry's name, and that no matter what, Strawberry will always be her best friend, so she doesn't need to change anything about herself.

- At one point, she is smeared by one of the officers, calling out her hypocrisy, on one hand calling death for all non-Arians, while being an untermensch mutt, herself, and obviously so.
- This hurts her, really bad.
- Draws a gun on the officer and very nearly kills her.
- Is put into temporary detention.
- Cecilia visits her in her cell, and asks if she wants to be let out.
- Scene dialog:

"I don't know."

Lia sighed. "Well, at least you know you messed up by pulling that gun out."

Strawberry gets more depressed.

Lia then opens the cell.

"W-what are you...?"

"Hm?" Lia looks at her like it's the most natural thing in the world, while she locks the cell behind her.

"Eh? W-what are you doing, Lia!?"

"I'm joining you, duh."

"Joining-" She shook her head. "You can't be seen here!"

"Why not? Are you telling me to leave my friend when she's feeling lonely?"

"Y-you're supposed to be the Princess of all Aria...!" she said, blushing, "People shouldn't see you inside a cell like this..."

Cecilia snickered.

"I told you, I don't care about any of that. Being expected to act like a princess, all that stuffy Chivalry stuff. What worth are they when it means I can't even comfort my own friend whenever I want to?"

"I-" her voice trembled.

"You were hurt, right?"

"..."

Lia then draped her arm over Strawberry's shoulder, bringing their heads closer together. "It's okay."

She sobbed.

"I don't really know anything about strategy and all that big stuff..."

"Mm."

"Pfft. So you're agreeing with that?" She chuckled. "But you know, I get this feeling that this while war thing is a lot bigger than myself. Of course I trust you. I know full well just how much smarter you are than me."

"Mm."

"But those guys are also on our side."

Strawberry frowned.

"They're also doing what they think is right. We're all doing our best for our country - for our people."

Bedgudgingly, Strawberry accepted. "...Mm."

"Listen-"

She glanced, for the first time, the gentle expression on her best friend's face.

"-I can't let you dictate how the General Command will execute our strategy. But what if we create your own section?"

Her eyes flickered. "What?"

"Mm! It'll be an independent force, separate from General Command, for you to lead. If there's anything I learned from the Dragoon Guards, it's that you can't fight change. You remember Old Slippy?"

She winced. "I remember you being terrible with nicknames."

"Hehehe! Yeah, Sleipnir was my partner. We basically grew up together. I raised him, fed him and trained him to be my steed for the dragoons. But as we grew up, the world began to change. And with it, the guard itself."

"You mean the steel dragoons."

"Mm." She nodded. "The guard was slowly being motorized, and one of the first to go were our horse cavalry. I hated it at first. I thought, how could a lifeless machine replace my long-time partner? But soon after eastern upheaval began, I saw the other companies returning for rotation and I realized - Old Slippy was going to die if we deploy. So I gave up and took up my Steel Dragoon - Slippy 2!"

Strawberry sighed. "What naming sense..."

"Cool, isn't it!? I added some side skirts here, and some gun mounts here, yeah?"

She averted her eyes. "Erm... yeeeeah..."

She didn't notice it, but her mind had cleared up and she wasn't feeling so bad anymore.

"Anyway, what was that story for?"

"Ueh?"

Her brows knotted. "...huh?"

"Ah! Right! I completely forgot!"

"Haaah..."

"Anyway, the point was, General Command still probably hasn't realized the advantage of your new strategy. So why not put it to the test? Tell me what you need and I'll see if I can requisition then for you."

"Y-you... really believe me? You would really do that!?"

"Of course!" She said with a wide grin. "If I can't believe my best friend, who can I believe?"

Strawberry tears up.

"Of course, it'll be a lot of work... you'll basically by competing with General Command at the start-"

"I'll do it!" She suddenly cried.

"-Woah! Hehe, that's the spirit!"

- As soon as she is released from detention, Strawberry gets to work.
- She is given a small detchment of artillery engineers, who she co-opts into creating new and terrible weapons - rocket artillery and chemical shells.
- She also learns from After-action reports in the Eastern-Imagica theater and researched the viability of flamethrower troops against fortifications and Durite armored infantry.
- With her advise, Cecilia also orders the construction of more special ordinance factories to supply the Arian Army.
- She originally planned for mage companies in her unit, but obstruction from General Command blocked it out.
- She is furious, but also predicted it. 
- Mages were powerful strategic and tactical tools and being denied their assistance was a heavy blow. So she also began research of anti-mage tactics, and came up with elite infiltration and observation corps "Stormtroopers" that could detect and eliminate mage Ateliers, or call upon accurate artillery to their position.
- Balloon recon was too distant and were easy prey for Magic spells.
- Planes were also less viable due to many homing spells (MCLOS)

- Her test comes at the junction of (____) city and (____) city, which was being stubbornly held by Ecclesian troops, with Durite Armored Royal Guard troops on reserve.

- She only has a force size of a battalion or so, with some Steel Dragoons attached, courtesy of Cecilia herself.
- She is nervous.
- The battle could forever make or break her.
- The town had long been under repeated assault, but good terrain coupled with dogged defenders and remaining civilians have defeated all of them.

- The attack commences an unorthodox leaflet drop.
- It ordered all civilians willing to rejoin the Arian Empire but unwilling to fight, to evacuate to Arian lines. It ordered the Ecclesian forces to honor the wishes of these civilians or be branded war criminals. 
- Naturally no civilian took the offer.
- Infiltrators were able to smuggle in some arms for a suicide mission, calling for the expulsion the Ecclesian traitors with some few patriotic Arians in the town. They timed the attack along a predicted shelling of the outside curtain, passing it off as a general assault that would take the town. They as well egged some civilians into escaping the warzone back to Aria or Ecclesia. Either way, they just herded them toward known Ecclesian checkpoints for them to get slaughtered (after firing at the checkpoints from inside).
- They would get clandestine pictures of these scenes.

- This is LONG before her section even deployed
- By the time her forces were setting up for the siege, "GAS THE ECCLESIANS" were already a meme.


- Armed with these pictures, Strawberry would send it to the presses and decry the inhumane warcrimes of the Ecclesian garrison in (____) town.
- The headline wrote - "Patriotic Arians in (___) massacred in Rebel purge"
- The article continued, the ethnic cleansing continued well into the night as Arian lines outside of (____) town reported gunfire in the distance.
- Imperial Chief of Staff Strawberry Einkelhauss strongly condemned the brutal act of violence, and vowed swift vengeance for the slaughtered hundreds, and criticizing the General Command for their weak approach, allowing the tragedy to happen.
- She also snuck in a scathing description of the Ecclesian scum, suggesting that the Arian people will no longer suffer any such insult, and any and all methods of exterminating the treasonous filth will be taken.

- She thus gains *official* jurisdiction over the battle, transferring it over to the newly formed Imperial Independent Siege Battalion - IISB.

- Therefore, all that was left were treasonous Ecclessians. There were no true Arians left in the town.
 
