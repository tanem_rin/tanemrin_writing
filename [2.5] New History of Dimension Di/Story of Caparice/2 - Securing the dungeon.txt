- Hoshino is interested in pre-cataclysm history.
- They find a research lab somewhere in the snowfields outside of Caparice.
- Hoshino likes.
- Alte can't let her go without her.
- Alte and the witches raid the facility and kill everything outside.
- Hoshino (Digger) and the Mechanized (<callsign>) enter the lab
- Some monsters are still inside the broken husk but are easily dispatched by the Mechanized's close quarters weaponry.
- Hoshino and the other Sappers help out by bringing out siege equipment like Sparklers (volatile flamethrowers that crackle instead of burn).
- They find the Liber Damnatus inside, and Hoshino's soul is grafted onto Marigold's. Hoshino can now use witch powers.

- The yuri are entirely uninterested in witchcraft, and even hoshino only studies them to find the root of their existence.






Oh SHIT: Hoshino doesn't really know what the book is for, and on her way out slips it inside her 4D pocket.

Except doing so allowed her to know everything about the object, thus melding her soul with the power and memories of Marigold, which was somehow stored there.



"Exploring, exporing!" A droopy-eyed girl sang to herself while bobbing her head from side to side.

"Is it going to be safe?" Asked the girl from across the cabin.

At that moment, the deafening roar of missiles rippled overhead, followed by distant explosions.

What followed was a formation of Airborne specialists, callsign Witch-3 and Witch-4 according to radio chatter.

She looked into the sky (happily/longlingly/nostalgically). Though she couldn't see her, she knew that her beloved was flying right now. Then, pulling back her gaze down to the girl across she said, (lightly/jokingly/light-heartedly), "It'll be fine, won't it?"

####

They soon arrived at their destination.

As soon as the back ramp dropped, a strong, pungent smell invaded the tight space of the APC.

"Uwah...! I really can't handle that smell!" (The girl on the other side) said, "Geez, I joined the Sappers so I don't have to smell this..."

"Haha. Sorry." (Hoshino - not introduced naturally yet) said.

They, along with their escorts - two girls clad in really thick coats and ballistic helmets and goggles, approached the site.

<Explain research facility here. It wsa a small igloo-like building, though from its shape, there must have been a solid structure, likely rectangular in nature, based on the shape of the snow pile that collected on top of it.>

"Did you know? Command found this place because packs of these beasts were seen weirdly congregating around them." said a girl with the thick goggles.

"Heh... I'm nervous about what we'll find inside now..." said another, tiny girl with the scarf almost up her her nose.

The radio then crackled to life <- so fucking overused at this point. I need more radio metaphors.

"Witch-3 to Digger-5, do you copy?"

/Oh, that's me./

Hoshino thus reached to the reciever on her chest and placed it against her mouth. "Yep. Good afternoon, Alte."

"Ngk- Ho- err, Digger-5, a-at least use my callsign..."

"Isn't it fine? The battle's over?"

She then heard a sigh over the radio. "Anyway, what is it?" she asked, "Wishing me luck?"

"Er- uhm..." After a long silence, a shy mumble came through the radio. "Yeah... take care... Hoshino."

The girls around her in the site, also with radios of their own, (beamed their warm smiles towards her), and she, to them.

/Wah. My Alte is just so cute... that's what makes it so fun to tease her.../

Finally, she answered, "Thanks, Alte. See you later." 

And so, the team dove into the (research facility - not yet explained.)

#####

They first cleared a hallway with smaller monsters, the two girls of the Mechanized took point.

The spitting beasts failed to even get close before they were peppered with automatic gunfire.

In one ambush, the tiny girl was saved by her oversized coat, which warded the acidic spittle from her body. Although it melted the coat.

"Frett!" (This was Rumi by the way)

She helped the shaken girl back to her feet.

Thet continued on.

<Labyrinthine>

Next, they came upon a large room with many monsters. I don't think we can take that many at once.

So, Hoshino and (friend) took out a Sparkler tank and sprayed exploding liquid inside, killing most of them in one fell swoop. The remainders were picked off by the mechanized.

They continued.

<Boss battle time?>

A bit dangerous, some injuries, but injecting pure red strain (




Red strain yuri name candidates: Mogra, Schee, Schnee, Moruu