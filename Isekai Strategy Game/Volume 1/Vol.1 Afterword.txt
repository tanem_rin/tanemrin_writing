Vol.1 Afterword

First of all, I want to thank you, dear readers, for giving this story a shot and sticking with it up to this point. I truly am humbled that so many people now look forward to a new chapter every week. Thank you very much. m(_ _ m)

I first started Isekai Strategy Game a few months ago, exactly the 1st of January of this year. It was just another hobby story at that point, written mostly out of boredom because I've exhausted all of the isekai anime and manga that I could get my hands on. (￣▽￣*)ゞ Boy, when I peek into my backups and see those first few chapters in their truly raw form, I still go red. But eventually, I came to like the story and the characters I've come up with. Or, actually, it's the other way around. I came to like the characters, and wanted to make a better story for them. Thus, the rewrite happened. (×﹏×) 

And here we are, 10 chapters published and a lot more more ready to go.

I must say, it was a pretty enlightening experience to have written even just to this point. When I consider how impatient I was at the start of this year, thinking to myself, "Agh, why can't they release faster!?" or "Why do I have to wait years in between seasons of this or that (isekai) anime or manga (you can probably guess which ones I mean)!?", it almost seems rather silly. After all, I've just spent nearly half the year publishing a single volume, which, if I try and imagine it as an anime... would probably total 1, maybe 2 episodes. 

Not that it does anything to satisfy the craving, though... Gimme the next season now! ٩(ఠ益ఠ)۶

Well, well, there's a bit of a disparity in perspectives between the producer and consumer, isn't there? Even I, sometimes, when I'm getting stuck on a bit that's tricky to write, I come to wish that someone would just tell me what happens next... _(:3 」∠)_ 

From the outset, my aim was to merge the two disparate worlds of fantasy RPGs, depicted in the story as the VRMMORPG, Tales of the Empyrean, with the multitudes of strategy games I've played over the years. What I came up with was a sort of hybrid RPG/RTS, much like Mount and Blade, where the player, that is, Sylphia, acts as the commander of an army. At the moment, Rika and her motley crew of guards took that role. In fact I took a lot of inspiration not just from that game, but also the Total War series, on how to present the Battle for Ealdton. (It was really fun to choreograph that battle, by the way!) But everything up to this point was merely akin to a tutorial. Sylphia was fumbling around with no idea of what to do, and without even knowing what powers she had. She even had an invisible hand, in the form of Alphonso and Aster, helping her out from the sidelines. But from here on out, it's the real deal!

And that's all I wanted to share. Thanks again, dear reader, and I hope you look forward to the next volume! I had a blast writing it and I hope you will like reading it as well! Sylphia goes to the adventurer's guild! A staple of all Isekai/Fantasy RPGs, right!? It just //has// to be there! But I have a bunch of fun surprises waiting ahead! Look forward to it! o(≧▽≦)o

- Rin Tanemaki

Note:
I don't know if it'll catch on, but I'd like to make the afterword something of a way to communicate with the readers every once in a while.

As such, I'd like to invite your questions on the comments down below! Please ask me if there's anything that confused you in the story so far, if I missed something, maybe about the schedule, what ever you want. Sometimes, I leave things intentionally vague the moment I introduce them since they get used later in the story. For questions that will /eventually/ get answered by a future chapter, though, I'll just say so. So don't worry, I won't spoil my own story for you! 

Your questions will truly help me a lot, especially later on, as the game- err, story world expands, and it becomes harder and harder for me to keep track of every loose strand I keep around.

Also, I'd like to take this time to thank those who gave such high ratings for this story! I'll try to make it so that it stays worthy of your praise!

That, really this time, is it!

Please look forward to the two extra chapters coming tomorrow and the day after, plus the start of the second volume this Monday!

Yay! ∠( 'ω')／