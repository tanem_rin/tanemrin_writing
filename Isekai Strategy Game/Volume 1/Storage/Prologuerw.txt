Prologue Chapter

All at once, four thousand men shouted out a bellowing warcry. Metal clanged against metal in a dizzying dissonance as the horde charged across the open field, eager to meet the line of pikes pointed against them.

Two forces met in a narrow stretch of flatland between two dense forests, one occupying the single wooden bridge crossing a wide river, and the other, the hilly terrain on the other side of the flatland.

"Hold your ground!" a small girl shouted. Through magic power, she propagated her voice like a shockwave that spread in the air. While deafeningly loud, her voice carried the tone of a child. 

In response, her soldiers, numbering at less than 1000 people, gave a resounding shout. 

"Crossbows, volley fire!" she yelled.

As soon as she gave her order, the crossbowmen, a few hundred feet in front of her, released a shower of bolts right into the charging soldiers. The moment of impact was clear; all at once, dozens of soldiers at the very front of the charge fell down and promptly disappeared beneath the footfalls of those behind them. 

"Crossbows, retreat!

Beside the girl stood her adjutant, a young woman that stood almost twice the girl's height. She held her hand to her chest as she looked over the battlefield.

The tiny girl turned to her and screamed, "Hey! Are you doing your job!? Give me status updates!"

The adjutant flinched and stammered, "Y-yes, ma'am!"

She closed her eyes and focused her thoughts into her eyes. /Remote sight./ She imagined herself soaring in the skies and looking straight down at the battlefield down below. Soon, she felt a warm glow emanating first from her chest, that gradually travelled up into her eyes. The next thing she knew, she was imagining the battle play out right before her. The soldiers down below seemed like ants from where she stood.

The girl yelled again, "Well, what's happening!?"

"A-ahh! Please give me a moment!" 

The main bulk of the enemy army, a massive formation of three or four thousand heavily armored soldiers charged straight for their diffused formation of three 100-man companies of pikemen, arranged in a triangle. Behind their pikemen, two companies of crossbowmen ran back and reformed their lines as quickly as they could. Lastly, at the back of the enemy's formation, the line of enemy archers, possibly 1000 strong started to line up their bows.

"Ah! Miss General," her eyes popped wide open, "the enemy is starting a volley!"

"Alright." The little girl, despite her stature answered to the title of General. She stepped forward, glancing behind her for a just moment to say, "Stay back."

"Y-yes, ma'am..."

"Okay, partner," she said, as she pulled her fan from her belt, "it's showtime!" She then unfurled it with a single flick of her wrist.

The adjutant yelled, "They're firing!" She tried to follow the arrows as flew by, but despite her best efforts, they hardly appeared as anything more than thin, black streaks leaping across the air.

"Yeah, I see them!" The little girl then twisted her body, held her fan out behind her, and said, "Alright, give it all you've got!" As she said this, magic that resembled a black mist engulfed her fan.

"You got it!" a deep voice boomed, seemingly from out of nowhere.

"Aaaaaaaah!" With all of her might, the girl swung her fan forwards.

A sound of a thunderbolt roared in the cloudless sky. In an instant, the once verdant hill in front of them was stripped bare, the scattered blades of grass swept away by a violent gale.

The adjutant's mouth fell agape.

In the flatlands below, both friend and foe rooted themselves to the ground as the wind blasted right through them. Directly above them, the volley of arrows were swept away in its entirety, some even falling on the enemy's own soldiers down below.

"A... amazing..." The adjutant muttered beneath her breath.

Without missing a beat, the general sent out her next commands with her magic-amplified voice, "Pikemen, reform behind the crossbows! Crossbows fire another volley then retreat!" Her orders came at a breakneck pace. 

The adjutant had never seen anything like it before. She viewed the battlefield from above, and it seemed that each and every soldier were reacting to her general's commands as she spoke them. At such a speed, the battle progressed at a speed she was not prepared for. Ultimately, amid her shifting her focus and panning her sight according to the changes in the battlefield, she started getting dizzy.

/What's going on?/ She shook her head. /Nonetheless...!/ 

Enduring the pain, she closed her eyes once more and resumed her view over the battlefield. /Is there a way we can win, though? It seems she somehow has a magic that can thwart the enemy archers, but with just their heavy infantry, they outnumber our entire army by at least four-to-one./ Her eyes then turned to her general. /But despite that.../

The little girl stood at her the tip of her toes, at the very peak of the hill. As she looked out to the battlefield a massive grin stretched across her face.

/She looks like... she's enjoying herself!? Does she somehow think that we are going to win this battle? Or... does victory and defeat not mean anything to her?/

"Uhaa! The melee is starting!" The general squealed. She then turned around to her adjutant and pointed ecstatically to the battle. "Hey, are you getting this!?"

"Eh? Ah, yes!" 

The front line of the armored swordsmen crashed head-on into the wall of pikes.

"Aaaahahahaha! They're actually doing a frontal assault! They're totally underestimating us!" the little girl gleefully said. With her amplified voice, she then gave the next command, "Alright, Pikemen, fighting withdrawal!"

While the adjutant was observing the melee up-close, she noticed a shadow forming on the ground beneath their pikemen. At that very moment, the pikemen all reacted in sync, even without the orders being relayed by the company officers. 

She gasped. /I knew it... it was magic after all!/ The adjutant bit her lip. /So, we're withdrawing our line to gain some distance on the sword wielding heavy infantry...? Of course we'd want to do that, since our main line is using pikes but... we can't retreat forever!/ She opened her eyes for a moment and glanced at the battlefield from her own point of view. /There's probably just three hundred feet between them and us. Is she going to lead them all the way back here?/

Then, at the corner of her eyes, she notices the enemy archers advancing.

"Miss General, the enemy archers are closing the distance! Another volley is on the way!"

"Alright, keep at it!"

After a short while, the enemy archers released yet another volley of arrows. And again, it proved futile against the little general's powerful wind magic.

/We're doing quite well despite being outnumbered this badly.../

However, after a while, casualties started racking up on their side as well. No matter how well the pikemen fought, enemies were able to slowly close the distance, especially as space ran out for their withdrawal.

"Miss General," the adjutant pulled on the girl's sleeves, "we're running out of room! Our lines are just about a hundred feet from the base of the hill!"

"That's plenty," she replied, "keep going!"

/What!? We haven't even gone through a quarter of their forces and we've already withdrawn the line this far! How is that plenty!?/ She wanted to scream at her general, but feared too much the repercussions she would suffer. 

Backing away, she tried taking it off of her mind by focusing elsewhere. However, the moment she turned her eyes to the river far in the distance, she felt her heart drop.

/What is that!?/

She closed her eyes and, with her magic, took a closer look at the activity by the river crossing. To her distress, several columns of enemy soldiers had been crossing the river and were headed in their direction.

"E-e-enemy reinforcements!" she screamed. "Crossing the river now!"

"What, really?" For the first time, a concerned look appeared on the girl's face. "Composition?"

"U-uhm," The adjutant tried to control her breathing as she focused her mind on the incoming enemy soldiers. 

"Miss General," the adjutant said, "it's a mixed force, four... no, maybe six companies of heavy infantry, two companies of archers! It's size is  similar to the one we are engaging! I-I-I strongly urge that we retreat! There's no way we can win!"

"A-Ah, that's not happening." The little general scratched her head. 

"Eh!?" The adjutant's eyes popped wide open. "What are you saying!?"

"Well, you see," Sylphia tapped her fan rhythmically against her palm, "it's pretty complicated."

"Huh!? What is complicated, Miss General!? We've hopelessly outnumbered! We have no choice but to retreat!"

"Yeah, about that." she shrugged, "So, it turns out we were set-up to fight a losing battle from the get-go. What no one told you is that the other Lords are watching us right now. Oh, and King's here as well."

"Wa-wa-wa-wait! What!? But this flank was supposed to be safe from attack, right!? And why is His Majesty here!?"

"Ahaha," she cut her off, pointing her fan into her adjutant's face, "that was a lie, obviously. You see, I have my share of people who want to humiliate me, no matter what... And what better way to do that than to lose decisively to an invading force?"

The adjutant's heart dropped. "T-then... are we supposed to fight to the death!?"

"Hmm..." Sylphia crossed her arms and rolled her head around. "It would be better if you didn't have to die, of course."

"P-please take this more seriously, Miss General!"

The general blinked. "Uh... ah... yeah. Sorry."

"Ah-" Realizing what she had just done, the adjutant cupped her mouth and muttered, "I'm sorry, Miss General...!" She then prostrated herself in front of the little girl. "Please forgive my rudeness!"

"Ah, haha, don't worry about it." The girl lifted her up and gave her a vibrant smile. "You're right. I should take it seriously from the start, or we could lose, right?"

"U- uhm..."the adjutant stammered. "I...I don't think I said something so elegant..."

Sylphia grinned. "Don't worry about the details!"

"H-huh?" The adjutant shook her head. "A-anyway, you mean you knew we were going to be forced into a losing battle...?"

"Hmm..." The general rubbed her chin, "let's say it's thanks to a certain well-connected someone that I know."

The adjutant raised a brow at her reply. "I... I see. B-but if you already knew all of this, why are we still..."

"Hehe," the general pointed her fan into her adjutant's face, "since they already prepared such an elaborate plot to destroy me, isn't it rude if I did something boring like avoid it?"

"E-eh?" she cocked her head.

The girl spread her arms wide. "Wouldn't it be more grand if I instead fell into their trap, and then blew it all up from inside!?"

The adjutant's throat felt dry and cold sweat started collecting on her forehead. /I-is she insane!? Who would want to do such a thing!?/

"Anyway, listen up." The Sylphia raised her head and looked her adjutant in the eye. "You're here because I want to train you as a commander. So watch carefully what I do, and think about why I did it. Clear?"

"Y-yes, ma'am!" She stood in stiff salute. /That's right... I'm not just a battlefield observer anymore... I was recruited me to be her adjutant! Though.../ Her eyes glazed over. /With the amount of manual labor she made me do the past few days, I could have sworn I signed up to be construction worker instead.../

"Mm!" The little girl gave a big smile that radiated her warmth.

The adjutant smiled back. /Looking at her like this, she really does look like a small child... She's hardly as tall as my little sister, and she even wears the same hairstyle from time to time... It's heard to believe she's really a-/

"Now," The general placed her arms on her waist as she looked out into the battlefield, "let's get this party started!"

After a while, their front line were pushed so far back that both of them were in earshot of the screams of the melee.

"Miss General, distance is just over 50 feet!" The adjutant screamed, "We're already in arrow range!"

"Ohoo, you don't say!?" Sylphia turned around and faced her.

"Yes, ma'am! They're right up against our fortifications at the base of this very hill! We should retr-"

The adjutant froze.

A devilish grin formed on the general's lips. "Good. Watch closely." She tapped her fan hard on her palm, creating a resounding snap in the adjutant's ears. "A new era of warfare is about to dawn on Terra Nova."

Her words sent chills down her adjutant's spine. To say she was baffled was an understatement - for some reason, she felt mesmerized by her general's voice. She took a shallow breath, "Miss... Ge- Ngh!?" Amidst her attempt to speak, her body suddenly froze up, as if all of her muscles cramped up at once. /W-w-w-wha!?/

"Total control." A silky voice, similar to that of her general's seemed to whisper directly into her ear.

"What's..." she struggled to speak, with even her mouth seemingly resisting her every movement. "happening!?"

"You will do as I say." the voice said. 

Before she realized it, a thick black mist enveloped the entire battlefield. /No way... magic!? I've never heard of magic like this!/

"I want you to see this." she said, "View the battlefield."

Without even thinking about it, she immediately closed her eyes and obeyed as commanded. /Ggh!? She... has control of my power as well!?/

"I will retreat our forces behind the fortifications."

Without even needing to give out orders, their soldiers moved as the general said. With the heavy infantry in chase, they abandoned their lines and ran back as quickly as they could to the line of wooden palisades they had painstakingly built over the past week.

"Why do you think I broke our lines?"

/Why indeed? Now our forces are effectively routing. Even if they turned to face the enemy once more, they could not possibly reform their formation in time./ She tried opening her mouth and found that her body was not resisting at all. "I don't know... Miss General." she said.

"Hmph," she heard a low snicker. "What do you think those carts you pulled around were for?"

"The carts...?" she muttered, "I'm sorry, I don-"

She had not even finished answering when, in the middle of her vision, the reserve soldiers manning their fortifications sprung up. They pushed out the wooden panels built into the palisades, exposing the carts behind them. She then focused in on the carts. Each had a large metal tube affixed on top but they offered no clue as to their purpose. 

/What are these supposed to be away?/

She hardly had the time to wonder when she was blinded by an intense flash. 

"Gyaaah!" She covered her eyes as she fell to the ground..

A split second later, the adjutant felt the ground shake beaneath her, as several hard thumps erupted behind her. 

/Aaaah! sWhat's happening!? What's happening!?/

When she had finally recovered her remote vision, she was greeted by a scene of complete carnage. 

The enemy pursuit had ground to a halt. Their front line had fallen apart as most of the men leading the charge had been transformed into tattered corpses and disembodied limbs littering the hillside.

She felt sick to her stomach. "M-miss General...! What is this!?"

The adjutant was late to notice it, but her general had been laughing softly to herself for a while now. Then, ss if her joy had finally boiled over, she let out a fit of maniacal laughter; not through her voice in her adjutant's ears, but the voice from her actual mouth. 

"Ahaha, Ahahahahaha!" the little girl clutched her stomach. "Aaaaaahahahaaaa!"

"Miss..." the adjutant swallowed her breath, "General...!?"

"Do you see it, my dear!?" she screamed. "It's artillery!"

"A-artillery...!?"

"Yes! Cannons, to be precise." the girl turned around and paced towards her adjutant. "As of today, catapults and ballistas are obsolete."

She swallowed her breath.

"Look behind us."

She closed her eyes, scrolled her sight to the hills behind them. No more fortifications stood around this area. Instead, even more of the carts were lined up following the crest of each hill.

/C-cannons...!/

A thunderous roar emanated from the cannons as a black streak, hardly visible to the eye shot out from the tubes all at once. /That is...!?/ Not more than a second later, the ground quaked from the impact of several explosions.

/No way...!/

She scrolled back to the front line, and saw several holes peppered all over the enemy's armored spearhead. Again and again, multiple explosions shook the ground, each one leaving a gaping hole in the once monolithic formation of the enemy's heavy infantry. The sheer power of the cannons not only blunted the enemy's advance, but also scattered their infantry.

The adjutant could not but surrender herself to awe. "Miss General... what in the world is this weapon?"

"That, my dear," the vulgar voice whispered into her ear, "is just the beginning." 

She shuddered.

"Watch the front. It's time for a counter attack."

"Eh!? But General, the enemy still outnumbers us! If we switch to an attacking stance, won't we lose our advantage?"

The voice snickered.

The frontline fell to utter chaos. When the adjutant was finally able to focus on their attacking force, she saw a familiar face leading a charge of the reservists and a reformed column of pikemen.

"Miss General, isn't that your bodyguard!?" she screamed.

"Oh, I see you've found our speartip. Watch her closely."

/No way... even if the enemy's formation is broken right now, most of our forces are lightly armored! Now that our cannons have stopped firing to give space for our assault, the enemy is free to regroup. And if they do.../

Beneath a flurry of crossbow fire from the palisades, the general's bodyguard dashed forward with her men in tow. Anticipating this, the enemy archers started drawing their bows.

"Enemy archers!" she screamed, "Another volley!"

"Aah! How annoying!" The general said with her real voice. "Partner, you're up!" she then threw her fan up in the air, "keep a good wind going behind us!"

The adjutant gawked at the fan as it unfurled and appeared to suspend itself in mid-air. /Oh my God... what magic is that!? Could... could the fan be a magical item!?/

Eventually, strong gust starting blowing constantly from behind even the adjutant. 

"Kyaaa!" she struggled to keep her skirt from fluttering with the gale.

"Focus on the battle!" the voice said.

"But-"

"Do it!" The adjutant flinched from the deafening scream sent directly inside her ear.

"Hngh!" her body cramped up again as she lost all control; both of her limbs and of her skirt. "Aaaaahahaaaa," she sobbed, "no!"

Her vision, still focused on the frontline, the adjutant then saw their soldiers removing the satchels slung around their shoulders. /Eh? What are they doing? And why did they carry their bags into battle!?/ A few moments later, she saw faint puffs of smoke trailing behind them. Baffled, she tried focusing closer to her general's bodyguard, but the limits of her magical ability prevented her from seeing any better. 

/What is it!? What's happening!?/

She was about to turn to her general when, all of a sudden, the bodyguard and her soldiers stopped within a few dozen feet of the enemy's reformed line. One by one, they then swung their satchels forward, tossing them into the shield line before them. As the satchels flew in the air, some flipped open, spilling their contents out - a solid black mass with what seemed to be a long wick sticking out of one end. The wick burned furiously, throwing sparks into the air as it, and the rest of the satchel disappeared behind the mass of armored bodies.

"Miss General, t-they stopped the charge!?" the adjutant stammered in her panic. "B-but it looks like they're throwing something-"

"Heh. "The general's lips twisted into a sharp grin.

In an instant, the enemy's front line disappeared in a cloud of dust and smoke. Several explosions rocked the ground and threw dirt, blood and body parts into the air indiscriminately.

The adjutant's jaw dropped.

"Grenades," the voice said, "another practical application of the ammunition we use for the cannons. What do you think? Amazing, isn't it?"

/W-where did she find these weapons!?/

After the barrage of grenades, the bodyguard and the other reservists ran back behind the line of their surviving pikemen. All three companies merged seamlessly into one unit and formed a battle line three ranks deep. They then advanced straight just in front of the cloud and started thrusting their pikes right through. 

Even their unaimed thrusts managed to claim a few kills as the enemy soldiers were unable to freely defend themselves from attack. The smoke left behind by the grenades proved too thick to see farther than a couple of feet. On the other hand, the pikes appeared to come from out of nowhere, deflecting of a shield or a breastplate, but sometimes punching through, leaving a wound each time. Meanwhile, their swords did not have the reach to retaliate in kind.

Finding themselves at a severe disadvantage at range, the enemy soldiers pressed forward, into the cloud or around it, but, any attempts at breaking through was met with precise hits from the layered ranks of pikes and thus repelled. While all this was happening, the like of smoke provided a range reference for the cannons at the far back, allowing them to lob explosive shells at the back of the enemy formation, breaking it up even further.

No longer able to advance, the enemy commander galloped forward and finally recalled his soldiers, covering their escape with another volley of arrows that again proved futile against the powerful gale blowing against them.

"Miss General," the adjutant spoke, "I think they're going to attempt to regroup with their reinforcements! If they do, then their numerical advantage would grow even further!"

"Is that so?" her general said, "then we should put a stop to that, shouldn't we?"

Another, distant explosion sent a shockwave across the ground.

/What is it now!?/

The adjutant opened her eyes to get a distant look. /Eh!?/

"M-Miss General..." she stuttered, "the bridge! It's... gone!"

To her amazement, the only bridge that connected the river in this province had turned into a column of dust. Around it, enemy soldiers were either running away, lying injured on the ground, or drowning in the river below. Furthermore, the reinforcing army had been split in two, with most of its number left on the far side of the river.

"What do you think?" the voice said, "Now the enemy can't escape us."

"E-escape...!? Us!?" She flinched. /Does she actually intend to annihilate the enemy army? No way... but after everything I've seen today... Somehow, I'm starting to think it's actually possible./ 

"Now, it's time for the finale." 

When she heard this, the adjutant felt as if the the restraints squeezing into her body were loosened. She felt light all of a sudden, and her movements were no longer restricted.

/Did... did the magic end...?/

"Are you ready?" the general approached her with a sultry grin on her face.

She swallowed her breath. "Ready for... what, General?"

"We're going to envelop the enemy."

"Eh? The adjutant jolted back. But we don't have the numbers for that..."

Her general turned around, slipping her a glance as she did so, "Yes, we do." She stepped up to the crest of the hill, casting her shadow over her adjutant. "This is a new age of warfare," she said, "fought not only by man..." 

"Miss... General?" Her heart raced.

The little girl reached her hand out in front of her and took a deep breath. Soon, a dense mist of magic gathered around her, engulfing her body in a sickly black cloud. She took a deep breath. All at once, the black mist collapsed into her hand. She then raised that hand in the air and snapped her fingers.

All that magic amplified the snap into to a deafening shockwave that spread in all directions.

Before she even knew it, the adjutant was thrown into the ground. All she could her was a constant ringing. Only as she opened her eyes did her hearing began to return. 

The gust had gone. There was a stillness in the air as she looked on to the back of her general -  a foreboding. As she regained her footing, the little girl glanced at her. With menacing grin drawn across her face, she beckoned her to look at the battlefield below. 

Though still dizzy, the adjutant stumbled back to her feet and did as she was told. The moment she observed the state of the battlefield, her heart sank.

From the forests, a tide of snarling beasts, humanoid in form, flooded out. The beasts charged directly at the scattered remnants of the reinforcing army, and at the line of enemy archers at the far back.

The adjutant crawled at the feet of her geenral. "M-m-miss General...!" she panted, "Monsters! Monsters are pouring out of the forests!"

However, the girl just glanced at her, saying, "I know."

The monsters cleaved through the rear lines of the opposing force, throwing their already disorganized ranks into an utter rout. However, with a bridgeless river behind them, the stragglers had no choice but to abandon all of their heavy equipment and swim across the river - a desperate decision that claimed more lives than it saved.

At the same time, their own line, spearheaded by her general's bodyguard, penetrated through the front and started chasing down the retreating heavy infantry.

The adjutant fell to her knees. /Don't tell me... Miss General is also controlling the monsters...!?/

In the meantime, the enemy commander galloped in front of the retreating heavy infantry and gathered a core of soldiers around himself.

"Mi-miss General..." the adjutant muttered. "They..." She paused. /No... is my help even necessary anymore...? With all this.../

"Mm. I see it. Looks like they're regrouping around their commander." the girl crossed her arms. "I have to hand it to them, it's pretty impressive to recover from a rout as bad as that."

The enemy commander and his reformed core of men then fought off the combined attack of their soldiers on one side and a pack of monsters from another. After several deaths on both sides, that pocket of the battlefield turned into a stalemate, with an exhausted enemy force trapped on all sides.

The little girl clicked her tongue. "Maybe I shouldn't have blown that bridge up after all... now they're fighting to the death."

The two sides stared each other down, both assuming a defensive position now that each had lost its advantage - of sheer numbers on the enemy side, and of surprise on theirs. 

After a short while, the general, with her adjutant and bodyguard in tow, emerged their line.

Fanning herself as she pranced along, the little girl confronted the enemy commander. "Well hello there! Did you enjoy my little welcoming party?"

The enemy commander grimaced as he glared at her.

"Now, what's with that face?" She tilted her head upwards, such that even at her height, she was looking down him. "Was my welcome unsatisfying?" 

His face contorted even further, but he would not reply.

She clicked her tongue. "How boring."

The adjutant whispered to her, "General, please don't provoke them any further... can't we just ask them to surrender?"

"Hm, now that's a good idea." she smirked. She then stepped forward, reached her hand out and amplified her voice, "Alright, listen up, you losers. Drop everything you're holding and get down on the ground! Oh, and take off all your armor too, those look expensive."

"Waaait!" The adjutant yelled. "General, you sound like you're trying to rob them! Please ask them to surren-"

Before she could finish speaking, the enemy general sneered at them, "If you want this fine armor, why don't you try take it? Or are you scared I might strip you naked instead!?"

The enemy soldiers laughed out.

"Oho!?" The little girl grinned. "You're going to need to drop more than your armor if you want a look this body!"

As she started walking forward, her adjutant and bodyguard chased after her. 

"Wait General," the adjutant panted, "what are you-"

Before she should finish, the girl raised her arm in the air, her fan pointed skywards. 

/Eh!?/

As she swept her arm down, she yelled, with a magically amplified voice, "Fire!". 

The sound of cannon fire thundered across the distant hills. In a split-second, black iron balls the size of a head crashed down from the sky. Amidst the flurry of explosions dirt, blood and bodies flew in all directions, peppered by the odd screams of death.

Throughout all of this, the little girl clutched her belly as she struggled to contain her laughter. "Ahahahaha, aaaaahahahahahaha!" she pointed and laughed at the enemy commander.

Seeing his men torn apart behind him, the enemy commander finally ordered a full charge forward, ignoring the monsters behind them.

"Aha!" Sylphia squealed, "So you're finally in the mood to play!?"

Meanwhile, the adjutant tugged at her arm, screaming, "Miss General, we need to leave the front!"

"Don't be silly," she said, with eyes ablaze, "this is where the action is!"

Immediately, the bodyguard stepped in front of her, sword and shield drawn.

The adjutant's grip wavered.

"Actually," the little girl muttered, "there's something I really want to try out..." From her belt, she drew a short, curved object of polished wood.

"Die, so-called Heroine!" The enemy commander shouted as he dashed straight for the little girl. As his men died in droves from the cannon fire behind him, and from crossbow volleys in front of him, his eyes focused only on a single thing.

"Ahaha, so you've heard of me!" the girl yelled back. "I'm honored!"

The enemy commander gave a booming warcry.

He leapt in, sword raised above his head, before crashing down on them. His sword connected with the bodyguard's shield, throwing her into the ground like a ragdoll.

The adjutant then attempted to intercept him by stabbing her blade into his chest, but his armor proved too solid for her blade to punch through. In retaliation, he simply knocked her her away with his shield.

"Ack!" The adjutant fell to the ground, blood dripping down her face, obscuring her vision from one eye. She attempted to stand up, but the moment she tried to move, a sharp pain spread across her forehead.

/No... ack... I need to.../

As she laid motionless on the dirt, she tried to open her eyes. Right in front of her, she caught a glimpse of her general. She stood, dignified, if alone, before the enemy.

"Ge-" she attempted to speak, "General..."

She held her arm at an angle above her head. pointing the wooden object right at the enemy commander's face. With a condescending smirk, she mouthed these words:

"Go to hell."

Click.

An fierce explosion deafened the adjutant. In a single moment, she felt a heavy thud against the ground. Fearing the worst, she strained to open her eyes, though the dust mixed in with the blood, producing a painful burning in her eyes.

Standing right before her, tinted through a shade of crimson, was a girl of small stature. The girl smiled at her as she bend down, placing her hand on her shoulder. Soon, the ringing in her ear subsided.

"Are you okay?" The girl asked.

"Huh?" The adjutant looked around, completely baffled. It was then that she finally found the enemy commander, lying face-down on the dirt, drowning in a pool of his own blood.

She covered her mouth.

"So, what do you think?" The girl held the wooden object to her. "This is probably the first time you've seen these things, huh?"

"I-I..." her head ached. It wasn't just the hit she took; her entire body felt heavy. "I don't know what to say..."

"Mm..." The girl crossed her arms and nodded. "Well, it might be a bit too much to take in at once. But you'll get used to it soon! After all," she then placed her hand on her adjutant's cheek and showed her a radiant smile. "you'll be working with me from now on!"

Her cheeks flushed bright red.

The girl grinned. "Now, let's get you fixed up."



/My General... The Heroine, Sylphia./

/Her name started circulating a mere five years ago./

/In such short period, she placed herself at a significant position within the kingdom, and became the talk of nobles and peasants alike./ 

/There are many stories of the Legendary Heroes in times long past. They spoke of brave men and women standing up to the monstrous hordes of the Demon King. Each was known to have incredible strength, far surpassing that of a human, and each wielded a mythical weapon that far surpassed magic as we know it./

/I even went so far as beg to become her adjutant just so I can know more about her./

/To say that my expectations were blown away is an understatement. Who is she, really? Someone who can command a horde of monsters... someone who has access to such frightening weapons and dispenses them to her subordinates... and someone who looks as frail as she does, and yet has such a twisted personality... Is such a person truly a Legendary Hero of old?/

/I thought that, if I get closer to her, I could learn more about her.../

/...but now that I'm here, only the questions keep increasing./