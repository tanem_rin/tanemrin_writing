Chapter 2

A man, shrouded in dark robes, sat beside the candle light with a book spread out on one hand, and a flask in another. Light did not reach far into the room. It provided nothing more than a silhouette of the various knick-knacks littered across the floor.

A knock on the door.

It took the man's complete attention away from his book, much to his dismay. Nevertheless, what was done, was done, and so, he placed his book on top of a pile of other books that towered just beside his seat, teetering precariously to one side, but not quite ready to collapse.

"Come in." His voice was deep and raspy. Very raspy, as if his voice was filtered through sandpaper.

The door swung open, allowing light from outside to illuminate a straight path between him, and the maid, who stood outside the door.

"Lord Alphonso, the King requests your presence." the maid said, her head bowed the entire time.

"I see." He thus downed the liquid within the flask in one go and stood up. "I shall come with you."

"Of course."

####

Following behind the maid, Alphonso made his way down a lengthy flight of stairs, heading down, deep beneath the castle. 

He was familiar with this place - an underground stairwell dug into the hill the castle was built upon. Just the fact that he was being led here to meet the King told him precisely what he was being called for - the Legendary Hero.

They soon came upon a large, wooden door, reinforced with steel belts. In front of this door, two guards, garbed in full plate armor, stood in salute with their ornate poleaxes in vertical display.

Alphonso nodded as he passed them.

The maid then pushed one side of the door open, and said, "Your Majesty, the Court Mage, Alphonso has arrived." She then stepped to the side and lowered her head as Alphonso passed her.

Inside was a massive room built of stone. Most of it was open space, and had two doors on opposite sides - one where Alphonso entered from, and the other, across the room from him. At the far end, narrow slits rather than windows, allowed sunlight to filter into the otherwise poorly lit room. 

Nearest to him, the King stood roughly in the center of the room, flanked by two servants. The third, the servant who called for Alphonso joined him soon after.

On the far end of the room stood a small girl, dressed in a fine, white dress. Alphonso noted the peculiar color of her hair, that took a silver hue, despite appearing so young otherwise. The girl cast a suspicious eye on him as he entered, holding her hands to her chest like a frightened child.

"Good morning, my Lord." Alphonso said.

The King gave him a nod, and then faced the little girl. "Dear Heroine, this is the Court Mage, Alphonso."

/Oh?/ Alphonso glanced at her. /So this child is the Legendary Hero? How interesting. There have certainly been stories of children being summoned as Legendary Heroes./ 

The King then turned to Alphonso. "Alphonso, this is the Legendary Heroine, Sylphia."

Alphonso nodded and then faced Sylphia saying, "Pleased to meet you, our dear Heroine."

"A-ah... The pleasure is all mine." the girl said, with a curtsy, "You will be the one to teach me magic, yes?"

The King interjected. "The Court Mage is here to test your magical aptitude, heroine." he said with an abrasive tone in his voice.

The girl jerked back. "T-test...?"

Alphonso stroked his trimmed beard. /A Legendary Hero capable of using sorcery... I wish to see such power in display!/ He stepped forward. "Magic is an innate form of understanding of the immaterial world." He said as he paced around the room. "In other words, either you have it, or you don't."

She flinched.

"It cannot be taught to someone who does not know it," Alphonso continued, "though it can be improved, if they know even just a sliver of its truth."

The King then said, "And there is but a single fool-proof way of assessing magical aptitude."

"A-a-" she stuttered, "and that is...?"

"Self-preservation." 

/Hm? That tone he is using... does my Lord doubt the Heroine?/

She shrank back "W-w-w-wait...! A-e-... ca-can't... isn't there another way!?"

The King ignored her pleas. "Alphonso."

"Yes, my Lord." He nodded. 

The King and his servants then retreated to one side while Alphonso positioned himself at the center of the room.

/Did something in the summoning ritual go awry? No. Everything was perfectly in place./

Sylphia took a step back. ""W-wait a second!"

/Surely, she does not look at all impressive at first glance, but she is still the Legendary Hero - the chosen of the Gods. She should be!/

/I shall put my faith in you, dear Heroine./ Alphonso closed his eyes. /Now, what spell shall I test on the Legendary Hero? To provoke a sense of fear, one of the grand magics ought to be ideal, if I did not need to set up a magical field beforehand. Perhaps an inferno would suffice. It is easy to dispel, just in case./

Having made up his mind, Alphonso started chanting. It started as low mutterings, only barely audible because of the utter silence enveloping them. Gradually, a smoke-like substance gathered around him. It glowed with myriad colors and danced in the air, as if it flowed with the mutterings that escaped his lips. 

After a few iterations of his chant, his mind finally fell completely empty. Finally, he started growing his voice. Focusing on the spell he wanted to cast, he emphasized every syllable of his chant; louder and louder each time, until the chant that once started from a tiny whisper had turned into a loud dictation.

Alphonso opened his eyes.

/Hmmm...?/ 

Before him stood not the frightened little girl mere moments ago, but one who stood steady and looked him in the eye. 

Alphonso saw a flame in her eyes - the flame of defiance. It burned intensely as she stood her ground, with clenched her fists. 

Sylphia rooted her feet to the floor and grinned.

At that moment, the King's raised his brow.

/What is she planning? It seems I need to set aside a store of magic to defend myself from her backlash./

Sylphia raised her arm and started chanting herself.

/Oh? Her magic is also based around enchantments? That is most interesting...!/

Her chanting went by at a blazing pace. "Hailmarymotherofgodprayforussinnersnowandatathehourofourdeathamen!"

Alphonso staggered back. /I have never heard a chant that quick! Is it some sort of divine technique!?/

"Aaaaah!" She then clutched her head. "It's not workiiiing!"

/Hm!?/ Alphonso's eye twitched. /Was she perhaps... imitating me?/ He grumbled, "Enough games, Heroine! If you don't take this seriously, you will die!" 

Even as he gathered magic so dense that it almost obscured his view, nothing appeared to be happening around Sylphia.

/Did I misjudge her? Does she really have no talent for magic? If that is so, then a hit from my magic would prove fatal!/ Alphonso glanced at the King, relaying his doubts with his expression.

But in spite of this, the King shook his head, and watched on with indifference.

/Does... he intend to kill the Heroine? Though indeed, a Legendary Hero who could be killed merely by magic like mine would be no Legendary Hero at all, still!/

At the very end, Alphonso's voice ramped up quickly in volume, reaching the climax with a shout, "Come forth!" /What are you planning, my Lord!?/ Immediately, the smoke collapsed around him and turned into a jet of fire. "Reduce the ground to ashes," he yelled, "Inferno!" The flames spread out from his hand, forming a cone, headed right for Sylphia.

"Aaaaaaahh!!" Sylphia screamed at a deafening pitch. "Do something!!"

In the split second before the fire was about to incinerate her, black smoke erupted from her body and formed into a ball overhead. In an instant, the smoke then collapsed into itself and transformed into a ball water, drenching her entire body.

"Stop!" the King shouted. 

"Ha!" Alphonso clapped his hands, breaking his concentration. In an instant, the jet of flame dissipated.

A large portion of the room had turned to sizzling black char. In between Alphonso and Sylphia, a cone of smoking floor showed exactly just how close she was to turning into dust.

At the far end of the room, the Heroine, Sylphia had collapsed to her knees. Drenched from head to toe in water, she held her hands over her eyes and sobbed profusely.

Alphonso sighed, stretching his shoulders back. /I haven't cast anything that big in a while.../

"So you can use magic, after all." The King approached her. "I am sorry to have doubted you, Heroine." 

He extended his hand toward Sylphia.

"When faced with a life or death situation," Alphonso followed behind him, "a mage will usually count on their most powerful spell, no matter what it is, to save themselves. My lord," He looked to the King, "it seems she is only capable of using the lowest level of magic."

"I see." The King gazed into her eyes. "That's fine. You did say that magic power can grow, even if she only knows but a sliver, correct?"

Alphonso nodded. "Indeed."

"Very well." With his finger, the King gently wiped Sylphia's tears away. "Come now, Miss Heroine," he said with a low, breathy voice, "and take your rest."

With uneasy hands, she took the King's hand and pulled herself up. Despite her being drenched in water, the King showed no hesitation as she held her hand. Water dripped down her dress the moment it lifted off the ground, leaving a large puddle beneath her. Though her sobs had yet to subside, she forced herself to face the king.

"We shall discuss the matter of your armaments another day," he said, "You must be tired."

She hung her head and, with a trembling voice, said, "Yes..."

####

That evening, Alphonso visited the King in his quarters.

"Your Majesty," he said, "you wished to speak with me?"

"Yes, sit down with me, my friend."

The King sat alone on his table out on the balcony. Alphonso approached him, and sat across the table, noticing that a glass with liquor had already been prepared for him.

"A troubling matter, my Lord?" Alphonso asked.

"Enough to make me drink on a Monday."

Alphonso joined him and lifted his cup, taking a sip of the finely aged Londinian Mulberry Wine the King always loved.

"Tell me," the King spoke, "what are your true opinions of the Heroine?"

"Hmm..." Alphonso swirled the wine in his cup.

"I had prepared an entire armory of magical weapons, acquired at great expense throughout my lifetime. I assembled all the possible types of weapons that a Legendary Hero had ever used..." He took a deep breath. "And I saw how she was unable to use every single one..." He then let out a deep sigh. "You were not there to see that part."

"I see."

"What options do I have?"

Alphonso mulled it over for a long while, and then came to the conclusion, "There are none."

"By the Gods..."

"I am afraid that we can only hope to bank on the one glimmer of talent that the Heroine displayed."

"Magic... huh?" The King finished his cup and then proceeded to pour himself another one.

"Yes..."

"From her talent," the King sneered, "below that of a novice mage."

"I am afraid so."

The King dropped his cup hard on the table.

"Is there no other way? Is it possible to summon a different Hero?"

"Such a thing had never occurred in the millenia of history of the Legendary Heroes. And even then, the cost to try once more..."

The King bashed the table with a closed fist. His eyes wrapped in despair, he took in a deep breath through his teeth.

"I am sorry, my Lord." Alphonso lowered his head.

The King shook his head. "The Kingdom is in danger." He looked sharply at Alphonso. "This cannot get out. I'm sorry, my friend. But I need to commit an evil."

Alphonso closed his eyes. "Is it a necessary evil?"

"Yes."

He breathed a heavy sigh, dipped his head down and pondered. After a long pause, he arrived at his answer, "Only the Gods can decide for sure." He looked at the King once more. "We mortals can only do what we believe is right. Be it good or evil."

The King gave a subtle smirk just at the corner of his mouth. "So there is a right and wrong, apart from good and evil? I would not have expected a chaplain to say that."

"Former, my Lord. You know that I have left that far behind."

"Yes... I know." The King relaxed in his seat once more. "My friend, in truth, I must request something of you."

"Anything, my Lord."

"I must ask you to teach the heroine everything you know. Everything."

"Hmmm...?" Alphonso's eyes narrowed.

"If she is the only thread of hope the Gods are willing to provide, then I wish to make the best of her."

Alphonso lowered his gaze. "Even if she should learn all the sorcery that I know, that still cannot reach the power of a true Legendary Hero."

"That is fine."

Alphonso raised an eyebrow. "Even though my Lord is pressed for time?"

"Nevertheless, there is no other option. I will take whatever you can give me."

After confirming his master's resolve, Alphonso bowed his head. "It will be done, my Lord. I shall see to it that our Heroine is molded into a Legendary Hero befitting that title."

"Thank you... thank you." The King's moustache shifted as his lips formed into a smile. "Please, join me, my friend. I feel I must drown my sorrows, just for tonight."

"As you wish, my Lord."

Thus, the two spent the following hours reminiscing, exchanging stories of times long past, and finishing off the bottle of wine between them.

From the that day onward, the servants that assisted in finding a weapon for the heroine would not be heard from again.