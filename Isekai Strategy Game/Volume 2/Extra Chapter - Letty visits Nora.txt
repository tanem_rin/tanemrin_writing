Extra Chapter - Homecoming

She gets off the wagon.
Ealdton is as lively as ever.
The guards are stationed everywhere.
Deen is personally training new recruits, jogs past Letty doing PT.
Letty continues to the market district, visits Adrianne's library.

The girl hopped off the wagon, holding down her wide-brimmed hat so it would not be whisked away by Ealdton's early summer breeze. She whisked her long, purple hair behind her ears and fixed her clothes, making sure that her dark, baggy mantle covered her from every direction.

She exited the stables and entered the Northeastern gate. Given the rumors she had heard about an attack, she was surprised to see no visible scars on the outer walls, still as pristine white as the last time she visited, several months ago. Even within the walls, very little changed of the old stone town she'd known since childhood. It made her question, if only for a moment, if the rumors were true at all. But then, she soon came to a large stone slab, erected along the wall, in the empty lot she used to visit. 

There used to be a large tree there, and during the summer, when her grandma would force her to go out of the house, she would often, in defiance, bring her books and read them under its shade. She was never caught. But, needless to say, the tree was gone. She gravitated towards the stone slab, perhaps from a combination of frustrated nostalgia and curiosity of the new. Written upon it were the words, 'Let us forever remember the fallen heroes of Ealdton', followed by a very long list of names; names she mostly didn't recognize, having not lived in the town for years.

As she continued her journey, she came upon a line of young men, jogging in a neat line up the street.

"Hup! Two! Hup! Two! Two laps left!" yelled the man in front.

They soon passed her, and she noticed all of them wearing guardsman attire.

"Deen," one of the men said, in between heavy breaths, "I swear- my legs are killing me!"

"No pain, no gain, Tom! Hup to it! You're a squad leader now! You need to be an example to your squad! Now, Hup! Two! Hup! Two!" 

"Oaaaaaagh!" he wailed, "I wish Cap was our commander instead...!"

The men soon disappeared on the next corner, though their voices still reverberated against the high walls for a good while after.

Witnessing that scene reminded her of a certain pair she knew. Just for a little bit, she helped herself to a small chuckle.

She soon entered the market district. As always, the crowds were thick and streets are bustlting with trade. Carts rolled back and forth, carriying goods, people, often both. Criers competed with one another to see whose voice could overcome the ambient noise of the market, each desperate to bring just one more customer to their stands.

And then, in one particular corner of the market, sat a shop that was definitively not busy.

She looked up at its sign and read it out, "Adrianne's Library." 

She smiled.

####

"Grandma!" she said, "I'm home!"

The girl closed the door behind her, careful not to bang it against the frame.

"Ah, Laeticia, dear!" said an old woman as she came out of the back room, "You didn't say you were coming today, I wasn't able to prepare something for you!"

She tilted her head and smiled. "It's okay, things were pretty hectic these last few weeks, and I was in a hurry to see you..."

"Oh my, is that so? Well, come in, come in!" She beckoned to her. "Let's get that stuffy mantle off of you."

"Ah, mm." 

That day too, Adrianne's Library was closed.

Laeticia, or Letty for short, joined her grandmother, Nora, for afternoon tea.

The alluring scent of freshly ground tea leaves, grown right in their back garden, wafted inti Letty's nose.

She shivered as the tea's warmth spread around her chilled shoulders and back. "Ahh..." A soft smile surfaced on her lips. "Grandma's tea is the best..."

Nora chuckled. "Your mother was really picky about her tea, after all." She sighed. "She was the one who insisted we plant our own tea from the seeds she got from somewhere or another... And tea isn't even that expensive, either."

Letty giggled. "Really...? That's the first time I heard of that."

"It true. Your mother really had a lot of quirks." Nora then smiled at her, her eyes locked in a deep stare into Letty's eyes. "Good thing you didn't take too much from her."

"Mmm...?" Her brows fell. 

"Ah, by the way, you said you were in a hurry to see me?"

"A-ah..." Letty jerked up. "That's right, I heard that Ealdton was attacked recently... and I was worried!"

"Recently...?" Nora put her palm over her cheek. "Dear, that was at least a month ago..."

She averted her gaze, twiddling with her thumbs. "W-well... I, um... Don't really hear the news that often..."

"Oh, my dear... have you been locking yourself in your room, even at work?"

She flinched.

Nora sighed. "Dear, oh dear."

"W-well...! Um..." She struggled to wrap her mind around what she was going to say in her defense. "Uh... well, my work is just... like that...?" She glanced up at her grandmother.

Nora held an unconvinced expression on her face.

"A-anyway...!" Well aware that she had no chance of persuading her grandmother, she decided to change the topic instead, "I heard the Legendary Heroine was the one who saved the town...?"

"Ah!" She clapped her hands. "Yes, she did! She visited this store that very day!"

"Eh!? S-so you met her!?"

"I did!" She chuckled. "She came in with Mr. Alphonso!"

"Eh...?" Letty's eyes widened. "With Master...?"

"Yes! You still remember him?"

"O-of course...!" She pouted. "H-he taught me everything I know...! And I haven't really thanked him properly..."

Nora's lips arced wide, beaming her granddaughter the warmest smile. "Don't worry yourself, dear. I'm sure you'll be able to thank him someday."

"I... I hope so..."

The two spent the next few moments in a peaceful interlude, sipping warm tea in silence.

Then, Letty spoke up, "Actually... I met the heroine, too..."

"Oh my? Did you, really?"

"Mm. " 

- she was a little girl wasn't she
- yes yes! she was a nice girl wasn't she?
- E-um... well... yes? She... made a habit of teasing me, though...
- Oh my! Nora giggled.

After chatting for a while longer, Letty went up to her room.

Just like everytime she visited, she was filled with a longing nostalgia. 

She flicked open her old books; the ones Alphonso recommended her to read while she was still learning magic.

Then, she wrote in her diary.

After noting the date, she went on to write, 'A few weeks ago, a couple of girls joined the adventurer guild.' She thought for a while what to write next, and then settled with, 'I didn't expect, when I took them in as apprentices, that they'd bring me along to an adventure I'd never forget in my entire life.'


- actually i mey the heroine too
-more chat
-visit room
-read her old books
-wrote diary








