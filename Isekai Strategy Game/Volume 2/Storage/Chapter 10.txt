Chapter 10 - My Preparations

Sylphia spent the better part of the next few days just drilling the sith day in and day out. In the meantime, she sent scouts out every so often to find any traces of Rika, Ruth and Ceressa, but none had turned up any information thus far.

After a while, along with a lingering cloud of dread hanging of her head, she got tired of her routine and decided to take a quick stroll of the village instead of drilling the sith.

"Letty, I'm heading out!" She said.

"Eh? Isn't it a bit early for the drills?" Letty asked, "Should I come with you?"

"No, I'm going to wander around the village for a bit!"

"Huh..." Letty cocked her head to the side. "O-okay... please don't cause any trouble."

"What do you think I am, a child!?"

"Eh? Um... well..."

Sylphia pouted. "Don't answer that!" She then stormed out of the temple with her cheek puffed to one side.

####

Outside the temple, two paths led out to the village. One path went further up the ridge, circling around a cliff face near the underground stream that flowed out beside the village. The other path led right down the center of the village and branched out from there.

/Okay, where to go.../ 

She took a quick glance at her options. Fewer huts lined the road up the ridge, and few siths walked along the path as a result. Meanwhile, the center bustled with commotion, with one or two siths always running between the densely packed huts.

/Hmm... I don't really feel like working up a sweat, so I guess I'll just wander around the center./

She skipped down the slight slope into the village. As she had become accustomed to for the past week, every Sith she came across would bow solemnly to her, whenever she would walk past them in the village. She was, after all, their Goddess of War.

As she passed along the streets, she got a good look at the siths houses. All of their huts were fashioned from dried giant mushroom caps, that varied greatly in size. Inside, she normally saw one or two siths lounging around and chatting away. 

/Hmm... so even monsters live a normal everyday life.../

Along the way, she met with a group of sith armed with practice weapons.

"Woof! Oh, my Goddess, is it time for our training already?"

/Ack, shit. Of course I'd run into these guys here.../

"Uh, err..." Sylphia racked her brain for a quick excuse. "Ah! That's right - today, I'm letting you guys do it on your own!"

"Woof! A-are you sure, my Goddess? Woof! The others aside, I am still unsure of my movements..."

"It's all right!" Sylphia patted him on the back hard enough for his head to swing back and forth. "All you need is practice, right? I don't need to be there just to tell you to keep working on it, right?" She winked at him.

"W-woof... right."

"Of course I'm right!" She grinned. "And here's the key - since you'll all be working as a unit from now on, you should depend less on me, and more on each other! That way, you can help each other get better, isn't that right!?"

"Woof!" The sith's eyes sparkled. "I have been short sighted all along!" He then lowered his head to the ground. "Woof! Truly, the wisdom of the gods far exceed mere mortals like us!"

"Bowow! Truly!" 

"Ruff!"

The siths beside him also lowered their heads.

"I know, right!" She covered her mouth with the back of her hand as she laughed, "Aaaaha ha ha ha ha!"

/Fuck yeah! Off the hook!/

Sylphia thus hurried away from them before they could have second thoughts.

####

For a while, Sylphia's stroll continued at a leisurely pace with no further interruptions. 

Eventually, at a far side of the village, she came upon a large hut with an exaggerated chimney built to one side.

/Oho?/

The unusual building piqued her interest and so, she decided to check it out.

/That chimney looks built from rock or something... If they're capable of masonry, why do they keep living in these mushroom huts?/

"Heeey..." Sylphia greeted as she turned the corner and took a peek inside the building.

/Oho?/

Inside the building, Letty was speaking with a sith with dark fur, who held, on his hand, a metal rod that prodded into an opening at the bottom of the large chimney.

"Oh, hey Letty." Sylphia waved at her.

"Ah?" Letty said. "I... didn't know you were coming, Sylphie."

"Ah, no, I was just roaming around and saw this place." She looked around the hut. "It's pretty big, isn't it?"

"Mm." Letty nodded. "This is where they make the Mythril, apparently."

A tiny spark lit up in Sylphia's eyes.

"Oh?" She skipped along inside and watched the sith work his tools. "Let me see, let me see."

"Rawf, of course, my Goddess." The sith moved slightly to the side, allowing Sylphia to look into the furnace. "Rawf, I am honored that the Gods favor my humble workshop."

The sith prodded the white hot flames, causing a mote of sparks to erupt out of the furnace.

"Rawf, please don't get too close to the flame, dead Goddess, you might get burned."

"Okies~" Sylphia idly replied, and then continued staring into the flame. 

The sith went on with his tasks with Sylphia looking over his shoulder.

"Speaking of," Sylphia said, without taking her eyes off of the sith's actions, "what are you doing here, Letty?"

"Huh? Weren't you the one who asked me to see if we could produce weapons faster?"

"Eh? Did I?" Sylphia chuckled, "Ehehe, I forgot."

"Geez..."

"So," Sylphia said, "what do you think, so far?"

"Well..." Letty rubbed her forehead. "The smith, here, has already told me that the smelting process takes the longest time, and it's impossible to make more than one weapon every day, or one set of armor every three days..."

"Geh..." Sylphia frowned. "that's really slow."

"Rawf, I apologize, my Goddess," the sith said, "the Mythril takes a lot of time to melt and shape, even if taken directly from the crystals..."

"Hmm..." Sylphia looked around the workshop. "By the way, are you the only one in here?"

"Rawf, yes, my Goddess."

"Huh? No one is helping you?"

"Rawf, yes, most of the village is tasked with protecting us, after all, so it can't be helped."

"Huh..."

/Wait, weren't there a bunch of siths just killing time in their houses? They've got some nerve to be slacking around when they're facing extinction./

"Say," Her eyes narrowed. "if I give you... I don't know, five, maybe ten guys to help around, how fast can you make weapons? Just give me a rough guess."

"Rawf, well... if the furnace is utilized all day without me having to stop to assemble the weapons..." The sith rubbed his chin. "Rawf, maybe 3 weapons and 1 armor a day?"

"Oh, ho? And how much faster if you had more furnaces?"

"R-rawf!? Well, I truly cannot say, but, perhaps, we can double the number for a second furnace, but only if I had more hands working on it..."

"Mmmhm hm hm...?" Sylphia's lips curled up.

"Sylphie...?" Letty glanced at her with worried eyes.  "What are you thinking...?"

Sylphia gave her a wide grin. "Hey," she said, "how about I give you something to do that's a little bit more permanent?"

"Eh? W-what do you mean?"

"Basically, I want you to manage the supplies we'll be needing. Food and medicine is a given," Sylphia patted the wall made of mushroom cap. "but that, of course, includes weapons and armor."

"Ummm...."

"I've got my hands full with training the siths, you see?" Sylphia shrugged. "I can't then go running around the village making sure the equipment is being made for each of them."

"U-uh... huhh..." Letty fidgeted around with her fingers.

"Okay! I'll take that as a yes!" Sylphia grabbed her hand.

"Ueh!?"

"Come with me!"

"W-wa... waaaaaaaait!!"

Sylphia dragged her around the center of the village with spring in her step, and started yelling.

"Listen up! Listen up!" She curled her hand around her mouth. "The Goddess of Mercy is recruiting members for her Supply Corps! Those who want to serve with this cute Goddess come on over to the temple to sign up!"

All the while, Letty put up a token struggle against Sylphia.

"W-wait! Wait...!" she whimpered, with tears gathering around her eyes, "T-this is... this is too embarassiiiiing!"

"Aaaaaha ha ha ha ha!"

Sylphia dragged Letty all the way up to the temple.

####

"Guh..." Sylphia panted as sweat ran down her forehead and onto the ridge of her nose. "I... forgot how steep... this village was..."

Similarly out of breath, Letty said, "Geez... I... told you..."

Behind them formed a line of sith that stretched down the road from the village center.

Sylphia paused for a minute to catch her breath.

"Well," she said, "at least we have our volunteers now!"

"W-what do you need this many siths for anyway...?" Letty asked.

"Hmm?" Sylphia raised her brows. "Isn't it obvious? They're going to help you churn out weapons from that workshop."

"Wait, help /me/...?" Letty shook her head and waved her hands in front of her. "I-I-I don't know anything about making weapons!"

"Don't worry about that!" Sylphia patted her on the arm. "The sith will teach them how. I'll just need you to organize them," Her eyes narrowed. "and make sure they don't slack off."

"Uuuu..." Letty whimpered.

"I'm counting on you, okay?"

"Cou..." Letty's eyes widened for a second before turning back. "O-okay..." Letty nodded.

Sylphia grinned.

####

Letty and Sylphia returned to the workshop with recruits they gathered.

Overall, Sylphia managed to gather 20 siths to work with Letty. She stood in front of them and prepared an impromptu lecture.

"So, here's what I want you all to do." She raised her index finger. "It's called an 'Assembly Line'."

"Assembly..." Letty cocked her head. "Line..?"

Sylphia nodded. "When I watched the smith here working alone, he told me that the part that takes longest is smelting the Mythril." She turned to the smith. "Correct?"

"Rawf." The smith nodded.

"That means, during the time that the Mythril is smelting, it should be possible to complete other tasks, if there are other workers in here to help." She pointed to the sith volunteers. "That means you lot."

"But first of all, we need to increase the production of Mythril." She pointer her thumb at the furnace behind her. "This thing can't make us the amount of Mythril we need to fully equip an army. So, first thing's first - I want you all to build one more furnace like this!"

"Eeh!?" Letty stood up in protest. "How long will that take?"

Sylphia turned to the smith. "What do you think? We don't need the mushroom roof, we just need another one of these big furnaces."

"Rawwwwwf..." The smith stroked his chin. "It will take a few days, if the we can work with this many people. But aside from that, we will also need a larger supply of coal and mythril crystals."

"Mm." Sylphia nodded. "You can have part of the volunteers to go out mining while the rest work on the weapons."

"Rawf!" The smith nodded. "It will be done."

"Good. In that case, let me place my first order: I want 100 spears and as many pieces of armor, and I want them by... 10 days- err... 10 dinner times." 

The smith's jaw dropped.

"R-rawf! E-even with two furnaces running, that amount cannot be possible!"

"Oh?" Sylphia's gaze sharpened.

"R-rawf!" The smith shrank back.

"S-Sylphie!" Letty stood between them. "Y-you're scaring him!"

Sylphia disregarded Letty and continued, "How many people do you need to make it happen?" 

"Rawf...?"

"How many?"

"Rawf... I don't know... Rawf, As many as I can have..." The smith hung his head.

"I see."

Sylphia stepped out of the workshop.

"Wait, Sylphie!" Letty chased after her. "Where are you going?"

"Getting more volunteers."

"Huh?"

Letty followed her to the flatlands outside the village, where Sylphia's army was practicing their drills.

"We've gotten a few recruits recently," Sylphia said, "The army is now about 100 strong."

She then turned to Letty and looked at her with a sharp gaze. 

"I will lend them to you for a few days to start up the workshop."

"Huh...?" Letty took a step back, placing her hands on her chest.

"In the meantime, I'll find more siths who can help in a more permanent basis."

With that, Sylphia stood at the ridge beside the training ground and took a deep breath.

"All units!" She shouted. "Attention!"

Her black haze fell upon the sith below, who immediately stopped their drills, formed up in neat files and raiser their arms forward in salute.

"Tell me: what is a soldier without his weapon!?"

Prefaced with a myriad of different barks, her soldiers answered, "A meat shield!"

"And tell me: am I here training soldiers, or meat shields!?"

"Soldiers!"

"Then I have a mission for you: in due time, your numbers will grow. Very soon, the weapons and armor you have stocked in the village will not be enough to arm all of you! That is why-" She then waved her arm towards Letty. "The Goddess Of Mercy needs your help!"

The dogs cheered.

"Wao! Anything for the Goddess of Mercy!"

"Ruff! Whatever you need, my Goddess!"

Letty staggered backwards. "E-ehhhh...!?" 

"She is going to work to expand the workshop of your village, but she needs workers! Tell me: are those hands of yours only fit to carry weapons!?"

Her soldiers responded, "Our hands toil for anything our village needs!"

"Then your village is in need! For the next few dinners, you will assist the Goddess of Mercy, and perform any task she requires of you!"

The soldiers cheered. 

"Wan! Of course, my Goddess!"

"Bowbow! I will be done!"

Sylphia then turned to Letty.

"You heard them." She smirked.

"S-Sylphie..." Letty winced. "How did you get them to be like this...?"

"Hm?" Sylphia cocked her head. "In the end of the day, they're dogs, right?" She smiled. "They're pretty loyal once they get used to you ordering them around."

"U-uhhh... huh..."

Later that day, all of Sylphia's soldiers went over to the workshop to help constructing another two furnaces. More than a hundred siths shuffled around the area, lugging materials, shaping dirt and cutting mushroom stalks.

Meanwhile, Sylphia and Letty looked on in the sidelines.

"They're... really industrious." Letty said. "Is it because of your magic?"

"Huh? I don't think so." Sylphia said, "You can see my magic, can't you? There isn't anything floating around the right now."

"Hmm..." Letty groaned, "but what if it had a latent effect?" 

"Ah." Sylphia perked up, "I never considered that," she then shrugged, "But how am I supposed to know?"

/A latent effect, huh...?/ She snuck a peek at Letty. /I wonder if she's only acting like this because of the charm spell I cast on her back then... If so, would it wear off eventually?/

Sylphia hung her head on her palm.

/Still, it's been several days since we left the guild and she still hasn't realized anything./

The next few moments passed in silence, until Sylphia remembered what she needed to teach Letty.

"Do you know what an assembly line is?" she asked.

"T-the thing you mentioned earlier...?" Letty looked to the ground. "No..."

"Mm. I see. The concept is simple." Sylphia stood up. "With how the smith creates weapons, he does everything himself - he smelts the Mythril, shapes it, makes the handle or whatever the weapon needs, and sharpens it."

Letty nodded.

"The problem with this is it's very slow. You heard him earlier - even with two furnaces, he would only be able to make six weapons and two armors a day. At that rate, he can never finsish my order in less than two or three months."

"Do... you really need that many weapons?" Letty asked, "Don't the siths already have some weapons and armor among themselves?"

"Not really. At least, not to begin with." Sylphia started pacing around. "When you think of war, you're thinking of a long term struggle - far from a single battle, you need to think of it as a prolonged conflict with one goal: extermination."

Letty shuddered. "B-but-"

"Yes, that may not usually be necessary. Oftentimes, one side can surrender if you offer lax terms, however, how can you be sure that the enemy won't be seeking for /your/ extermination?"

Letty hung her head. "I... see."

"So we need to prepare for the worst case - that the entire army I am training is wiped out, all
of their equipment, stolen. In that case, we need to rebuild at least an army to protect the village. Would 6 weapons be enough? 12? 24? No, we need hundreds, as fast as possible. That's why I need you to increase production."

"But why me?" Letty said. "Aren't... you more suited to things like this?"

Sylphia shook her head. "You're diligent, and you can grasp new ideas very quickly. I'm not familiar with the process of creating weapons and armor here, and it will take me a long time to figure it out. That's why I'm counting on you."

Sylphia took Letty's hands in hers. "By teaching you what I know, and learning the methods of the siths..." She then clasped their hands together. "I need you to combine that knowledge and create a new one - an assembly line that can mass produce Mythril weapons and armor."

Letty's eyes gleamed for the first time. She mumbled, "You... trust that I can do this?"

"Mm." Sylphia nodded.

"But... what if I fail?" Letty averted her gaze, but Sylphia gently pulled her face back.

"You won't fail." She gazed deep into Letty's eyes. The crimson red glint in her eyes sparkled vividly, like polished cut of ruby. At that moment, while Letty was focused in her eyes, her black haze bubbled up from below and wrapped itself around Letty.

Letty closed her eyes and withdrew her hands to her chest. Then, she took a shallow breath before returning Sylphia's gaze once more, saying, "I will do my best."

Sylphia gave a warm smile, tilted her head slightly and said, "Thank you."

While waiting for the new furnaces to be built, Sylphia and the smith taught Letty the hings she needed to know. The smith explained his process of creating armaments, while Sylphia tutored Letty about the modern concepts of mass production and using replaceable parts in construction of machines. Though Sylphia's own understanding was vague and general in nature, Letty absorbed the concepts quickly and started applying them in weapons and armor construction.

In the days the followed, the workshop became the busiest corner of the village. Every hour, dozens of siths passed in and out, bringing in bags of coal, mushroom stalks and raw Mythril crystal, and taking out bundles of polished mythril arms.

At the center of this, Letty invented new techniques of creating weapons and armor, of which the 3, and later 5, Mythril furnaces churned out non-top every single day. The new armaments looked nothing like the ones the sith always used. Though slightly less durable, they each came in separate parts that were quick to disassemble and replace. In particular, parts which were expected to break often, such as spearpoints, were produed in larger quantities, in comparison to their hafts.

Roughly 10 days later, production had ramped up to 10 pieces armor and 36 weapons every day even without Letty's direct supervision. Despite this, achievement, they were still not able to meet Sylphia's order. At some point, all but Sylphia had forgotten the amount she requested; though, in the end, she had no complaints, and so told them they had met her expectations.

####

In the morning of the 10th day, out in the flatlands, Sylphia finally finished drilling the Sith soldiers.

"Company A! Forward!" 

On her orders, a neatly arranged group of 80 Siths marched in sync without her needing to cast any more magic.

"Company B! Forward!" 

Right behind the first group, a second one of the same size marched forward.

"Assault Company, forward!"

Behind the two groups, a much smaller formation of just 20 Siths, led by the Sith who tried to defy Sylphia weeks ago, marched in sync with the rest.

"All Companies, halt! Stand in attention!" 

At her command, the Siths stopped marching and faced Sylphia, standing fully erect.

Sylphia looked out to them from a steep hillside. With the two weeks' worth of work she put in to training them, a warm feeling of accomplishment started welling up from inside her. Keeping it sealed in for the last minute, Sylphia stepped forward and took in a deep breath.

"First of all, an announcement!" She said, with the loudest voice she can muster without it cracking, "From your efforts, you have finally graduated from training, and are now fully-fledged soldiers! Congratulations!"

With a synchronous bark that much resembled a war cry, the Sith cheered. In full regalia, each soldier raised his weapon of solid Mythril.

"With your training complete, the next step can finally begin. All company captains will join me in the temple afterwards. Dismissed!" Placing her fingers just beside her eye, Sylphia gave a salute.

With a sonorous bark, the dogs returned the salute by raising their stubby arms forward in salute.

/Well, they can't reach their foreheads with those short paws of theirs, anyway./

####

Sylphia returned immediately to the temple.

There, what little space available was set aside for a table, upon which rested a large map made of dried mushroom cap. Beside the map, Letty made her preparations.

"Letty, are we ready?" Sylphia said.

"Mm." Letty nodded, "The scouts just returned while you were drilling the soldiers."

"And?"

"They have found Rika and the others."

Sylphia's heart skipped a beat.

"So they're safe!?"

"Y-yes... but..."

Sylphia's heart dropped.

"But what?"

"It's strange... the report said Rika and the others were... training with the Kobolds...?" Letty rubbed her forehead. "I'm sorry, that was exactly what was reported to me... There aren't any other details, apart from the fact that they're safe."

/Training? Like morning exercises? Hmph. What a bunch of carefree people. Here we are raising an army to save them, and they're spending their days happily keeping in shape?/

Sylphia glanced at her body.

/I haven't eaten well for ages, much less have any exercise.../

She sighed.

/Being too skinny isn't very cute, either! Oh well, at least we could still bathe regularly, thanks to the river flowing nearby./

"Well, what's important is we still have someone to rescue." Sylphia's expression darkened and she started mumbling beneath her breath, "had it turned out that they've killed her, then I would have had to commit genocide..."

She grinned while furling her brows.

/Now only a massacre is necessary./

Letty cocked her head to the side.

Sylphia instantly flipped her demeanor. "Ahaha, it's nothing!"

Sylphia joined Letty by the table and whipped out Sebastian.

"Anyway, so, there are a few minor farming settlements scattered in the cavern, and all but one belong to the Kobolds." Sylphia pointed to the map with her fan. "The goal, then, is simple - we burn all their farms."

"Wait," Letty jolted. "burn? D-don't you mean... capture?"

"Nah." Sylphia waved her hand. "We don't have enough men to capture and hold all this land. Only the Kobolds do. So, we steal anything we can carry, we burn the rest, and then-" Sylphia dragged Sebastian to the one farm the Siths controlled. "We make them retaliate and - boom!" She slapped her fist into her palm. "We destroy their forces, chase them all the way back to their village, and raze the whole thing to the ground!"

"And..." Letty placed her hand on her chest and swallowed her breath. "all this is necessary...?"

"Mm." Sylphia nodded.

"I-Including destroying their village?"

"Yeah."

"I'm sorry, since I don't know much about this... but why it necessary to raze the kobold village?"

"Why, of course, it's so they can never get back up again." Sylphia said, as if it were a matter of fact.

Letty shuddered. "What do you mean?"

Sylphia sighed.

"Well, we're going into some post-war things here already, but..." She scratched her head. "Basically, I'm planning to permanently reduce the population of the kobolds. Because they reproduce so fast, if left alone, they could easily rebuild their numbers, raise an army of their own and take revenge on the Sith, right?"

"Eh? Uhm, I guess..."

"So we nip this neverending cycle of revenge in the bud. The kobold village will be razed and their population will be spread about the Sith villages as slaves..."

Sylphia placed her palm on her cheeks and started rambling to herself.

"It should be simple to teach the Sith to then control their slave population in case of widespread revolt... oh well, we can figure that out when the time-"

Letty hit the table, ripping a part of the map.

"Hey," Sylphia's brows dropped, "what's gotten into you? You ruined the map you spent all week making..."

"What... are you talking about?" Letty held tightly onto her skirt. "Slaves? Population control? Razing entire villages? Can't you hear what you're saying!?" Letty's eyes flared with anger, such as Sylphia had never seen before.

"What?" Sylphia raised an eyebrow. "Of course I c-" She sighed. "I don't understand what you're trying to say."

"Don't you...?" Letty held back her tears. "How could you not!?"

/Hmm... it's just in times like this that she's assertive? Tsk. Give me a break./

Sylphia sighed.

"Let me guess," she said, with a condescending look on her eyes, "after spending so much time with the Sith, you've come to see them as people." Sylphia opened and closed her fan. "And so, you don't like the idea of killing them, anymore."

Letty flinched. "N-no... that's not..." She averted her gaze. "well..."

"Listen - no matter how you look at it, these things are just monsters. Maybe they can speak and all that," Sylphia twirled her fingers. "but in the end, these are just monsters."

"B-but...!"

"And don't forget -" she glared at Letty. "you were the one who posted a quest to clear this dungeon."

Letty froze.

"So what's the point of regretting it now?" She shrugged. "You must have consigned countless other dungeons to death before, haven't you? What's the difference? That you're here, right now? That you can see the fruits of your actions directly?"

"S-stop..." Letty said, with a trembling voice.

Sylphia heated up. "Why don't you take responsibility for your actions for once? Look your victims in the eye and tell them: You're going to die now." Sylphia banged the table, herself. "Instead of sitting behind a table all day and signing away at death warrants for a living!"

By the time Sylphia came to her senses, her heart was pounding hard, and Letty was sobbing profusely right in front of her.

/Ah... wait, what did I just say?/

She felt a cold chill washing over her shoulder. Her face contorted and her mouth fell agape, as if she had just seen a ghost.

/Wait, why did I say that?/

"Hey." Finally, Sebastian spoke up. "That's too much, isn't it? Even for you."

/I have to say something, quick!/

"Y-yeah..." Sylphia blurted out, "that's... right."

/Something! Anything! Right the fuck now!/

"L-Letty..." 

Sylphia tried to reach out to her, when Letty turned around and ran to the exit, with tears trailing in the wind.

"Letty, wait!" Sylphia shouted.

Despite anything she said, Letty did not listen, and disappeared out of the temple.

A heavy silence filled the temple.

Sylphia breathed a heavy sigh.

"You're merciless," said Sebastian.

She ground her teeth against one another.

"Did you really mean what you just said?" He asked.

She hesitated for a moment. "I... don't know... But I wish I hadn't said it."

Sebastian sighed. "I think you're wrong."

Sylphia clicked her tongue. "I know that already!"

"Huh? Wait, no - er... I mean, yeah you are wrong in that way too, but also - I think you misunderstood her feelings."

"Huh?" Sylphia looked at Sebastian with contempt. "The hell are you talking about?"

"You can't be serious, right? Do you really think she cares so much about some monsters all of a sudden?"

"T-Then," Sylphia mumbled, "why did she react like that?"

Sebastian sighed. "Talk to her." he said, with a soothing voice. "If need be, I'll step in this time. Just talk to her."

"But the final brief with the captains is..."

"Come on," Sebastian scolded her, "do you really think you can command your troops in that state?"

She scowled at him. "Man, you're annoying." Sylphia rolled her fingers into a fist and gripped them tightly. "But you're right," she released her breath, along with the tension built up in her body.

"Then go."

"Mm."

Sylphia ran after Letty as fast as she could. On her way out, she met with the company captains, and instructed them to wait at the temple.

####

Thanks to a couple of Siths who happened to pass by, Sylphia eventually found Letty by a mushroom grove overlooking the village.

Sat with her legs tucked in, Letty laid her head onto her knees and rested alone in the field of giant mushrooms. The breeze coming from the underground stream bursting out of the cavern wall blew gently into her long, unfurled hair, swaying it along her back as it passed by.

Sylphia approached her, careful not to make a sound. She had yet to figure out what to say, although she had already long decided what she needed to do. As she closed the distance, even her light steps, which parted the blue grass below became audible to Letty, who flinched the moment she realized. Despite this, she stayed her ground and pretended to ignore Sylphia.

/What a bother, this girl's personality.../

She stood for a moment beside her, to see what her reaction would be. For the entire time, Letty did not even so much as take a peek.

/Uwah... she's really upset./

With a sigh, she decided to sit down right behind Letty, facing the other way, and letting just their backs touch.

/Hmm... still no reaction. At least she isn't angry enough to not want to be touched by me./

"So..." Sylphia said, "you wanted to say something earlier," she then mumbled to herself, "Sebastian told me."

Letty didn't react.

"I'll listen."

She jolted slightly.

For a long while, Letty kept quiet. Sylphia wondered if she was collecting her thoughts, or if she was just intent on staying angry at her, but soon enough, her question was answered.

Letty took a breath, "You're very rude, for an mere apprentice..."

/Ugh... I almost forgot, that was supposed to be our relationship./

Letty continued, "You don't listen to what your Master says... you order me around... you make a habit of teasing me... and- and-!" She paused for breath. "I'm okay with it..."

"Heh?" Sylphia looked behind her.

"But... but..." Her voice started trembling. "When I see you making scary faces... when you say such cruel things without a second thought..." she sobbed. "...my chest hurts."

/Ah.../ Sylphia lowered her head. /that's what he meant.../

"I understand why..." Letty shifted her position, placing her hands on her chest, "...but even if I understand- my chest still won't stop hurting...!"

Sylphia allowed Letty to shed her tears for a while. When it seemed that her sobbing had subsided, Sylphia finally spoke up.

"Hmm...?" Sylphia leaned backwards, laying her entire back against Letty. Then, tilting her head to the side, she whispered, "Anything else?"

Letty thought for a moment, then said, "No... that's it."

"I see."

Sylphia took in a breath, pushing her head backwards against Letty's neck. She then twirled around, pressing her chest onto her back and hanging her head onto her shoulders. 

Letty squirmed around, saying, "W-what... what are you doing?"

"You're too nice."

Letty grumbled.

"Here I thought you were worried about the monsters, and it turns out you were worried about me, instead?" Sylphia puffed out her cheek. "How am I supposed to react to that?"

Letty whimpered. "S-stop teasing me..."

"No way, your reactions are too cute." Sylphia rolled her head around, massaging Letty's shoulders.

"Geez..." The tension on Letty's shoulders dissipated, and her voice stopped trembling. "Why do you keep treating me like this?"

"Same to you~" Sylphia said, with levity in her voice. "I'm just a 'mere apprentice', why does my Master care so much if I turn rotten or something?"

"No," Letty finally turned around, her frown stretched across her face. "that's exactly a problem for me to worry about..."

"Ah," Sylphia covered her mouth. "so it is! Tehe~".

"It's not a joke..." Letty twiddled her thumbs. "besides, it's not just about being your Master... I really," she glanced at Sylphia. "really, don't want Sylphie to get hurt... someday."

"Get hurt?"

"Mm." Letty nodded. "When you get used to hurting others... you start to feel numb. And then, you find it hard to know when you're hurting others, and you can't stop yourself anymore..."

Sylphia was given pause. "That... sounds like it comes from experience."

Letty nodded again.

"An old friend?"

She didn't respond.

"I see..."

/It sounds like she's been through a lot. I wonder if that has to do with her personality...?/

Letty flinched as Sylphia wrapped her arms around her shoulders.

"Don't worry," Sylphia whispered, "so long as I have you by my side, I won't rot away."

Letty placed her palm over Sylphia's hands. "Really?" a breathy voice escaped her lips.

"Mm." Sylphia closed her eyes and nodded.

They stayed in place for a few moments, as the breeze flew past them, rubbing against their skin, sweeping upon their clothes.

Sylphia then opened her eyes.

"So uh..." Sylphia's body went stiff. "I'm very late for the meeting with the captains... can we go now?"

Letty chuckled. "Mm. Okay."

Thus, they walked hand-in-hand back down from the grove. Along the way, they came upon the company captains, hiding behind a large mushroom stem, ruminating with one another.

"Rawr." one said. "So even the Gods have their issues, huh?"

"Bowow." another one nodded.

"Arf. In a way, they are not that much different from mere mortals like us..."

Sylphia stepped in front of them. "Hey." she said, with an ominous voice as her glare pierced through them like a lightning bolt.

The dogmen jumped to attention. Each one trembled in fear in the presence of her murderous aura.

She loomed over them. "Have I taught you how rude it is to eavesdrop?" 

They shrank before her.

Sylphia drew her fan.

"If you're going to do it," she shouted, "make damn sure you don't get caught!"

With all her strength, she swung Sebastian forwards, causing him to scream out. Each Sith, she gave a big whack on the head with her fan; and each time, Sebastian squealed in pain.

After dishing out her punishment, Letty tugged on Sylphia's sleeves.

When she turned around, she was greeted with her pouting face.

/I-I guess that face is saying, "Buuu~ You just said you'd stop making scary faces!" Or something like that./

Sylphia snickered. "It's really cute when you make that face~" She poked Letty's in the cheek.

Letty sighed. 

/I guess, from now on, when dealing with Letty, I'll need to spare her the details. It isn't really important for her to know all the down-and-dirty stuff. That's what I'm here for. After all, we all live, in a way, in our own little bubbles, safe in the knowledge that we have all the answers, and everything corresponds to our own narrow worldview. That's why it's a harsh feeling to scratch that worldview, exactly because of how narrow, how fragile it is. So, for the time being, she will work on the logistics side of this war, far from the muddy reality. As for everything else-/

"Alright," Sylphia puffed her chest out. "let's go to the temple!"

/She doesn't need to know./