Chapter 29 - Hidden Amongst a Field of Clovers

At the corner of the main road and Dellwick's market street, a young woman swept the floorboards of an empty shop. It was not empty in that it had no products to sell, no - on the contrary, its displays were brimming with intricately woven shirts, dresses and other articles of clothing. But as the girl finally pushed the dirt back out, beyond the shop's front counter, she couldn't help but notice that today, as well, the shop was empty of customers.

An air of dread hovered over her shoulders.

/Ugh... and no one bought anything yesterday too... or the day before that./

With the daily chores done, all that was left for her to do was take a seat behind her stall.

And so she waited.

And waited.

At times, she couldn't help but notice the innocent glances by the people passing by, prompting her to jerk up and invite them into her shop but, as always, no one took her offer. She liked to think of herself as a strong-willed, but she could only take so much, for so long, without being disheartened.

She alternated glances between the catalogue of hand-made designer items within her shop, and the plain, rugged clothes worn by the market-goers.

"What am I waiting for...?"

/Of course no one will buy anything. I'm this far out in the sticks!/

/I just wanted to make beautiful clothes the way mother taught me... I worked so hard, travelled to many cities...!/

She sneered at herself, "Ha, what was I thinking?"

/But of course, these was never any chance that a no-name village girl could catch the eye of a noble. Most didn't even bat an eye, but some even went so far as to disparage me in public... those eyes... I'll never forget that condescending look from that self-centered stylist...!/

She clicked her tongue.

"What was that guy even wearing!?" She grumbled, as her thoughts began leaking out, "The colors clash so badly, and the style is too old-fashioned! Just because his name was a bit famous-!"

This outburst startled even those just passing by her store, attracting nervous gazes from all around.

/Eek!/

"I-I'm sorry, I'm sorry...!" she said, "Please don't mind me. Ahahaha..."

She sighed.

/In the end, I'd ran out of money for travel expenses, all without getting a single client.../ 

Tears collected below her eyes.

/I thought I could last at least the rest of the year... but ever since that tax hike, I've had to skip meals just to keep the store running for this long.../

"Uuu..." Wiping her snot with the back of her hand, she took a deep breath to calm down.

/...but I guess the dream is over./ 

She glanced deeper into the shop. A small table, the place she liked to call her workshop, stood in the far corner, lit by the small window just beside it. On it, lines of textiles, threads, and her various equipment were littered all over. Beside that workshop, hung in a dark corner of her shop, along the wall she shared with the stall next door, was a one-piece dress that gave off a vibrant purple glow from the tiny patches of light that dotted its collar.

Just gazing at it left a smile on her face.

/My last piece... I thought I would make one last thing out of the materials I had left... I didn't put any real hopes on it when I started, but by the time I was really working on it, I really fell in love with it... The style's a bit old and simple, but there's a certain beauty you could only find in simplicity./

/It's also way too small for me./

She scratched her head.

/Clothes for noble children, huh...? Damn, if only I'd thought of that route months ago... I might have found a niche client or something./

"Ha."

/As if./

While she was lost in thought, a voice suddenly called out from behind her. "Um, excuse me, miss?"

"Eek!" Her legs jerked upwards in fright, toppling her chair backwards. She was soon stopped, however, by her knee bumping on underside of the counter - which, at the same time, sent a sharp pain coursing up her leg.

/Uwaaaaah!/ She cried internally, biting her lips shut.
"Ah, sorry!" The voice said, followed by a light tap on the wooden counter. "Are you okay?"

Rubbing the back of her neck, the young woman stood up, fighting through the pins in needles. 

"Ah, no, no," she said, "that was my fault." 

She wiped away her tears and attempted to put on her usual business-face. 

On the other side of the counter stood two young ladies and a child, each in wildly different clothes. But two things sparked her interest - first, that they approached her, rather than being called, and second, that none of them wore a farmer's clothes. 

The combination screamed one thing to her:

///Customers!///

The tallest among them, was a woman with a large-brimmed hat that had a pointed tip, bent down, on its top. 

/Hoho? I'd seen that sort of old-fashioned hat before... Moyenne, was it? I've been to Rafale, but I really can't remember Raffalian names to save my life. But Brandonbury, for sure. I've tried many times to grab a foothold in that city, and I've seen the Academy's students wearing them! So this woman is a mage. Hmm... I don't really have clothes that are suited for a mage.../

Beside her was a woman with fiery red hair, tied into a ponytail that sat down on her shoulder. She she wore rather tight clothes, such that even the vest she wore over her shirt still fitted closely to her form. 

/Those muscles on this woman's thighs! She looks like the thoroughbred adventurer type... urgh, if it comes to them, they might as well be lower on the style heirarchy than farmers! There's no way she's buying anything. /

And with them was a child, standing right in front of the counter. She wore a thin white dress and a light colored hat, both of which almost sparkled beneath the sunlight.

/Hmm... a little girl? Is she with them? Her style isn't bad, but... I don't really have children's clothes.../

Her stomach knotted.

/Wait, no, no! I can't think negatively at a time like this. These people are probably travelers! If anything, this is the closest I've ever been to making a sale! Come on, me! Channel that inner merchant in you! It's make or break, and my entire life is on the line!/

"Ahem." she cleared her throat. Even while her knees were still wobbly from the lingering pain, she put on a welcoming smile and said, "Welcome to Nina and Cecil's Apparels! How may I help you?" 

She then motioned her hands to the arrangement of dresses further into the store. 

"Would you like to browse our collection? We also do custom jobs!"

The child, hardly taller than the counter in front of her, looked up and said, "A-ah... Actually, we were just going to... erm..." Her eyes then scrolled back and forth towards the dresses. "well, the dresses do look nice..." 

Her words instantly lit woman's face up. "Th-Thank you very much! These ones you see here are all made by me! If you would like, you can browse more of our collection inside." 

"Hooooh...?" The girl's eyes widened.

/Alright! Come on! I just need you to browse my collection, and surely the others will follow!/

Indeed, the young lady was about to enter the shop, but then, the mage grabbed her by the shoulder. 

"U-um... Sylphie," she said, "aren't we here to for Mr. Dante?"

Her shoulder jolted up upon hearing that name.

"Ah, that's right!" The girl then turned to the shopkeeper and said, "Actually, we're looking for someone named Dante. He was supposed to be a member of the Merchant's Guild, have you seen him?"

"E-ehhh..." 

/So they... were just asking for directions?/

She crumpled back down on her chair.

/Figures. I guess something so convenient can't possibly happen.../ 

/Tsk./

/But still, Mr. Dante? I wish I didn't have to hear that name today, much less see his face./

Letting out a sigh, she answered, "Mm. He should be making his rounds in the market soon..." 

/Though I wish he doesn't, even just for today./ 

"...you'll probably run into him sooner or later. You, err..." She rolled her eyes. "...can't miss him."

"Huh." The girl's eyes flickered. Then, her face lit up with a smile and said, "Thanks!"

She chuckled. "You're welcome, little miss."

The adventurer then said, "Well, if we're going to wait anyway why don't we go and have some honey tea over there?" She pointed her thumb over her shoulder.

The mage lowered her head, covering her face beneath the massive brim of her hat, "U-um..." she said, fidgeting with her hands on her waist, "th-that might be nice... I've never tried Dellwick tea before..."

"Is it good?" asked the little girl.

The adventurer grinned. "Real good."

/Letting three customers just slip out of my shop... I guess I'm not really cut out for business, after all.../

"Mm..." Even as she nodded, the little girl's eyes turned back to the dresses inside the shop. Then, she suddenly waved the two others away, saying, "You two go ahead, I want to take a look in here for a bit."

The woman's heart jumped. 

/R-really!?/

"Oh. Okay, then." The redhead waved goodbye and walked away.

/Is this really happening!?/ 

She held her cheeks with both hands. 

/J-just one... if she buys just one dress...! Well, it's not nearly enough to cover my debt, but maybe! But if I finally show some results, maybe, just maybe, I can ask for some sort of extension, and still make it!/

But the mage then cocked her head and asked, "Do... you have any money, though?"

The girl snickered. "No, but looking is free, right?" 

In that moment, the shop girl's heart cracked in two. Biting onto the inside of her lips so that her face would not show any rude expression, it was all she could to maintain her fake smile, while holding in the depths of her despair. 

/That's it... I'm never hoping for anything ever again...!/

She sobbed.

####

The light patter of the girl's sandals reverberated in the empty shop. Her eyes twinkled as she gazed at the rows of dresses lined up along the walls. With each step, she examined every detail of each dress that found her fancy.

The shopkeep had long since calmed down and returned to her seat behind the counter. She observed the child peruse the clothes she had spent so much effort into designing, and then making. Though she already knew that the girl had no intention of buying anything, just having someone to look at her work, and then express their delight, was enough to put a smile on her face.

Afterwards, the child returned to the front of the shop.

"How was it?" asked the shopkeep.

"The dresses are really nice!" she said, "But they're all too big for me." 

She then looked at the set hanging at the front of the shop. Running her tiny hands down its sleeves, she said, "I really like this. But it's not my size, either... I wonder, do you have anything my size?"

She chuckled. "I'm glad you like it, but I'm afraid I don't..." 

At that moment, a flash of lightning sparked inside her mind. 

"Ah, wait a second." 

She stood up. 

Deep inside the shop, past the gallery, she came to the dark corner where she left her final work. Taking it within her arms, she presented it in front of the girl. 

"How about this?" asked the shopkeeper.

"Oh!" 

She set the dress against the girl's chest, allowing it to flow down her body. "This one turned out a bit small since I ran out of materials, but for you..."

The girl's lips opened wide with a smile. "Ah, finally! And it's cute, too!" With a glimmer in her eyes, she asked, "Can I try it out?"

"Eh? T-try...? You mean, wear it?"

"Mm!"

"E-er... it's supposed to be a showpie-" 

/Well, it doesn't matter now, does it? At least someone will wear it.../ 

Tilting her head to one side, she said, "Okay, follow me."

"Yay!"

The shopkeep led the girl into the measuring room deeper inside the shop. Windows with blurred panes allowed light to filter in, while disrupting vision through them. Measuring tools were kept neatly arranged on the shelf. None of them had seen use for a long time. She soon left the girl to change, going back out into the shop floor.

/What an adorable little girl.../

She sat back down, behind the front counter and fixed a long gaze towards measuring room.

/If she was only a bit older, and we had met a bit sooner, then maybe she could have been the one to wear these dresses.../

And, as a shallow breath escaped her lips, she heard a large voice call out from behind, "Oho? Cecil?"

Just as before, she freaked out. 

"Eeek!" she screamed. 

This time, however, she was able to anchor her arm onto the counter just in time to avoid banging knee against it a second time.

The voice carried a deep timbre, and what's more - it was dreadfully familiar.

/Urk... he's here.../ 

She slowly turned around. 

In front of her stall stood a stout man in sleek, colorful robes. He grinned as he looked down on her, as though he were watching something some small animal.

"There you are," said the man, "I thought you were out or something! Hah hah hah! But I knew my dear Cecil won't be so rude as to run from her debt!"

With her head tilted as far down as she possibly could, she mumbled, "Please don't call me that..."

"Oh, sorry, sorry." His puffy cheeks folded over every time his expression changed. "Was just glad to see you. You know how it is."

Slowly, she got off her seat and faced him. Though he was much rounder than her, he was not that much taller. Had she not lowered head, they would have stood eye-to-eye. 

But she did. And while she was at it, she also mumbled a inscrutable series of ums and errs, as her mind quickly fell into panic.

Then the man soon interjected, "Well, I think we both know why I'm here, right?"

She grunted.

"As promised, 200 shillings, to be paid in full, today." He said, shoving his open palm in her face. "Let's have it."

"U-um..." She hunched forward, lowering her head even further, while rubbing her hands below her chest. "I-I..."

The man's grin only grew even wider. "Now, Cecil. You aren't going to tell me that you don't have the money I lent, are you?"

"I-I'm sorry, Mr. Dante...!" Her voice broke, as she came on the verge of crying.

"Oh, don't worry about it! Hah hah hah!"

She lifted her head in surprise. "Mm...?" 

"Look, it's nothing to cry about, right? 200 shillings."

Her heart jumped. "D-does that mean!?"

"Yes, yes! After all, gold and silver aren't the only thing of value, is it?" He lifted his arm in the air. "Like this store."

She cocked her head. "Uh... huh...?" 

"It's a good location, the property is reasonable in size... I'd venture to say that it might be worth... eh, around 200 shillings, don't you agree?"

Her heart fell. 

"Eh!? Wait, wait!" she cried, "Please, anything but that! This isn't just our shop, this is where I live...!" 

"Hmm, yes..." He tilted his head up to the second storey of the shop, where a window connected into the small room that Cecil called 'home'. "That is a bit of a pickle, but hey, what a coincidence! It just so happens that I also know someone who can rent you a room! If you tell him I sent you, he'll surely give it to you at a steal!"

"No, no, please!" She shook her head vigorously, sending her tears streaming into the air. "I can't lose our home...! Isn't there any other way!?"

"Well," He rubbed his blubbery chin. "there is one."

"Please, tell me!"

He smirked. "Well, it's simple. I'll let you extend your debt, but from now on, I'll need to charge you interest."

"Ngh-!" Cecil clutched her chest. /Interest...!? That means I'll fall even deeper into debt!?/

"So, you'll get more time, as much time as you need, in fact, and..." He swung a couple of fingers around as he calculated within his head. "...well, since I really like you, I'll only charge you a /measly/ 10% on the month." He slapped his palm on his face. "Ah! I really am too generous when it comes to young ladies! If the lads at the guild find out about this, they're certain to laugh at me!" 

He then snuck a peek at her between his fingers.

"I-I..." She clasped her hands together and pushed her head down upon them. 

/If I don't take this opportunity, he'll seize my our house for sure...! I can't let that happen! But... I don't have anything left... without even any materials to make clothes, how in the world am I going to pay for a larger debt!?/

"Hey, Cecil," he said, with growing impatience, "aren't you thinking too much about this? I think it's a simple decision to make, to be honest."

/And just what will happen to me, if I can't pay an inflated debt owed to him...!?/

"Hey, come on now, Cecil," he said, wrapping his arm over her shoulder, "I loaned you the right amount, didn't I?"

Her skin crawled from the sensation of his skin touching hers. Pulled her face away from him, she answered, "Y-yes... you did."

"And we agreed you'd pay it back today, right? That way, I don't need to charge interest."

"Yes..."

He then opened his palm in front of her face. "So, 200 shillings." 

She gripped tightly at her skirt. With a tremble in her voice, she said, "I... I don't have the money..."

"Right?" He shrugged. "So I'm going charge you interest! It's that simple isn't it?"

"Nnnnnrgh...!" She leapt away, throwing his arm back. "B-but...! I already can't pay the 200 shillings!" She shook her head. "Mr. Dante, how can I pay if it will grow every month...?"

"Come now, it's just a ten percent interest, dear! Ten percent of an apple, well, that's just a single bite, isn't it?"

"W-well..."

"See? I told you, you're just thinking too much about this!"

/W-was I...? Just worried over nothing, after all...?/ 

Her mind blanked out.

/Maybe.../

She took a deep breath. 

/In any case, nothing could be worse than losing my home!/

"I-in that case..." she muttered.

His eyes widened, as well as his grin.

"Mr. Dante... I-"

All of a sudden, a teeny voice rang from inside the shop. 

"Ah!" The girl form earlier ran out, wearing the clothes Cecil had made. "Are you Dante of the Merchant's Guild?"

Dante peeked to the side and let out a grunt. "Who's that? Your niece?"

"Eh?" She shook her head in surprise. "Ah, no, um..."

"Oh," said the girl, "and how do I look?" The frills on the skirt fluttered as she twirled around and gave a wink. "Cute, right?"

Her anxiety quickly gave way to confusion, as she was swept by the girl's pace. "Uh... yeah! It looks really good on you!"

"It does, doesn't it?" She grinned, before quickly turning towards Dante. "Anyway, You're the Mr. Dante, right?"

"Er, y-yes, I am. What is it?" he asked, scratching his head.

"I've actually been looking for you!"

"Huh...?" His eyes widened. "Just so you know, I'm not selling candies or anything."

"That's not it!" She pouted.

Cecil noted that her expression just now was rather cute.

"A-ahem...!" Dante cleared his throat. "Either way! Can't it wait!? Miss Cecil here and I are in the middle of important business." 

"Is that so...?" The girl cocked her head. "Okay. I can wait."

He sighed. "Anyway, as I was saying!" He waved his fist impatiently. "It's simple. You pay interest, you get more time. That's all that matters, right? So let's-"

But at that moment, too, he was interrupted, this time by a dark silhouette appearing behind him. "Excuse me," said a stern voice, "who is charging interest, exactly?"

"Grrrr!" he grumbled. Finally he had lost his patience. Letting loose his fury, he turned around and screamed, "What is it now!? Can't you see we're in the middle of busine-" 

But the moment he got a good look at the person behind him, he froze up and choked on his own words. 

"Eugh-!"

Behind him stood a woman with raven black hair. Towering over him,she laid down a soul-extinguishing glare, as though he were the lowest form of life that crawled the earth. 

"Huh?" Her cold tone of voice sent shivers not only through Dante's skin, but also in Cecil's.

/I-it's...!/

"Eeeeeek!" The merchant shrieked. "M-M-M-Madame Secretary...!?"

"M-Miss Secretary...!" Cecil cowered away from her presence like a frightened puppy. "G-good morning...!"

"I'll ask again," said the woman as she ringed her fingers over Dante's collar, "who is charging interest, exactly?"

"E-e-err..." Dante twiddled his thumbs.

Her brows furled. "It couldn't have been you, can it? Because as far as I know, you were still licensed as a Trader, Mr. Dante de Salvatore, not as a Banker. You know what that means, right?"

He hung his head as he tried to defend himself. "W-well, Madame Secretary...! We were simply forming a contract between two consenting-"

She whispered into his ear, loud enough that Cecil could hear her viper-like hiss, "No loans with interest."

Her face turned pale. And with a lifeless voice, he replied, "Y-yes, Madame Secretary..."

She swept her overhanging hair behind her ear as she pulled away. "So long as you understand." 

Next, she turned her hawk-like eyes at the other two.

Cecil flinched as she met eyes with the woman. Long eyelashes cut like a knife's edge across her pale grey pupils - pupils pointed directly at her. 

"Miss Cecil." said the woman. 

"Yes, ma'am!?" With a just a single word, the shopkeeper was forced into submission. 

"...you should also keep in mind the limits of the license given to the merchants you transact with. If you're unlucky, you may also face penalties for dealing with them illegally."

She hung her head. "I-I'm sorry, Miss Secretary...! I'll be more careful next time."

With a nod, the secretary's eyes then turned to the little girl. "Hm? And this is? Your niece?"

"Eh? That's the second time-!? No, she's a customer!" She puffed her cheek out and muttered to herself, "Is it so strange that I can get customers too...?"

"Hmm..." The tall woman paused for a short time, placing her hand on her chin. Afterwards, she adjusted her glasses and said, "No, I did not mean to say that."

/W-why did you have to think about it!?/

"In that case," She glanced at the little girl. "you should return to your parents immediately. It is not safe for a child to be out alone." Giving each of them one final glance, she then nodded, saying, "Now then, please stay out of trouble." 

Dante and Cecil nodded.

She then glared at the stodgy merchant once more, and said, "Especially you."

"I-I will!" He cried.

Just like that, the woman walked away, disappearing as abruptly as she came.

And in her wake, the two adults let out a sigh of relief.

"Umm... who was that?" The little girl asked, "A secretary of some sort...?"

Dante pulled his collar open, fanning a breeze with his hand into the opening. 

"Tsk. Eris, the Secretary of Dellwick..." he said, "A nasty woman, she's been lording over this place ever since the fine Lady Farleigh passed away..."

"Heeeh... I see."

"Well, anyway." Dante's eyes narrowed. "Cecil, you heard what Madame Secretary said... I guess that means I won't be able to give you any extension on your debt." A mischievous smirk surfaced on his lips. 

Cecil eyes widened.

"I'm afraid there's nothing we can do."

"Wait, Mr. Dante!"

"Are we really going to go in circles here, Cecil?" He sighed. "Sorry, but my hands are tied. Nevertheless, a contract is a contract. Is it not? I did not force you to take that loan."

"B-but...!" She clenched her fists. 

He grinned. "But I'm not cruel. So I tell you what - I'll give you till the end of this week to pack up your things."

"Wait!" Finally, she fell to her knees. Clutching at the man's pants, she grovelled, "Please don't! I'll do anything! Anything! Just don't take the shop!" 

He groaned. "Cecil, Cecil. Calm down..." He tried to pry her off, but she would not let go. "You're embarrassing yourself!"

For a while, the noise had been attracting glances from passers-by and the neighboring shops alike. But Cecil could hardly care anymore.

Thus, she screamed even louder, "I don't caaaare!"

"Oh, give me a break!"

Amidst their noisy back and forth, a third, tiny voice joined in, yelling, "Hey!"

"Ueh...?" Cecil lifted his face, wiping her tears from her eyes.

The little girl now stood beside them, with her hands on her waist.

"Huh? What now!?" Dante gumbled, "What are you still doing here!? This shop's closing! Go home to your parents already!"

The little girl's eyelid twitched. Unlike the childlike smile she had since she came, she looked at him with a straight face, almost resembling that of the secretary's. 

She then asked, "How much does this girl owe you?"

"Huh? Why do you even-"

"I said-" Her gaze sharpened. "-how much?"

"Urk-" He swallowed his breath. 

"Eh...?" Cecil's eyes flickered. /

What is she-/

Dante clicked his tongue. "At least that stopped the waterworks..." Taking a couple of steps away from them, he fixed his clothes and said, "She owes me 200 shillings. Alright? Now, scram!"

"S-shillings...?" The girl cocked her head. "What's that?"

"Ugh..." He scratched his head. "Silver coins! Damned noble brats..."

"Ah." She smirked. "Say, you're the one who trades food in bulk, right?"

"Yeah, what about it?"

"Heh." The girl's smirk turned into a full grin. "I have a proposal for you. A deal."

"Huh? What are you talking about?"

"Simple." She pointed her finger right at Cecil's face. "I want to buy this girl's debt from you."

Both he and Cecil shook their heads in disbelief. 

"Huh...!? What are you-" He scratched his head.

She giggled. "Come with me. I will show you precisely what I mean."

Cecil swallowed her breath.

/That face... that manner of speaking... is this really the same child from earlier...?/

"This is so stupid," he muttered while fixing his clothes. Then, glancing at Cecil, he added, "Fine. I'll play along for now. I think I ought to let Miss Cecil calm down for a bit anyway. We'll continue this later!"

Thus, the two of them left the shop. Along the way, they met with the two other women from earlier - the mage and the adventurer. All four of them soon disappeared down the road.

As she picked herself up, all that circulated within Cecil's mind was a haze of confusion. 

/Eh... what just happened...?/

"Ah." Rubbing the the back of her head, she whimpered, "She didn't give back my dress..."

####

That afternoon, just as Cecil was closing up shop, Dante and the little girl returned.

/Hm? Something seems odd.../

Unlike earlier that day, the stout man appeared almost submissive as he followed behind the girl, head hunched forward whilst rubbing his hands together. At the same time, he had a wide grin on his face that was unlike the one he always had - less that of a lecher, and more of a sycophant.

/W-what happened to them...?/

"So, you're Cecil," said the girl. 

"Eh?" the shopkeeper answered, "Y-yes...?"

"Hm, hm." She then held a smug look on her face while she spoke, "Can I meet Nina, then?"

"Eh...?" Her brows fell. "Um... well, Nina is my mother... and... well, she passed away. Some time ago."

"Oh." The girl's eyes flickered. "Please accept my condolences."

Cecil clutched her arm, pulling it closer to her body as her eyes took on a forlorn glint.

But the girl continued, "Well, in short, Mr. Dante and I have come to a deal."

"H-huh?" Her shoulders jolted. "Deal...?"

"Heh heh heh! Indeed!" he said, still rubbing his hands, "Miss Sylphia here has offered to buy your debt, as part of our partnership!"

"Syl-" Her eyes popped wide open. "E-eeeeeh!? Did you say-"

"Yep." The girl shrugged. "Long story short, I'm letting you keep that shop of yours."

"//EH!?//" 

The moment those words fell on her ears, Cecil scrambled out of the shop. Weaving her way around the counter, she almost fell when her foot snagged on a loose cord. Kicking it off, she then ran in front of the girl, crouching down to her eye level.

Then, clasping her hands together, she cried, "I-is that for real!?"

"Hee hee, of course!" The girl then twirled around, letting the dress flutter in the wind. "I've taken a liking to this dress. And I also like a lot of the others you made." She then placed her hand on Cecil's shoulder and, with a sly grin, said, "It would be a shame if Dante here closed you down."

Cecil fell limp. For the first time all day, or, indeed for a long time, she felt the weight on her shoulders disappear. Her heart felt light. Her body felt light. The ambient ache around her temples she had learned to ignore for a long time also disappeared. For the first time in recent memory, she felt... refeshed.

/Is this... is this really happening? I'm not just dreaming, right...?/

She cupped her mouth. "I-I don't know what to say...!"

The girl smirked. "Just 'yes, ma'am' will be fine. After all, this shop now belongs to me."

Suddenly, the weight on her shoulder returned. "W-what do you mean...?"

The warm, cheerful smile on the girl's face disappeared. In its place, a sinister grin stretched out from cheek to cheek. "In short, I'm taking ownership of this shop, in Dante's stead. Just like him, I can't offer any interest, after all. So, continuing on the thread earlier, I'm collecting on your debt. Well, since you don't want to let go of that shop that much, I'll offer you a deal you can't refuse."

"E-eh?"

"I don't really want to take your shop away from you either. After all-" She held both ends of the dress within her fingers. "-it would be a shame to let your talent go to waste. So here's my offer - let's be business partners."

"P-partners...?"

"Mm. We'll take joint ownership of the shop itself, and I'll let you keep your house on the second floor, as a bonus. And just like that, your debt will be cancelled."

"Eh..." Her eyes widened. "T-that sounds too good to be true..."

"Is it? Of course, as your partner, I'll be taking a part of the profits."

"W-well... of course..."

"But before we talk about that, it would be nice if you actually started making a profit first."

"Ngh-" She clutched her chest.

"As a start, no more taking out loans from shady people." The girl pointed her thumb over her shoulder. "Really, what did you think would happen?"

Dante chuckled. "That hurts! I must be the least shady person in this entire village!"

"Yeah, yeah."

Cecil hung her head. "S-sorry..."

"Well, you don't need worry about it anymore." She reached out her arm, and with an open palm, offered her hand to Cecil. "Right? I'm looking forward to working with you on our new business."

Those tiny hands, smooth and soft - the hands of a child, bore enough power to subdue a man like Dante, and turn him into a grovelling sycophant. The thought of it sent shivers down her spine.

"J-just... who are you...?"

"Oh, have I not introduced myself? Pardon me." She tilted her head and smiled. Giving a curtsy, she then said, "I am the Heroine, Sylphia. Pleased to meet you..."

Her lips fell agape. /He-Heroine!?/

The girl snickered, "...Cecil Cloverfield."