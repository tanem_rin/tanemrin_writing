Today, quite unlike her, Sylphia was hard at work.

"I wish!" she clenched her teeth as she yelled, "Fireball!"

She held out her hand and imagined a globe of flame bursting out from the ether. However, no matter how hard she tried, no matter how vividly she imagined, the black haze would not appear to materialize her wish.

/Shit...!/

She stamped her foot on the stone floor.

Her voice mingled the patter of her sandals as the echo bounced back and forth within the wide, open area.

She asked permission from the King to use the underground vault where she once tested the Kingdom's magical weapons. It was a place filled with nothing but bad memories, and that naturally kept her away from it. But with her discovery of a method with which to reliably cast magic, she now needed a private space where she could safely test potentially destructive magic.

Not that she has had any use for it, so far.

"Aaaaaaaaagh!" She scratched her head. "Why won't it work!?"

Despite being alone, the little girl threw a tanrum. She whipped her legs and flailed her arms, but, ultimately, none of it helped her unlock the mystery of her own magic.

It did drain her stamina, though.

And after a few seconds of whining, she was forced to calm down.

She sat down.

"Geez... what the hell am I missing?" she whimpered, "I can do something like this, right?"

She then imagined a lighter in her closed fist. It took hardly any effort before the black haze materialized its shape. And without even flicking the device, a tiny ember appeared just over her thumb.

"Are you telling me I can't power it up, or something!?"

/I can also do the megaphone and the bucket of water without any problems. But anything bigger just doesn't work.../

"Grrrr!" In her frustration, she pulled her legs up, planted her forehead on her knees and sobbed, "Stupid, useless magic! At this rate, I'll never, ever, be OP! What the hell is the point of being a heroine, anyway!?"

"Perhaps," a raspy voice suddenly intervened, "it is up to you to decide?"

"Ueh!?" Startled, her head twisted sideways like a rubber band. There, she saw the ever-suspicious figure of a man in a dark robe. "Ah... Alphonso... what do you mean, up to me?"

"Did you know that there are various myths of the Legendary Heroes?"

"Huh? Apart from the one I read?"

"Indeed. I have heard many, sung by lesser bards. None of them are published, so I cannot (completely recount) their tale, but of often, each one focused on a single hero - their personality, sometimes their accomplishments."

"Oh..."

"I still remember - the Venerated Paladin, Rheinhardt, was known as a kind and fair man, for better and for worse. It was said that he often came at odds with the Peerless Blade, Lucius, who was stoic and steadfast."

"Huh... I didn't know they had names. The myth only ever referred to them by their class..."

"I suppose they could be embellishments made by the lyricists themselves. But there still remains the impression left by the great deeds of the heroes. You have read the passage where the great Priestess held back the Swordsman's blade just as the latter was about to cut down a child possessed by a powerful demon."

"Yeah... she exorcised it away, didn't she?"

He nodded. "But it did not end there. Having lifted the demon from its host, its spirit would then be free to simply take over another body, repeating the cycle. Thus, she decided to imprison it within her own body."

"Couldn't she just destroy the demon?"

"Perhaps not. Because otherwise... she wouldn't then have asked the hero, Rheinhardt to slay her with his holy blade."

Sylphia's eyes widened. "Ehhh....?"

"Suppressing the demon's power within her holy vessel, and then being struck down by a blessed blade - perhaps that was the only way to truly destroy such a demon. Or perhaps the demon could have been slain within the child's body. Who can say? All that remains is her name, immortalized thus - Laeticia the Merciful."

"Laeti... wait, so she died!? That can't be right! She was supposed to slay the Demon King with the rest of the heroes!"

"I do wonder... you recall that last paragraph, where the heroes call out all of their most powerful skills to slay the Demon King?"

"Uh..." She averted her eyes. "Yeah..."

"Did you notice that each one spoke aside from the great Priestess?"

She jolted. "Eh?"

"A fascinating coincidence, isn't it? Of course, the accepted explanation is that, as a gentle heroine, the Priestess had no skills tailored to inflict injury, even on the Demon King." Rubbing his beard, he then snuck a sideways glance at the little girl and with a smirk, said, "But I do wonder..."

Sylphia winced. 

/So you're telling me that hero myth I've been referencing for my studies might not be entirely true, after all!?/

"Geez..." She scratched her head. 

/Though I can at least attest to some of the skills it mentioned./

"So, Miss Heroine... what do you think?"

"Huh?"

"What sort of legacy do you wish to imprint upon history?"

"Ah..."

/That's right... being the heroine, I guess it's already decided that my name is going to be told in myths a thousand years from now.../

She crossed her arms as she circulated those thoughts in her head. And slowly, her shoulders fell and fell, as though an ever increasing burden weight upon them.

/A thousand years.../

/That's.../

/...sort of lonely, isn't it?/

/By that time... will this world still be as it is now? After all, the invasion of the Demon King is like a cycle, isn't it? And when the cycle restarts, and another generation of heroes are called... will I be forgotten then?/

"Hey..." she muttered.

"What is it, Miss Heroine?"

"Are... there any records of the heroes... before the last one?"

"Hmm...?" He raised an eyebrow. "You mean the ones from a one-and-a-half millenia ago?"

She clenched her teeth.

/When you say it like that... I guess the answer's pretty obvious./

Alphonso sighed.

The old man approached her, his steps echoing against the stone walls of the underground vault. 

Then, he knelt down, and pulling her chin up, said, "Miss Heroine, please don't make that face." 

She froze up. It was her first time seeing it, after all - a kind smile from the man she always referred to as 'geezer'. 

As their eyes met, he let go of her chin and, with a low, breathy voice, said, "It is the same for you and for me. The time will eventually come for us to be forgotten. It is simply a fact of life..." He then placed gently over her head and rubbed it back and forth. "...But while we are here, I believe we ought to make good use of that time."

"Good... use? How?"

"Hmm... that is a difficult question. Ultimately, it would differ from person-to-person, but I believe it matters not how humble the deed you accomplish. If you do it to your satisfaction, and to those around you, then you will certainly leave an impression that will last a lifetime."

"A lifetime..."

"Mm. Of course, it's nothing compared to a thousand years... but could we, who only hold one life, truly ask for anything more?"

Her eyes flickered.

/Somehow... it makes sense./

Finally, Alphonso stood up once more and asked, "So, Miss Heroine, what legacy do you wish to leave for the next generation?"

"Huh..." She considered it earnestly, and after a few moments, she arrived at her conclusion, muttering, "I want to be be remembered as the cute one..."

The old man paused. "...hm?"

"A-ahh, nothing...! Err... I guess I haven't decided yet?"

"Ahh..." He let out a sigh of relief. "Perhaps that is to be expected. It has been no more than a few months since you came to this world, after all. There is still so much for you to learn and experience."

"A-ahh... Mm. Let's go with that." In an attempt to change to the topic, she then asked, "A-anyway, what brings you all the way here, Alphonso?"

"Oh! Indeed, I'd nearly forgotten." He cleared his throat. "Miss Heroine, His Majesty, the King, informed me that part of your spoils from subjugating the Dungeon in Dellwick was a stash of Mythril items, is this correct?"

"Huh, ah... yeah? Sort of."

His eyes narrowed. "And is it true that you have been selling these items to the local merchants?"

/Eek!/ Her shoulders stiffened from his gaze. 

"I-is that... bad?" she asked.

"Of course not. It is no one's business to tell an adventurer what to do with their spoils, after all."

She let out a sigh of relief. /Thank goodness./

"However, I am quite disappointed that you did not inform us first."

"A-ahh..." She rubbed the back of her head.

"After all," he smirked, "I would have been willing to buy every last piece that you can give me."

"Heeh..." Her mind, still stuck in a defensive posture, took some time to process his last comment, but when it did, she exploded, "Eh!? Seriously!?"

"Of course. And I would have bought it at a favorable price, too."

/But all we sell outside are accessories... does he have a daughter or something?/

"Ehh..." She scratched her head. "I didn't know you were that much into jewelry..."

"Oho...? I see." He nodded. "So, as I expected, the mythril artifacts you recovered were of that nature..."

Sylphia winced. "W-why...? What about them being jewelry?"

"Mythril is, of course, rare to the point of being mythical. Hence, the name. But there was an era... or, I should say, an Empire that once thrived in it."

"An empire full of mythril!? Such a thing exists!?"

"It did..." His eyes took on a distant gaze. "...but it has long since crumbled into nothing."

"Ah..."

/That was a relief... I almost thought that I was going to have competition in mythril sales.../

"But my research on that empire has uncovered an interesting use for Mythril."

"Hm?" Her ears twitched. 

"It seems Mythril can be worked in such a way that it conducts magic very well. I've unearthed some texts, describing of powerful mages wearing shining bracelets that could annihilate entire armies."

"A-armies..."

"I do wonder if there were certain embellishments made on its effect, but the description of the accesories remained the same, throughout - it glowed a vivid, golden-green hue and the mages wore them around their wrists. In other words, a bracelet. Almost none identified its material by name, but recently, I came upon an ancient war report, doing just that. It called them by the name 'Muistros' - old Empyrean for what we now know as Mythril."

/I see... the description certainly matches the Mythril I know... but we've used plenty of magic in the cave and I never noticed the mythril crystals reacting in any way./

"Tangential as the link may be, it is the only lead I have on my ongoing research. Thus, Miss Heroine, please forgive me for interrupting your studies, but I wish to make a formal request here: Please allow me to buy as much Mythril as you can give me."

"A-ahh... I see... sure, we can talk about the details later..."

"Hm! Very well!"

The old man looked quite pleased behind that dark hood, certainly another first, as far as she was concerned.

/So he has other sides to him as well, huh...? I guess I've been so used to getting lectured by him that I've never really noticed./

She let out a sigh of relief.

Somehow, she felt a little more at ease with Alphonso, from that day onward.

####

















*Sylphie will sit down girly like and terrify herself.
*She's starting to forget who she used to be

*Letty asked her why she had to burn the farms.
*She couldn't answer back then, but reveal that she'd always been just a little fucked up.

*She will finally ask Alphonso what happened to the heroes after they slew the Demon King.
*Unfortuantely, Alphonso doesn't know. Their story ended with the death of the Demon King. Perhaps it is an indirect way of expressing the fact that theiy perished during the battle? Or perhaps, after they slew the Demon King, they all were unsummoned? Who knows. All he is sure of, is that there is only the grand magic of summoning heroes. None to return them to their own worlds.

-> Truth is that the heroes forever remained in the world. And only the Elf in the Empire knows that the heroes went on as Kings of the various precursor countries and died here.

The Human and Dwarf King was envious of the Elf King's immortality. They thus waged war to kill him.