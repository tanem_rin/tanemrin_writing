Chapter 18 - Red Dusk

Perched atop a sturdy branch of a lone Raffalian Oak, a masked woman let out a yawn. Her leg swayed down one side as she looked out into the distance. She traced the winding road into the distance until it disappeared into a fortified gatehouse. It was the entry to a large town, set beside a row of mountains, which, at dusk, such as then, became the essence of beauty, as the sun nestled perfectly in its bosom, before vanishing.

There, she waited. For hours and hours, she observed the distant specks coming and going along that road. She perused each one using the device in her hand, which granted her the eyes of an eagle, whenever she peered through it.

She snickered. "This thing is pretty useful. I wonder where Alphonso got it?"

The wooden device came with a tapered conical shape, with the small end featuring a polished glass eyepiece. 

After a while, a certain carriage attracted her attention. Among the ones she had seen that day, this one exuded a particularly thick air of luxury. A single white horse, a rare coat among the breeds in Rafale, drew the vehicle along the stone-paved road. But what most piqued her interest was the heraldic sigil it bore on its body. She waited until it was close enough to distinguish its finer details and then, she confirmed it.

"Bingo."

It was the image of the two-headed snake - the official symbol of the Crotale clan. Her target had arrived.

Like a jungle cat, she leapt from branch to branch as she made her descent. Despite her speed, she still made a light landing, in front of a man, whose face was hid beneath a black hood and cloak.

"Is it time?" he asked.

She grinned. "The snake enters the pit."

####

The village of Renne was home to the crossroads between three important cities in Rafale. For many, it was known more as a road with a village built around it than vice versa. Nevertheless, its prime location made it the trading post of lower-rung merchants who couldn't muscle themselves into the adjacent town.

Despite its relative prosperity, the village was protected by only a few guards, most of whom patrolled its streets at night with nothing but a sword and a torch. It was, after all, in what was considered a safe territory, and despite being ringed with only a flimsy fence, the village had suffered nothing more than small bands of rogues, terrorizing the trade routes.

And so, tonight as with all nights, farmers and merchants alike littered the streets; one to meet the end of another hard day's work with drink and merriment, and the other, to sell said drink and merriment. 

But in their wake, they cast a long a shadow. Guards regularly left their posts to join with their friends. Often these were the older, more experienced ones, leaving the greenhorns to do the actual patrolling.

It was in this shadow that the pair snuck themselves in. 

"Is everything set?" she whispered.

"Yes, boss," he answered," they are just awaiting our signal."

She stood on her tiptoes and looked out onto the street. It was a wide intersection - the very heart of trade and diversion for the entire village. Shops and stalls lined all its corners, attracting the customers by the hundreds.

The woman's eyes turned to a few inconspicuous figures surrounding that intersection: 

A travelling merchant, selling tools for farming and hunting.

An old woman sat on a wooden chest, waiting. 

And a beggar, draped in rags, stretching her hands out to anyone who passed.

She smirked.

After a while, the canter of the carriage horse came into earshot. 

It soon slowed to a walking pace. The villagers swept out of its way, parting the packed intersection, causing the crowd to bunch up along the sides.

"Alright, let's go," she said, lifting a loaded crossbow from beneath a mound of grass.

"Yes, boss." The man quietly drew a large dagger.

Finally, they emerged from the shadows and approached the crowd lined up along the street. 

The woman aimed her crossbow at the carriage horse as it passed and waited.

Meanwhile, the man crept up behind a guard idly watching the carriage pass by. His victim was in the middle of a yawn when, in a lightning-fast movement, he slid his dagger into the young man's back, cupped his mouth and dragged him into the ground.

In that instant, the woman shot the crossbow bolt right into the side of the horse.

A scream.

The horse recoiled from the hit, rearing itself up on two feet and letting out a frightened squeal.

The man then drew a large, curved blade and slashed the villager before him, who had noticed his earlier deed and cried out. He then took the guard's torch and threw it at the thatched roof of the house beside him.

Meanwhile, the carriage driver lost control of the horse. No matter how he pulled on its reins, the frightened horse, now foaming at its mouth, kept pushing forward and trampling over villagers before it. It went on a rampage before, with a deathly squeal, it thrashed violently and fell over on its side. It pulled the entire carriage down with it, breaking its wheels and tipping it entirely onto one side.

At the same time, a large brawl erupted in the tavern. Chairs flew out its windows, crashing onto the road outside. Men soon poured out of the tavern, and so did the violence. Amidst the chaos, the travelling merchant handed them his tools to use as weapons. Before anyone knew what was going on, his hunting knives were used to slash at human flesh and his timber axes, to chop at bone.

In an attempt to quell the violence, one guard ran forward, with sword drawn. He did not notice however, the beggar behind him, draw a blade from beneath her rags. He hardly had the time to yell at the men to stop before the beggar shoved her blade into the his back. 

Noticing this, yet another guard stepped in and attacked her. He had just brought his sword up when a crossbow bolt pierced the side of his chest.

The old woman across the street, now sat behind her open chest, pulled another bolt from inside and loaded it into her crossbow.

Panic spread.

Soon, multiple plumes of smoke rose into the sky - not only from the house that burned at the intersection, but from faraway parts of the village.

"Boss," the man said, "the bandits are attacking. Time is short."

"Tsk. They're early. Just how badly did they want to sack this place? Let's hurry!" She removed her mask and her black cloak, revealing a set of peasant's clothing underneath. Then, she ran out to the road and yelled at the top of her lungs, "Bandits! Bandits are attacking! Everyone, escape into town!" 

Following her voice, the frightened villagers ran down the road, carrying only whatever they can fit in their hands. Meanwhile, the man faded back into the shadows, and using his torch, set the alight the surrounding houses.

Next, an old woman passed by her and said, with a much younger voice than her looks would suggest, "Lady Aster, I shall prepare for our escape." Notably, she now wore a different set of clothing, and the chest she sat on had been set ablaze.

She nodded.

And just like that, the old woman melded into the crowd and made her way out of town.

Screams and wails filled the air. In their haste, some fell on the road, only to be trampled underfoot by others. Children were even separated from their parents while each made their mad dash to safety.

Even as the night grew deeper, the conflagration lit up the streets as though it was late afternoon. 

Aster stood in the middle of the intersection and took in the stench of burning wood.

/Ahh, the smell of a job well done.../

The carriage door blew open with a loud crash. The first to climb out of the overturned carriage was a young man. He pulled an older man up before the two then pulled a young woman out with them.

/Ah, time to wrap this up./

The three of them leapt off the wreckage. 

"My Gods..." said the young man, with a gasp, "what in the world happened!?" He held his sword and shield close to him as he looked around.

/A swordsman, huh?/

The young woman, garbed in a white robe bearing the sigil of the Goddess of Life, followed closely behind. She grasped at her staff as she looked on in terror at the carnage around them.

/And a cleric, I suppose. A typical combination./

The old man laid his hand on the swordsman's shoulder and said, "Boy, we need to get out of here, right now." He was dressed in a collared shirt beneath a tan vest, both of intricate design. Hanging on his waist was a short sword, resting in its scabbard.

/And here he is./

"Huuuuh?" Aster playfully said, keeping her hands behind her back, "Leaving so soon? Won't you stay a while and keep me company?"

"Huh?" The swordsman said, "What are you talking about, miss!? You should escape alre-" He was about to walk towards her when the old man pulled him back.

"Boy." The man's brows furled. "Look."

"Huh?"

She giggled. "What, this?" She then pulled out the dagger she hid behind her.

The young man flinched. 

"You..." The old man said, "you're the scum that's behind all this, huh?"

"Mmm..." She tilted her head whimsically from side-to-side. "Maybe?"

The young man clenched his teeth. "You... you monster! Look at what you've done to this village! What the hell are you!?"

"Hihi." She shrugged. "Why, I'm just your garden variety bandit... or maybe arsonist? Hihihi." 

"Bandit?" The old man raised an eyebrow. "An assassin, more like." 

"Hoho, so you've heard of me?"

"Not before I left Londinium, that's for sure. But now that it's come to this, my employer was kind enough to hire some trustworthy adventurers to stay by my side."

"Adventurers, huh?" She giggled. /So you haven't heard of me./

The young man turned pale as he listened on their conversation. "S-sir Greyland... what's going on? Londinium? Why are you being chased by an assassin!?"

"What, you didn't know?" Aster shook her head. "Do you just go around the quest board, look for the highest reward in your class and go for it? Tsk. Tsk. Tsk. Adventurers these days are really wishy-washy, aren't they?" 

"What did you say!?"

She then entered a fighting stance, holding the dagger in front of her. "Well, it doesn't matter." 

"Martin, be careful!" The cleric said. Then, with a short chant, she held her staff towards the sky.

"Ah, thanks!" said the swordsman as he held his sword and shield at the ready.

/She cast a buff, huh? Even if I can't see her magic, I already know who she's supporting. There's just no subtlety to these two. You can tell they've only ever fought against brainless monsters their whole lives./

"Don't worry, Jeanne." He grinned. "With you by my side, I'm not going to lose to some two-bit lowlife!"

"Hmph." The assassin snickered. "Look, I don't really have any business with either of you, so if you get out of here right now, I don't need to kill you. Right away." She then lifted her blade, pointing it at her target - Ernest Greyland, Former Guard Commander of Ealdton.

In response, the swordsman dug his feet into the ground. "Not happening," he said with eyes ablaze, like the surrounding village. 

/Of course you wouldn't./

"Hah." The old man drew his sword. "Don't think I'm just a frail old man. I didn't get to be the guard commander for nothing! I don't care how good you are, you can't possibly beat three of us."

"Mr. Greyland... Alright!" The young man psyched himself up. "We'll make her answer for what she did to this place!"

"Martin!" The cleric readied her staff.

"Mm! We can't lose!"

Aster sighed and shrugged. "At least the kids are as stupid as ever..." Then, she lowered her voice and muttered to herself, "...just like we were." 

At the corner of her eyes, the cloaked man waited in the shadows, his blade primed and waiting for her signal. 

/Right, let's do this in one go./

"What, not going to attack!?" the swordsman taunted her, "In that ca-"

/Now./

She blasted forward.

The irregular timing caused him to flinch. He raised his shield in reflex and braced for Aster's attack.

/Big mistake./ 

In that moment, Aster's subordinate leapt out of the shadows and charged in with his curved sword. He zeroed in on the turtled swordsman, aiming to slip his sword on his unprotected side.

However, Greyland noticed the attack and leapt in, blocking the attack with his own sword. "Look out!"

With a mighty clang, their weapons collided.

"Sir Greyland!" The cleric squealed. Then, with another chant, she raised her staff and pointed it towards the old man.

Aster clicked her tongue. 

/Plan B./

Using her momentum, she leapt up and drove both her legs into the swordsman's shield. To her surprise, however, he took the entire weight of her attack and not collapse.

/Damn! Endurance buff!? Plan-/

"Gotcha!" The young man threw his shield down and lunged at Aster, who lost her footing in that instant.

/Ngh-/ Her eyes met with the cleric behind him. In an instant, she then shifted her weight around, grabbed a knife from her back and threw it at her.

"Jeanne!" Seeing this, the swordsman quickly drew his blade back and swung it at the knife, deflecting it just as it left her hand.

Aster fell to the ground and used the momentum to roll nimbly away.

Thus, the assassination descended into a stalemate. The two assassins faced the two swordsmen, supported by the cleric's magic.

The lady assassin let out a breath. Then, wiping the sweat off her forehead, she said, "Journeymen, huh?"

He smirked. "You underestimated us, didn't you? Why don't you two drop your weapons and give up? Then we don't have to kill you. You'll probably hang, though."

Her eyes narrowed and her lips curled upwards. "Don't get carried away, boy."

The sounds of shattering glass and splintering of wood approached from the distance. The trickle of escaping villagers had long stopped, and only they were left in the intersection, stood amidst a ring of fire.

She sighed. "This is taking too long," she said, stretching her arms and neck around. 

"I agree, let's end this," he replied.

The cleric started chanting once more.

He bent his legs down and prepared to charge. 

"Leave the other one to me!" said the old man.

"Right!"

"Hmph." Aster waved her finger, beckoning at him. "Come on, then." 

Unfazed, he stood his ground. "Sir Greyland."

"Mm." The old man nodded. "On three. One."

"Two."

"Three!"

They lunged forwards. 

On her face appeared a mischievous grin. Her feet swept across, kicking a wave of dirt up and into the swordsman's face.

Unfazed, the young man kept charging. "Oaaaah!"

/Idiot!/

"Martin!" The young woman yelled. All of a sudden, the screen of dirt Aster had just kicked up blasted into her own face.

"Wha-" Her eyes seared in pain. "Aaauauaaah!" She fell back, rubbing her eyes with both hands in desperation. 

/Repel!? That's not a spell a cleric uses!/

"You're mine!" The young man screamed, his voice looming closer and closer.

/Shit! I have no choice...!/

She grasped the blade tucked in her belt and, fighting through the pain, opened her eyes just enough for a tiny squint. At the right moment, she then swung the blade overhead and met his blade.

The moment the two blades collided, all that could be heard was a sustained ring. 

For a single moment, the world around her was washed of all color. Everything was a monotone grey. Unliving. Unmoving.

The world had stopped.

Even the pain.

All that moved was her and anything that her blade touched directly.

In that split-second, she had just enough time to parry the sword away and thrust a kick into his flank.

And then, everything moved once more.

The swordsman fell screamed in pain as he lost his balance and fell aside.

"Martin!" the cleric ran after him and laid her hand on his shoulder.

"What... was that!?" grunted the young man.

Meanwhile, Aster, who had just about collected herself from the pain, stood before them. "You bastard." Her eyes, still bloodshot, glared in deep contempt at the two of them. "You made me use it..."

As she said that, a lock of her jet-black hair appeared to wash away its color, turning into a snow white. That same time, the blade in her hand, which resembled a fang set within a handle, glowed a sickly dark purple.

"What the hell... is that!?" said the swordsman.

"A cursed blade. One that trades time... for time." She said as she ran her fingers down the white lock of hair running down her face.

From now on, there would be two separate lines of white hair flowing down her head.

"I've wasted enough of my life on you." She furled her brows.

With a jolt of his shoulders, he muttered, "Jeane... get back." 

"Boss!" The voice of a young girl came from behind. The beggar from earlier then leapt out from behind her and threw a crossbow into her hand.

She caught it in mid-air and, flipping it around with its own momentum, aimed it right at him.

"Guh!" Seeing this, he knelt down and hid his entire body behind his shield. 

Aster grinned. She then pulled a bolt from her belt and inserted it into the cocked bowstring. Finally, she aimed slightly to the side and pulled the trigger.

####

The crossbow made a loud, mechanical noise. The tension in its bow and string, launched the bolt into the air, leaving a high-pitched 'fwip' in its wake. What followed, however, was not the sharp thud of the bolt lodging itself into the wooden shield, but a soft blow, almost soundless, that just barely rose above the crackling of the flames.

"Wha...!?" Martin peeked out of his shield, only to see the assassin's smug face. "Huh... some assassin you turned out to be. Missing at this distance with a crossbow?"

"Hmmm...?" She cocked her head to the side. "Did I /really/ miss?"

"Huh...?" He raised his eyebrow at the suggestion at first, but the more he processed her words, the more he began to derive its meaning.

His heart raced.

He turned slowly to look behind him as if dreading what he was about to see. 

With every ounce of his being, he wished that it was not so. 

He wished instead that the woman was just bluffing to divert his attention. He knew he was falling for it, but he could not stop himself. Placing himself at a disadvantage was fine, so long as he could make sure that it was, truly, just a bluff.

But it was not.

Jeanne fell to the ground, her pure white robe stained with a patch of crimson on her abdomen.

"Jeanne!" His voice cracked. 

Against everything he had ever learned from his years of adventuring, against his every instinct as a swordsman, he turned his back on his enemy, and ran.

"Jeanne!"

He threw his sword and shield on the ground to pick her up from the ground. "Jeanne! Jeanne! Say something!"

Her eyes were closed, and her breaths were shallow. Atop of that, she was burning up, and sweat poured down her from her forehead. But nonetheless, she opened her eyes. "Ma..." Despite her weak voice, she forced her words out. "Mar... tin..."

"Jeanne!" Even as he trembled, his lips still formed into a smile, unable to control his joy that she was still alive.

"Mar... tin..." A single teardrop ran down the corner of her eye. "I'm... cold..."

"Jeanne...!?" He choked. "H-hold on!"

The thin strands of her light robe easily gave way as his hands tore them open around her wound. With bandage and magical herb in hand, he started with cleaning the blood spilled over her abdomen. 

However, when he did get a clean view of the injury, he was only plunged further into despair.

The skin turned black around the wound. A sticky, clear liquid coated the bolt up to the exposed bit of its shaft.

He trembled. "N... no!"

"Mar... tin... I can't... feel-"

"Shh. Shh... Jeanne." He placed his forehead over hers. "You need to rest..." And has he pulled away, his tears dripped onto her cheeks, mixing with her own.

The voices around them sounded distant. Almost a blur.

He swore that he heard Sir Greyland calling for help, or something of the sort, but couldn't quite make it out.

His mind was a haze, as he stared blankly at the girl laying at her knees, unable to do anything for her.

All he could do was hold her hand and feel her warmth slowly slip away.

Soon, he heard footsteps approaching.

"Ahhh! Job's done!" A woman said.

"Good work as usual, Boss." said a man with a deep voice.

"Mm. Good work, everyone."

"Boss," A young woman said, "what are we going to do with them?"

The footsteps stopped close by.

"Well," the woman said, "the girl's gonna die in a short while."

Martin clenched his teeth. 

"You...!" He then grabbed his sword off the ground and spun around. "Bastaaaaaards!!" 

He flailed it the assassins gathered behind him. 

"Oohoohoohoo!" The woman reacted in amusement as the three evaded his every attack. "Feisty! I like that!"

"You bastards! You bastards! You bastards!" And he just kept going. Propelled less by skill, and more raw brute rage, he brandished his blade left and right, and each time, the assassins dodged, sneering, taunting.

After some time, the woman let out a satisfied sigh and said, "Alright, Lil. I'm bored."

"Yes, boss." When he next swung his blade at the young girl, she parried his blade, grabbed his arm and wrenched it.

"Graaaaahhh!!" Pain gushed around his arm, turning it limp, and dropping his sword onto the ground.

The girl followed up by kicking him twice once in the chest and another in the abdomen. Once he then hunched forward in pain, she gave him an axe kick on the back of his head.

It sent convulsions throughout his body as he fell flat on his face.

The woman sighed. Crouching beside him, she then pulled his head up by the hair. 

"See, this is why Adventurers should just stick to hunting and dungeon quests," she said. Then, she leaned into his ear and whispered, "Because there is no monster out there more dangerous than a human." 

His entire body trembled as a cascading (waft) of fury boiled up from his guts. Clenching his fists, he pushed himself off the ground, and despite the pain, faced the assassins before him.

"I don't need you to tell me that!" he snarled, "Who the hell wants to become an adventurer, just to be some old man's bodyguard!? But what choice did we have!? Dungeons are too much for a two-man party, and the hunting quests pay like shit!"

With a sigh, the woman said, "Lil, slit his throat already."

"And the girl?" the young girl asked.

"She'll die on her own. But for the moment, she'll be a nice gift to our friends, won't she?"

Martin snapped. "Ffffffuck.... youuuuuu...!" With all of his remaining strength, he climbed back to his feet.

"Ho?" The woman cocked her head. "Rang. Give him his sword."

"Yes, boss." The large man in the cloak thus kicked Martin's sword back to him, landing it by his feet.

"Looks like our little toy still has a bit of fight in him. Lil. Play with him, won't you?"

"Yes, boss." With a blank stare, the little girl drew her blades and faced Martin one-on-one.

/Jeanne just wanted to become an adventurer... to travel the world, see new places, challenge herself! That's why I worked so hard to become a swordsman - so I can protect her!/ 

/She used to be such a frail girl. She looked down on herself. She couldn't talk with other people. She would often shut herself in her room and read books all day!/ 

/That's the kind of girl she used to be!/ 

/That's the kind of girl I want to protect!/

/She's the girl I promised to protect!/ 

"I..." He growled. "I won't forgive you!" 

"Hm...?" The woman before him raised an eyebrow.

/Shout! My soul! Shout!/ 

"I won't forgive you!" He dashed forward with his sword. "I won't let you-"

"Hup." With a single movement, the young girl parried his sword with one blade and stabbed his stomach with the other.

A sharp pain coursed up and down his spine. In his anguish, his knees finally gave up.

He stared into the girl's eyes - a bottomless pit, devoid of all emotion or compassion.

And then, just as he was about to open his mouth, he felt a cold streak across his neck.

"Jea-" His throat filled with blood. "-nne..." 

His sight blurred.

From that point, his voice would no longer come out. It hurt every time he breathed, but even then, he wanted to speak her name, just one more time. 

"Jeanne-" Blood dribbled out of his mouth as he finally fell onto his side.

As if in (defiance/to make fun of) his loneliness, his grief, the last things he heard were the jubilant voices of those who took everything from him.

"Alright!" the woman said, "Well done, guys! Let's go home!"

Their voices blurred away into his mind as his consciousness faded.

All sensation soon left his body. 

All there was, was the feeling of drowning.

But even in his final moments, his thoughts still went to the kind, gentle girl he was leaving behind. 

As he closed his mind's eye, he heard, or thought he heard, that same girl calling out to him. 

"Martin..." she said.

And all he could do, was shed one last tear.