Chapter 15 - The Temple of the Death God

Letty walked behind the sith carrying Sylphia on its head. That climbed to the far side of the cavern, up a steep rock face that resembled a mountain in sheer height. Cutting down its flank, a gushing stream of water erupted out of the wall, crashing far down into a misty pool below. The water then flowed into a riverine formation that twisted and turned across the cavern's landscape. The closer they came to this waterfall, the stronger the gust of wind that buffeted them.

/This... is incredible.../ Following the line of siths marching before her, the scribe gawked at the unique geological formations occurring within the cavern. Her mantle fluttered along with the wind, just as the grass beneath her. All the while, she held on to her hat, lest it be swept away.

So captivated was she that, for the moment, she forgot everything else, including their dire predicament.

/What is this place...? I've never seen, heard or read of anything like this! Is this still part of the same dungeon? I... I don't understand... but it's beautiful./

The dirt path they followed, nothing more than a line of trampled grass, led them across a rock-bridge, where the water had carved deep into the soil, leaving a narrow arch over and across the water below. Eventually, they passed beside the waterfall itself. Shrouded by the mighty girth of the rock face, the wind died down as they continued further up. And soon, they arrived at a settlement built upon that very rock face. Huts, fashioned from giant mushroom caps, dotted the steep incline, connected to each other by paths winding left and right, following a shallow ascent.

She gasped. 

She had seen nothing like it before. Dozens, if not hundreds of siths populated what appeared to be a village made entirely of monsters.

/This many monsters, in one place!? What in the world have we gotten ourselves into? This... this is far beyond even a Master-level dungeon.../ She shook her head violently. /No... this isn't even in the realm of a dungeon anymore! Should this many monsters run rampant in the countryside, it could easily be a kingdom-wide catastrophe!/ She turned anxious eyes towards the surrounding monsters. /And their equipment... siths and kobolds are both known to be clever beasts. Some have been reported to fling stones and use primitive tools for combat, but Mythril!? How could mere monsters have made such things!?/

They soon passed what appeared to be a gate fashioned from giant mushroom stalks and entered the village proper. Though the village siths mostly kept to whatever they were doing, the girls did attract the odd glance from a few siths.

But their journey was not yet over. They climbed up the steep paths further up the village. At that point, the uphill journey had exhausted Letty, and a couple of siths had to help her along to keep up the pace.


The dogmen dropped them off in a musty hut, no more than the cap of a giant mushroom, turned on one side and fashioned into a wedge-shape to keep it standing. The house came with no windows and only one entry, guarded by well-armed siths.

And so, they waited, not knowing what would happen next, not able to do anything about it. Beside her, Sylphia sat over to one side in solitude. 

Hours passed with nothing happening. Or so she thought. Without the sun, or, indeed, the passing of light to dark, it was impossible to tell what time it was. Mythril crystals illuminated everything in their spectral glow, all the time.

All this time, Sylphia neither moved nor said a word. She just sat there, curled up with her knees tucked into her chest, and Letty was getting worried.

"A- um..." She tried to say something to soothe the little girl, but words would not leave her tongue. 

/What do I say...? That it's okay...? Even I don't believe that... But I can't believe that it's hopeless... or else-/ She bit her lips. /All that's left is to give up./

She shook her head. /No! I can't give up!/ Then, with a big slap on her cheeks, she stood up. 


/And I can't let her give up, either./

She loomed over Sylphia. And though her footsteps should have been audible, the girl gave no reaction.

/I need to do something!/

No matter how hard she tried, she couldn't find any word to say. Thus, she acted.

Without saying a word, she knelt down, wrapped her arms around Sylphia and squeezed her in her arms.

/I'm sorry, this is all I can do.../ 

Her mantle fluttered down and covered Sylphia within.

/Please, say something.../

Each felt the other's warmth penetrating their wind-chilled skin. It permeated through their body like a wave, travelling from head to toe.

/I can feel her breath.../

After a short while, Sylphia's head drifted down onto Letty's chest. 

/Thank goodness./ 

Eyes closed, the little girl rubbed her head side to side, like a kitten. In response, the young woman sat down, pulling the girl's head gently onto her lap.

She ran her fingers along the girl's hair and smiled. /It must have been traumatic for her to lose Miss Rika. I don't really know what I can do for her apart from this, but for now, I want to comfort her as much as I can.../

The soft strands caressed her palm as they parted from her touch. Each one took a soft silvery hue, but a faint shade of pink emerged when they clustered together. /What pretty hair... Christa said she was actually 19, right? I wonder if that's true. Right now, she looks nothing to me but a little girl.../

Slowly, her own eyes became heavy. The strain on her shoulders loosened, and her head tilted downwards. Lured in by Sylphia's quiet snoring, Letty, too, fell into slumber. 

####

Sylphia opened her eyes. 

At first, all she could see was a blur. But as her consciousness roused from its slumber, her surroundings began to take shape. /Eh?/ She turned her head around and felt a soft sensation at the back of her head. /Ah./ Once her body had fully awoken, she realized - she was lying down with her head on Letty's lap. 

Before her, she saw Letty's sleeping face, complete with a line of drool dripping from her mouth.

Shr snickered. /So she's the type who drools when asleep, huh? How cute./ She then reached out her hand to the girl's face. Letty's warm breath washed over her hand as it inched closer. She took care not to make any jerking movements. And, in the final stretch, she tucked all her fingers in but one, which she poked into the sleeping girl's cheek, saying, "Poi."

Letty jolted awake. Slurping her drool back up, she looked around in a daze, and grumbled, "Eh? Ah? Whuh?"


Sylphia chuckled.

"Uhh...?" Letty rubbed her eyes. Hen, looking down, she met eyes with Sylphia.

"Good morning, I guess?" Sylphia said. "Don't know what time it is, though."

A puzzled expression clouded her for a moment. She let out a shallow breath and said, "Good morning... Feeling better?"

"Mm... Thanks."

"Thank goodness," she said as she drew a warm smile across her face. 

/I'd been thinking of a way to get ourselves out of this mess, but I haven't come up with anything... They said they rescued us, but... in that case, why'd they bring us all the way back here? They could've just sent us packing back out of the hole and that would've been it. At least if that happened, we might've been able to ask someone for help. But now.../

The silhouette of a sith soon appeared through the door. It waddled in with its tiny legs, and looked at them with its dark, beady eyes. Upon closer inspection, this one had a gray coat with black spots all over. She had noticed it earlier, but the sith really were like dogs, in that their coats could vary wildly in color and pattern.

The dogman barked, "Bow. You, eat." It laid an earthen plate, filled with a few mushrooms and small fish in front of them. As it walked back out the hut, it turned back one last time and said, "Bow. After eating, come outside and follow me."

The sith soon left, wasting no time with hellos or goodbyes. 

/But he did have the time to say 'bow'... what's that about? He didn't even bark. He just said 'bow', without even trying to sound like a dog.../

After heaving a collective sigh of relief, both their stomachs grumbled.

"Ugh..." Sylphia said, while rubbing her belly, "I even forgot we haven't eaten since entering the dungeon."

"Mm..." Letty nodded. "Everyone was so excited..."

"But still," Sylphia cringed as soon as she pulled the plate closer. "I don't think I'm desperate enough to want to eat this just yet..." 

The mushroom and fish glimmered from a coat of water glazed over them. One poke of the fish and she knew, from its texture and temperature, that it was uncooked.

"I-is it safe to eat this...? I mean the fish is raw, and I've never seen a mushroom like that in my life..."

Letty inspected the mushroom up-close. "It isn't brightly colored," she said, "there aren't any spots, and it looks like they cleaned it... i-it might be edible?"

"Might...? That doesn't sound-"

But before Sylphia could even finish speaking, Letty popped the mushroom in her mouth.

"Wai-" Sylphia froze. Swallowing her breath, she stared at Letty's face as she ate the mushroom.

First, she rolled the mushroom around in her mouth. Then, her eyes closed as she took her first bite. The dull crunch was audible even to Sylphia's ears.

"Letty?"

She opened her eyes but continued chewing. 

The corners of Sylphia's mouth twitched. "W-well...? Is it good?"

"Hm..." Letty tilted her head to the side.

/What does that response even mean?/ 

Her eyes turned towards the other mushroom. 

/'Hm...'? What does that mean? 'Hm... it's okay'? Or 'hm... I'm gonna die'?/

She leered at the fish-sized fungus on the plate. Her mind rejected it, but her stomach craved it. 

In the end, the fact that Letty had first taken the plunge settled the matter.

/Well, if we're gonna die here anyway, I'd rather not die alone./

Finally, resolving within herself to join her companion, she reached out for a mushroom and bit into its cap. Its cold flesh gave with a certain crisp against her teeth. 

Letty swallowed her mushroom and asked, "How is it?"

Sylphia rolled the mushroom around in her tongue. As she thought, the food was uncooked. A cold sensation followed her bite and a hint of a grassy flavor seeped out while she chewed on it. /What a strange taste... It's not necessarily disgusting, but it's bland as fuck./ At that moment, she understood. Thus, tilting her head to the side, just as Letty did, she said, "Hm..."

The scribe covered her mouth and chuckled.

She joined in with her own laughter. /Since she always keeps her emotions at bay, it's not too often to see her laughing like this... but her mannerisms sure are cute.../ Next, she pinched a chunk of meat from the fish. The tough skin made it difficult to pierce through with just her nails, and even then, the meat itself was similarly tough. On top of all that, despite being rinsed over with water, the fish retained its slimy texture that stuck to Sylphia's fingers while she raised the meat to her mouth.

/Bleh... I can already imagine how this will taste./

Finally, she laid the chunk of fish on her tongue. As soon as it made contact with her tastebuds, a mild, salty flavor spread across her mouth. /Oh, not bad. Not bad at all. It sure could do with a little goddamn cooking, but it could definitely be worse. That sliminess really makes my skin crawl though, bleugh./ She sighed. After helping herself with a little more, she realized that the deep depression she felt earlier had gone. 

/Now that I'm thinking a bit more clearly, things aren't quite as bad as I feared... Somehow, we haven't been killed off yet, so there might be a chance that we could be set free. I mean, we were given something to eat after all. And if we're safe, then there's also a chance that Rika's safe./

/At least, I hope so./ 

"Letty," Sylphia said, "We're going to rescue them, no matter what." 

"Mm. We'll save them." Letty nodded. "...but how?"

"We can figure it out as we go along." She then stood up and patted down her dress. "For now, there's no point in holing up in this dank hut." She lent her hand to Letty. "Let's go."

"Mm." Letty clasped her hand.

Thus, the two of them walked hand-in-hand as they met with the sith waiting for them outside.

####

They followed the sith deeper into the village. Huts covered the steep slopes, many with bleached white poles made of mushroom stalks supporting their weight. Dog-like monsters of various sizes and pattern of fur roamed freely around the settlement, going about their day-to-day lives. 

One thing Sylphia took notice of was the large number of siths walking around with weapons, some even with armor, within the village. Besides that, most of the small siths, presumably children, appeared to spar with each other using toy weapons as their primary pastime. All in all, the culture and layout gave the village a tribal, militaristic feel. She imagined that, if looked at from afar, it might have looked more like a military fort than a village.

After perusing the layout of the village, Sylphia's eyes wandered toward the siths themselves. The Cu Sith, as they were called, resembled dogs only at superficial level. 

For one, their proportions were exaggerated in several aspects. They took a humanoid posture, standing on what would be their hind legs, but despite this, none of them stood higher than Sylphia's shoulder -  the shoulder of a human child. In contrast, their heads were very large, larger than a human's if the snout was taken into account. 

Their bodies were tiny. Their torsos looked to be the about as large as, if not smaller than, their heads. Their limbs grew out to about half that size and looked like nothing more than stubby protrusions. 

Despite that, their warriors wielded swords as long as they were tall, and spears far taller - easily many siths in height. On the other hand, having such a tiny body allowed them to get by with a much smaller set of armor, apart from an oversized helmet.

/Come to think of it, they look pretty similar to the Kobolds earlier... As if both of them popped right out of a cartoon and came into this world. C-could this be how the monsters look in Tales of the Empyrean as a game!?/

Eventually, they arrived at a slit cave. The narrow fissure dug into the rock itself, forming a passageway just large enough for Letty to walk through.


The sith guiding them stopped at the entrance to the cave. It turned around and said, "Bow. Lower your heads when you enter. Bow. You will be in the presence of a God."

Sylphia shuddered. /A... God!?/

Letty's grip on Sylphia's hand grew tighter.

/I don't like how this is going.../

Thus, they entered the cave following only the feet of the sith guiding them. Eventually, the narrow passage led to a wider space. In it, many more sith stood around them. Unable to raise her head, though, Sylphia could not make a good estimate of how many there were. 

Suddenly, a booming voice echoed through the room. "//Beware//, outsiders. //You// are in the //presence// of the mighty //Death God//, Barog-Tong!" The spine chilling voice carried with it a menacing rhythm that emphasized every other word.

Its voice washed over Sylphia like a cold mist and made her skin crawl. /A-a-a-a G-God!? Like, for real!? A fucking God!? H-holy shit! What's more, a //Death God//!? Geez, we're screwed!/ It took all of her willpower to keep her head down. All of her instincts screamed at her to, at least, get a good look at the God that was about to take her life.

"//Show// me, my faithful," The God, Barog-Tong, continued, "who //dares// step into //my// domain!?"

"Bow. You may raise your heads," said the guide.

And Sylphia did just that. She found herself in a small cave, with seven other siths, apart from their guide. They each wore intricate looking dresses, fashioned from rotten grass, rocks and other primitive materials. It reminded her vividly of witch-doctors she had seen in cartoons. Seeing them in real life, though, she thought they looked rather goofy.

One of them stood in front, atop a stage of raised earth, with a curtain of weaved blue grass draped around something, or someone, at the very middle.

The sith in front, one with a brown coat and a circular spot of white around his nose, raised its stubby arms and barked, "Ruff! Behold, the true form of the God, and tremble!"

Sylphia swallowed her breath.

It then grabbed one end of the curtain and bunched it to one side, revealing what laid behind. 

There stood a small table, bleached white with a smooth texture. On it, a plate made of solid Mythril imparted a pale, blueish-green hue upon the arrangement of various grasses and mushrooms in it. Perched at the very top of these embellishments was a folding fan, spread open. Sharp strokes of black and red adorned the fan's blank white cloth, forming the shape of a face with its eyes closed.

Sylphia was in the middle of appreciating the intricate design of the fan, when all of a sudden, the colors shifted around the white canvas. 

"Wha!?" She jolted back and bumped into her companion. /Th-the face... moved!?/

Its eyes opened and swept across the room left to right. As its gaze fell upon them, she felt a Letty's grip tighten around her hand.

It sharpened its gaze. It looked displeased.

/Eek!/

A few tense seconds passed. A bead of sweat rolled down her brows. It itched so much and she wanted to scratch it so bad, but she could not move. She didn't want to move. She imagined, perhaps, if she did not move, then the thing wouldn't see her.

Suddenly, the fan's eyes and mouth popped wide open. "Hngka-!?" It shrieked. 

The siths gasped in unison.

"Ruff! What is wrong, my God!?" The Sith in front asked with a fearful tone.

/Ueh!? The- the fan is....!? Barog-Tong!?/

The fan soon pulled its jaw back up and said, with an exasperated voice, "M-my faithful! L-leave me with the outsiders!" 

"Ruff! My God...?"

"I-I said leave!" It screamed. Its voice had raised greatly in pitch, and it no longer emphasized every other word. 

"R-Ruff!"

With a cacophony of barks, the Siths scurried out of the cavern with their tails between their legs. Thus, Sylphia and Letty were left alone with Barog-Tong.

/Eh!? Eh!? Eh!?/ She looked frantically around her as the dogmen scurried around them like mice, funneling themselves into the tight passageway and out of the crevice. /What's happening!?/

The moment the noise settled, Barog-Tong said, with a trembling voice, "Y-You... you're... you're humans...! Aren't you!?"

Fighting against her nerves, she squeezed her voice out. "Y-yes..."

It sobbed. "It's true..."

/Eh?/

"Waaaaaaaahhh!! It's really true!!" The design printed on the fan swirled to life and depicted a stream of tears flowing down its eyes. Even the cloth changed color, from a pure white, to a deep blue.

"Eh? Wha-" Sylphia winced. "What!?"

"Please, please take me with you!" The fan shook in its altar, dropping bits of mushroom and fish all over the cave floor. "Please, oh Gods, please!"

But she couldn't make sense of the situation. After being taken on a roller coaster ride of emotions, yet again, it was all she could do to stand speechless, while the adrenaline drained from her body.

Letty then stepped forward, asking, "Um... could.... could you possibly be... a living weapon?"

Sylphia's jaw dropped. "A... living...!?"

"Y-Yes, I am!" it said, "A strong one! An amazing one! You'll totally regret it if you leave me here!"

Excitement gushed into the little girl's system even before the terror had all filtered out. This resulted in a strong tremor coursing through her body, reflecting even in her speech. "L-L-L-L-Letty... I-I don't know what the hell's happening,  but i-i-isn't that amazing!?"

"Eh? U-um... yes..." said the scribe, clasping her hands over her chest.

Soon enough Sylphia had calmed down enough to speak properly. "Huh? What's with that reaction?" She raised an eyebrow. "Isn't a living weapon an ultra-rare item?"

"It is!" The fan shouted from the sidelines.

"So it says." She slipped each of them a short glance. "So why aren't you more excited about this?"

"U-um... well... isn't it quite suspicious that it's trying so hard to get us to wield it...?"

"Huh?"

"I-I mean... if it's really a that great, shouldn't it be more difficult to obtain its power?"

The spark of realization hit her hard, giving her pause.

"Basically, what I'm trying to say is..." Letty turned her worried eyes straight into Sylphia's eyes. "It might be cursed..."

"Ah, I see..." She nodded. "That makes sense."

The fan's eyes widened. "Wha- wait, wait! Why are you deciding I'm cursed all of a sudden!?"

Ignoring its pleas, Letty continued, "We should leave it alone, Sylphie..."

"Mm... that's pretty disappointing, but..." Sylphia sighed. "You're right. It's not worth being cursed."

"What!?" It screamed at a high pitch. "Wait! I'm not cursed! Really! Please, believe meeeeheeheeee..." It sobbed.

"Mm..." The little girl crossed her arms. She was conflicted between pity, suspicion and general disgust at how needy the supposedly amazing legendary weapon was acting. "Then why are you so desperate for us to use you?"

"Grrrr!" Its eyes pinched together in frustration. "Fine! I'll tell you! Just... just don't laugh, okay!?"

"Huh?" Her eyelids twitched. "The hell are you talking about? Get on with it!"

"I-I will! Just... you promise, okay!? I..."  The color of its surface turned red. "I'm sensitive, so..."
 
She frowned. "Tell us already, or we're leaving."
 
"Aaah! Okay! Okay!"

Letty subtly leaned in.

"Ahem." It cleared its throat, despite not having one. "Living weapons do not die, you know?" 

"Huh." Sylphia cocked her head. "Okay...? So?"

"So we do not understand the concept of death like you living beings do. However - even we living weapons have a concept of 'hell'."

"Ah." Letty jolted, as if coming to an understanding.

/Huh?/

"Little miss, do you know how long I've been trapped in this godforsaken cave?"

"Of course not."

"510 years."

Sylphia flinched. "Ueh...?" 

"I've kept count, you know? Ever since I was created, I haven't seen the light of the sun. Not once! Hell, I've not seen night! Just these dumb kobolds and siths running around, making a mess of the place..." Its voice grew weary. "Well, that would have been fine if anyone bothered to pick me up and use me in the meanwhile. But they won't. And you know why?"

"No."

It screamed, "Because I'm a fucking fan, that's why! What sort of evil God would create a living weapon out of a fucking fan!? Are you kidding me!? Even I know how fucking useless I am!"

"Uwah..."

"I've sat here, unused, for five centuries! At first, they didn't even care about me, right? Just running around, lobbing stones at each other. I thought, hey, these morons can make tools and weapons!  Maybe I could teach them to take me out of this godsforsaken cave, yeah? Nope. The idiots managed to collapse the goddamn entrance to trap each other in. Genius."

"Erm..."

"I thought, fine. These chumps would do. These guys, the kobolds and siths, they're dumb as hell, but they're intelligent enough to use tools and weapons, so why not, huh? The rules didn't say I needed to be used by humans, right?"

/Eh? Rules? I think this guy is glossing over a bunch of really interesting shit right about now.../

"Oh, but then some joker came around and taught them how to dig and use Mythril! That turned out to be a slippery slope..."

"Eh!?" Both Letty and Sylphia squealed. They looked at each other, muttering to themselves, "Teach...? Who would...?"

"Long story short, they came to believe I was their god..." It sighed. "Well, to be honest, I was okay with it, at first, right? The free praise really gets you. But //then// they decided what no one is allowed to touch their God! Because I was too holy!"

Its resentful voice echoed so loudly throughout the room that Sylphia wondered if his 'followers' could hear him ranting to the 'outsiders'.

"Aaaaaah!" it jerked up, down, and side to side on its tiny podium as it screamed, "And they even had holy wars where each tribe tried to steal me away, just to plant me in their little villages! Not a single time, any one of them tried to use me! 500 years!"

/T-this guy's got a lot of baggage.../

"Do you see what I mean!?" Tears streamed down its face in an exaggerated wave pattern. "This is hell! To be locked away, unused, unable to grow - that is hell for a living weapon! I may just be a fan, but I still have needs! //Needs//! I //need// to be used! So for the Gods' sake, please take me away with you...!"

The two girls exchanged anxious glances.

"I don't know..." Sylphia said, "should we? I'm starting to feel bad for it all of a sudden..."

"W-well... but it could still be cursed..."

"Ah, right. That's pretty risky, isn't it? So, there," Sylphia turned to the fan. "Sorry, Barong-Nong or whatever, I'm sorry for you, but that's how it is, so..."

"Oh?" The fan lowered its voice menacingly. "Even after I prostrated myself to you, you'd still swat me away?" The design on its face grew more intense, with strokes of red and black flowing around it.

Sylphia swallowed her breath. /I-is it angry?/

Letty whispered to her, "Don't worry, living weapons cannot attack without a master."

"Ah, I see! That's a relief."

"Don't be so sure..." The fan said. "Don't forget - I am still considered a God by the dogmen of this village."

Sylphia froze up.

"One word from me, and I can have them slit your pretty little throats!"

Letty reflexively tried to cover her just as like earlier.

/Tsk. This guy isn't fucking around... to be killed now or to be cursed, huh...? The balance will depend on the curse, but.../ She placed her fingers over her mouth. /He's their god. We're just visitors. But we're the first visitors he's seen in centuries. He must be as desperate - no, maybe slightly more desperate than we are... If I play my cards right... then he might just be what I need.../

Sylphia smirked. "A deal with the devil, huh?"

"Sy... Sylphie?" Letty turned anxious eyes towards her.

"It's alright," she whispered, unlatching herself from her Master's protective arms, "I've got an idea." 

"A..." The scribe attempted to say something but relented. She then took a step back, giving her implicit approval to Sylphia's plan.

"Thanks." She then turned to the living weapon before them, declaring, "Borat-Dong or whatever, I'll make a deal with you."

The fan's eyes widened.

"I will take you up. Curse and all. But let me make one thing clear-" She tilted her head upwards and looked down on him with a sharp, menacing gaze. "I'm the boss of this relationship. You're /my/ living weapon. Understand?"

"Done," without hesitation, it answered.

Her lips spread out, forming a malevolent grin. "Good."

####

The siths were later summoned back into the room. 

They shuffled through the narrow passage, it being only wide enough to fit one of them at a time. In front, atop the earthen platform, stood Sylphia and Letty, at either side of the God-Fan. Their very presence there caused discontent among the Sith. They chattered with low, scornful voices amongst each other, but none so blatant as to be heard clearly in front.

"My //faithful//! Led me your ears!" Barog-Tong's voice bellowed.

With a chorus of various barks, the Sith all knelt down and lowered their heads.

With a smirk on her face, Sylphia nodded to her living weapon.

"Rejoice!" The powerful voice echoed within the cramped room. "For I, the mighty //Death-God//, have seen //fit// to answer your //faith//!"

The siths recoiled in anticipation.

"For today, disguised as //two// outsiders, I have summoned the //heralds// of your salvation! //Behold//!"

The siths raised their heads in concert.

"Ruff! Heralds!?"

"Bow! Who-"

Sylphia giggled as she waved her hands at them. Meanwhile, Letty meekly nodded.

The fan continued, "Before you stand the //two goddesses// of my heavenly court!"

The dogmen's oversized jaws dropped at the revelation.

Their chatter grew louder thus, "Wan! There are more gods aside from Lord Barog-Tong!?"

"Woof, this is unprecedented..."

"Bow. What do they mean by, 'our salvation'...?"

Barog-Tong raised his voice, "Indeed! I, the omnipotent //Death-God// hold a pantheon of lesser Gods, of whom these two are my most trusted!"

"Ruff! To hold even other Gods as His lesser - such a magnificent God, Barog-Tong..." The sith lowered his head into the ground.

"Indeed - allow me to introduce them to you. To my left is The Goddess of War, Sylphie!"

All eyes focused on Sylphia. With a condescending smirk on her face, she gave them a curtsy, pleased as all hell with herself to have turned the tables on the siths who, just earlier, had ordered her around.

The fan continued, "And to my right is the Goddess of Mercy, Letty!"

Letty took a simple bow before stepping back.

"These goddesses, I prophesize, shall be your //salvation//!"

As the roaring echo of Barog-Tong's 'god voice' faded away, the siths mumbled amongst each other.

"Woof, The Goddess of War... does this mean we will finally destroy the wretched catlings?"

"Bark. I hope so... They have stepped upon our crops for far too long!"

Barog-Tong again raised his voice. "My faithful! There is one last matter that needs to be addressed."

"Ruff! What is it, my God?"

"I am stepping down from this altar." 

"Ruff!?" The sith ran forward and prostrated itself in front of the fan.

Meanwhile, the rest of the dogmen trembled on their tiny knees.

"Ruff! But why!? Is there something to your disliking!? If there is, then allow us to-"

"That is not that case."

"Ruff!? Then-"

"My faithful!" Its powerful voice sent them groveling on the ground. "What lies ahead... is the end of all conflict."

The siths recoiled. 

"Woof, I knew it..." one of them whispered, "so it is finally time..."

"It then behooves me to act, not only as your Supreme God, but the God of this entire Cavern! I shall, myself, join the Goddess of War in battle and see to it that victory is achieved!"

The siths erupted in cheers.

"Woof! The God Himself shall join us!?"

"Bow! Then victory is guaranteed!"

"My faithful! From the outset, you knew that I was a living weapon of infinite power. You have kept my power sealed all this time, but now, that seal shall be undone!"

The siths gasped.

"With the name that the Goddess of War gave me, so shall my true power be made manifest! Let it be known far and wide - the //Death-God// Barog-Tong shall, henceforth, be known..."

Sylphia lifted the self-proclaimed Death-God from its altar. As she did so, the gaze of each sith followed her hands upwards, their faces filled with awe.

"...as Sebastian!"

Sylphia presented the fan to the monsters before her. Each one uttered praises to its new name and kissed the ground in a show of faith.

"Praise be, Lord, Sebastian!"

"Our God, reborn! Blessed be His name!"

"Oh, bring us salvation, our God, Sebastian!"

Once, the ceremonies were completed, each sith hurried outside, eager to be the first to spread the new name of their Lord. Their wild barking resonated back into the rock chamber and fell off farther and farther.

And so, in that cave-within-a-cave, the unlikely trio, Sylphia, Letty and Sebastian were, once more, left to themselves.

Sebastian was the first to break the silence. "Uhh... so, why Sebastian?"

Sylphia snickered. "You're my servant now, remember? Which, basically, makes you my butler. And all butlers should be named Sebastian."

All color washed away from Sebastian's face. "...huh? What the hell are you talking about, little girl?"

"Just roll with it." With a flick of her wrist, the fan folded into itself. She then tapped it onto her palm and said, "Ah, crisp. Like it's brand new. Right?" She turned to Letty and gave her a wink. 

"U-um..." The young woman avoided her eyes. "I-I'm not so sure about this..."

She grinned. "Come on, it'll be fun!" She then patted her on the back.

"Fun- Eh? Aren't we going to save the others?"

"That too!"

Letty whimpered.

Thus, the Almighty God, Sebastian, and his goddesses, Sylphie and Letty became known to all the sith village.