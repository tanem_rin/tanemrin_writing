"Oh, so you were working for the enemy after all? Hmm... interesting."

The spear maiden then resumed her misc. errand.

"W-what are you doing!? Kill this traitor immediately!?"

She ignored him. "Ah! Here it is."

She was about to leave the tent when the senator screamed. "Wait! W-where are are you going!?"

Without letting him speak any further, Kiya finally thrust her blade into the man's throat.

"Hmm..."

"Why... aren't you stop me?" Kiya asked.

"Because it's none of my business."

"...?" She knotted her brows.

"I just came here for a good fight." She said with a shrug. "But the moment I arrive the old man tells me he'd assembled an army to crush the enemy? Way to waste my time." She then smirked menacingly. "But what if the army's chain of command is broken? What if the soldiers start deserting? What if we suddenly start losing!?" She then curled up, clutching her arms in utter arousal. "Wouldn't that battle be more exciting!?"

She winced. /This person is crazy./

"Aah, I can't wait!"



"Ah." The spear maiden got an idea. "It's really up to you, but if you're looking to degrade our fighting ability, you're better off leaving that man alone."

Kiya jolted.

"Instead, you might want to go after his assistant. Handsome guy, blonde hair."

The senator scowled at her. "W-what foolishness are you saying!?"
