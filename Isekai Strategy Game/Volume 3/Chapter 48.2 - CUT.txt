The village burned in the distance. The flames cast their dim light upon the fields around them, illuminating the polished armor of the men of the 80th Line Company. Their steel boots pounded the fertile soil, crushing what little remained of the village's livelihood as they approached a criss-crossing wall of pale, blue light.

"Keep in formation, men!" the captain shouted, "Once we cross that boundary, we are in the enemy's territory!"

"Yes, sir!" answered those around him.

They advanced in a loosely packed checkerboard, with each tile being a squad of 12 men. Six of these carried crossbows, while the other, tall, rectangular shields. Every man carried their own pack of bolts, even those without a crossbow themselves. In addition, at the rear of the formation, was a group of robed men and women of the Xth Anti-Mage Battalion.

Meanwhile, the sky above them flickered like a fireworks display. A flurry of mighty spells, coming from both sides, smashed repeatedly against the wall. Jets of flame sprayed onto it, only to scatter harmlessly. Bolts of lightning crashed onto it, creating a thunderous roar, but little else. Even boulders of rock and ice screamed in the air, slamming themselves to dust against it, to no effect.

In fact, these were two magic barriers, pressed against each other, preventing the other's spells from penetrating their respective boundaries.

And the men of the 80th were a mere stone's throw from the precipice.

The pale light grew brighter the closer they came. At the same time, it also emitted an eerie hum, like a blunt saw carving fruitlessly through steel. The captain knew this all too well. He knew how it ground at the will of men who were unexperienced with magic.

And thus, with a mighty roar, he aimed to soothe this men's spirits by numbing their senses with his voice, "We are the vanguard, men! Today, we have the honor of dealing the first blow to the menace of the east. So tighten your boots! Grasp your sword! And follow me, as we kick Horsch right back to Alfheimr!"

Those men, more than a hundred strong, roared at the moon, itself mostly obscured by clouds and smoke, as they took their first step into no-man's land.

####

A fireball streaked overhead just as two soldiers in red shirts ran into a dug-out embankment on the side of the hill.

"Hans! Do you understand!?" one said to the other, "We need those reinforcements!"

"Yes, sir!" the young man said as he immediately ran.

He then cupped his hands over his mouth and screamed, "Once you deliver that message, run right back! I need you to deliver an order to the right flank!"

Once the boy disappeared from view, the man sat down into the embankment, joining a couple of others sat against the dirt.

"...fur Scheisse." He grumbled, "We don't have enough men to contest the village! What is that fat idiot thinking!?"

"Now, now, Captain," said one holding a cracker, "keep your voice down, the mages might hear you. It's bad for morale."

He sighed.

"Isn't it fine?" (soft voice) the other then added, just as a lightning bolt zipped by, grabbing all three's attention for a whole second, "When the situation becomes untenable, we can just retreat right back to the fort and the General won't be able to complain."

"Shit! And I'm just about to retire, too... I don't want my pension to be affected by a last-minute defeat!"

"Don't worry about it." He shrugged. "That's just a bad rumor."

"No," He gave a stern riposte, "really, listen. I know a guy-"

But he was cut off when a man in light scale armor ran down into embankment and saluted to him. "Sir! Report from the auxiliary!"

"Ah, yes!" he said, before turning to his mates, "excuse me for a second."

He then stood up and joined the messenger by the mages' aterlier behind the embankment.

"Captain," the man said, "the shock attack was a success. The enemy's main line has broken and our infantry has taken the streets."

"Great! Where are the lancers now?"

"They are resting at the rear line, but they also reported a second formation of infantry approaching the contact line!"

"Hngh!?" His eyes popped wide open. "They're charging through no-man's land!? Blessed Rheinhardt, this must be the Raffalian Assault Infantry I've heard about!"

"Orders, sir?"

"Kuh! We don't have the reserves to meet them. Send out the message to all units engaged in the village - full retreat back to the fort. The forward atelier shall cover them."

"Yes, sir!"

He then paced back to the embankment, yelling, "Hey, you lot! It's time for work!"

"Oh? New orders, Captain?" asked the one who had just finished his cracker.

"Yeah, we're pulling back. Looks like the enemy is going all-in for their hand."

"Uwah," the other grimaced, "Raffalians are just not right in the head."

"That's just fine for us. If they want to break themselves in our first defense line, then let them!" He turned to the one with the soft voice. "Karl, send the retreat illumination over the village. Count five minutes and then saturate the south and east entrances to the village."

"Got it, Captain."

He then turned tot he other. "Ernst, your section will bombard no-man's land. Since we don't have forward elements, you need to spot the enemy with illumination and then hinder their advance."

"Basically, make a wall of flame in front of them, right? Easy enough."

"Mm!" He nodded, "Then, go!"

As the two ran over to their respective ateliers, the captain was left to himself, tapping his feet anxiously.

And, in the background of a blazing fury of spells raging behind him, he grumbled under his breath, "Hans, where the hell are you!? I need to send those lancers, pronto!"

Just as he said this, a sceam erupted from behind him, "We're illuminated!"

A loud crash followed, the shock of which reverberated up from the ground and through his feet.

He froze in place, his skin turning into gooseflesh as a cold chill then travelled up his spine and onto his shoulders.

And as he turned around, his senses were met with the screams of the men and women of the mage ateliers scrambling for cover. On the ground, a young woman had been staked through the chest by a massive wooden arrow.
 
His eyes widened in abject fury. "Mage... hunters...!?"

####










#####

--NEXT CHAPTER--
Intrigue to discuss:

- The Grand Sorceress is late because of the black caps
- Easter cells inside Rafale made the bandit attack happen on the supplies
- Kiya is back at the camp, and she is feeling the pressure to disrupt the Raffalian operation. She notices the Senator and considers his assassination. She doesn't know that the lieutenant was the one making good things happen.





#####

First scene is the one with the Eastal commander, daylight.


**The commander receives word that the Grand Sorceress is finally on her way from Griffonland. Touch upon the black caps.

=====================




**She won't be able to do it, because next chapter, the Grand Sorceress will have arrived and smash the Atelier they built in Tybolg (along with the entire village), as well as both Assault Companies.**






The dim glow of the burning village traced out the silhouette of the distant hillside. Upon it, bright red flares erupted.


It was the signal that the battery commanders had been waiting for. With a chart in hand and a dim light over their shoulders, they called out their first order. "Ranging volley!"

A mages stationed at each ballista then set the arrowheads alight with a fire spell.

"Loose!"

With a loud, simultaneous thwack, the fiery arrows leapt into the air, each at progressively higher angles.

The projectiles landed in succession, producing a rough line in the distance, which the battery commanders quickly used as reference.

One then called out, excitedly, "Angle, 41 degrees!"

"41 degrees!" the other commanders echoed his call as the crews got to work.

The rangers readjusted the pitch of their contraption while the loaders reeled the bowstring back for a second shot.

"Load!"

One after another, the ballistas received a new bolt in their mechanism. They were ready to fire.

"Loose!"

This cycle repeated over and over, as the battery of 20 or so weapons unleashed a deadly barrage into the darkness.

"Hmm... so you use these contraptions to attack sorcerers from afar..." She chuckled. "Pretty clever."

"I am glad our humble arsenal has entertained our dear heroine. But this effort would not be possible if not for the brave mages of the <>th Anti-Mage company who volunteered to follow behind the 80th in order to highlight the location of the enemy atelier. This is a textbook anti-mage tactic, devised from the last war against Easter, and the quintessence of modern warfare."

"Yeah, but you know." She lazily crossed her arms behind her head. "If this is how all humans in this world fight, I might actually die of boredom."

He let out a light chuckle. "Please don't say that. We humans are rather cowardly creatures. If there is no path to victory but though a river of blood, we simply cannot help but build a bridge over it." He said, tapping the light ballista right as the machine convulsed violently, flinging what may as well be a spear dozens of feet into the air.

"Hmm..." She moaned, unconvinced. "Humans in my world aren't."

"Ho..." He glanced at her curiously. "Can you share with us the techniques you used for besieging a fortress in your world then, dear Heroine?"

"Haha, are you kidding?" She said, waving her hands in front of her face. "Demons don't build fortresses, they destroy them! Oh, but if it's defending them, then I guess I do have a little experience... Basically it just boils down to "hack them down when they scale the walls", right?" she said, swinging her arms down as though she were holding a sword.

"A-ahh... I see. Thank you for sharing your wisdom."

"Mm!" She nodded with a smile. She then began muttering under her breath, "But if they did... I wonder how many people wouldn't have had to die...?"

/Hm?/

A messenger then arrived.

"Sir, the 80th's main thrust is diverting east, northwards of the village. It seems they were on the receiving end of a storm of magic spells."

He grunted. "Even"




#######

CUT



>>>> Do all the intrigue and stuff next chapter??
**Hans was killed by Aster**

But Aster thinks that Easter might be losing a bit too much so she lets Hans deliver the message without killing him.

"Geez, who'd have thought Easter would get pummelled this bad? At this rate, Kiya's the only one who'd have some fun!"

"Tsk. Should we redeploy against Rafale instead? We've already spread out this much, how long will that take...?"

####

***Back to the 80th PoV***

Streams of light flew overhead like shooting stars, turning night into day for just a split second before the mages of the Xth Anti-Mage Battalion would, with a hard thud, launch some sort of invisible projectile, undoubtedly of the magical sort that would snuff out each flare from the lightest graze.

Nevertheless, the balls of light seemed to do have done their job as within moments, various elemental spells crashed down all around them, spreading slippery, sharp shards of ice and rock, and glowing embers setting light to the grass under their feet. Indeed, the only thing allowing them to advance was the magic shield their own mages cast overhead. It was nothing as large as the walls they had passed - indeed it resembled a clear dome or an umbrella rather than a wall, but it did its job. Spells that impacted directly on the shield - that is, those that were to hit their forces directly, deflected away harmlessly. Mostly, at least, as the edges of the formation were left exposed from the after-effects of these spells.

"Tighten formation, men! Get under the magic shield!"

The shield was not perfect, however. A single mage's barrier could not possibly hold a candle to one made by a pool of several mages', in the form of an atelier. Every time a spell hit the shield, tiny, hairline cracks would form, and soon the shield dissipated. Another mage would then take point and create another shield overhead.

The captain knew, however, that this placed far too much strain on each mage, and could not be kept up for long.

"Kuh... as expected of Easter's magical atelier... at this rate, our mages must be half-way to exhaustion... We can't make it to their mage lines!"

"Men, we turn! Turn! We will outflank the village and crush the enemy there! Mages, bring down illumination on the enemy ateliers! We'll let the ballistas take care of them!"

As their own streaks of light, glowing bright red, reached their destination, the wind howled with the sound of steel-tipped logs flying in the air. Soon, even the enemy's spells stopped raining on them, giving their mages a well-earned respite.


"Men! Anti-cavalry formation!"

The formation them sundered and quickly reformed into a tight square, with the shields interlocked facing outwards, protecting the crossbowmen behind them.

The lancers charged.

"Wait for it... wait for it...!"

""



###########





I am the descendent of the Zeth, the God of the Scales; Beacon of the Stars. My name is Extella Zeth. Demigod. Heroine of the Last Kingdom of Mortals. And now, Heroine of the Human Kingdom of Rafale! Rejoice, all ye in attendance! For you shall be witness to the power of the divine!