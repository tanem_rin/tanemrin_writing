
- Eris is the formal contact between the main bandit group operating in Dellwick and Count Rosteforough. They don't work for her in the official sense, but she allows their existence to the extent that they don't overdo the banditry, and that they stop other, uncontrolled bandit groups from planting roots. There have beem occassions where she intentionally sent the bandits to their deaths for being too much trouble. Her primary contact is the bandit leader, "Eckhart", an immigrant from Easten with lots of underground contacts, and extremely redpilled about her business. They respect each other, but keep their relations strictly business.
- Eris' contact is with the messenger, Roderick, a lower-level assistant to the Count. He was the one arranging orders for Eris to carry out.
- Eris and Eckhart are both from the Easter Empire, but only came to know each other in Londinium. Unbeknownst to even Eckhart's lower subordinates, she and Eris always arrange their 
larger raids, and Eris occassionally asks for 'victories' against the bandits, which she occasionally uses to purge the less subordinate members of her faction. This relationship is supported by all of Eckhart's inner circle.
- Eckhart is a girl, by the way, and normally helps out as a farmhand during harvest seasons, also so that she can mark big hauls for attacks. For this reason, she doesn't participate on raids herself.
- Sylphia will eventually tighten the in and outflux of people in Dellwick, which will threaten one of Eckhart's few legit sources of income.
- When Sylphia unleashes the Kobold Cultists on her subordinates, her operation is hurt badly, and Sylphia takes her in (unofficially) as Eris' helper, and gives her a legit identity as a barmaid in Dellwick's new tavern (it's slowly turning into a town).
- Eris wasn't expecting Sylphia to have a dark side that appreciated her 'real' abilities. Unlike the pig headed Rosteforough, who she felt was only able to wield her due to his immense wealth, Sylphia was able to turn the tables on her and corner her into a bargain. She felt that serving under such an unscrupulous and ambitious Lord might be more beneficial than a lifetime of playing Sycophant to the wealthy bottoms of the House of Lords.
- In the end, Eris' goal is to make enough wealth so that her sister is able to live comfortably. With Sylphia's help, she is able to even give her an education at the Royal Academy - something that was out of her reach even with a lot of wealth, as it only catered to nobles - or, as for Sylphia, Lords.
- Through all this, Rika tries to keep Sylphia safe, despite her antics. She highly disapproved of Sylphia condoning the bandits, saying that she can't, as a former town guard, accept their existence. 
"I wish we could live in a world where crime doesn't exist anymore." Sylphia said. "But that isn't this world." /Nor is it in the world where I came from./ 

"But Sylphie, you're the heroine! You've done things that I thought was impossible..." Rika's mouth pursed up. "Why can't you do this?"

"Rika..." Sylphia lowered her head. "I'm not as great as you think I am. I can't solve every problem that comes my way. I can only make the best of the situation we're in - and this is exactly that."

"I don't..." Rika clutched her arms together. "I don't understand."

"It's the simplest thing for me to send out soldiers to hunt all of the bandits down, but what then? When these bandits are gone, others will take their place. Stealing will never be as as difficult as doing a real job."

Rika furled her brows. "In that case, just keep chasing them down! It's what guards do, right!?"

"It's not as if charging into a hidden camp in the woods is he easiest thing ever. A big operation like that costs lives, Rika. On the other hand, here we have the golden opportunity to put banditry in Dellwick under control - through Eris, we can minimize the impact that Eckhart's bandits has on the village, and on top of that, since this is their territory, they will do their best to keep other bandits out - ones we can't control at all."

"Sylphie... you know what you're suggesting is illegal."

She crossed her arms and let out her breath. "I know. I'm sorry, but because I'm so powerless, this is the best I could do..." She then glanced up into Rika' eyes and said, "Are you disappointed?"

Rika opened her mouth, as if she was going to say something, but pulled back, and instead nodded. "Mm."

"I'm sorry."
- The Kingdom of Londinium has been in military mobilization for a while now, but Dellwick was exempted due to being a primary source of food for the kingdom. Sylphia starts noticing more soldiers in the capital and elsewhere.
- Other lords side comment that Sylphia should also start mobilizing actual soldiers and not just town guards, since Dellwick is very close to the rafalian border.




######################################################
######################################################
################## PLAY BY PLAY ######################
######################################################
######################################################


- Get contacted by messenger
- Forge a letter from Count Roste herself, stroking Sylphia's narcissism.
- Get rejected anyway.
- Go to tavern (not too far from inn) and meet Eckhart, inform her of an upcoming job.
- That night, write a letter to Emi via the post. Say that she's doing fine, and she's at Dellwick at the moment, so she'll bring home some sort of souvenir when she comes home.

-- CUT --
- Next few days, bandit attacks one after the other start happening in the middle of harvest season.
- Citizens are getting angry since, though their taxes are lowered, the lax security and their loss in life and money from the food caravans are taking a severe toll.
- Sylphia gets pissed, but refuses to blame Eris or Rika. Eris informs her that with the lost guards, the situation is more severe than before, and suggests that she might be able to bargain with count rosteforough for a few soldiers to replace the guard in exchange for the food he requests.
--- "Goddamnit, Eris, now isn't the time to talk about that damn parasite!" She sighed, saying she's going to figure something out.
--- "My Lady, it would be best if you did not make a habit of cursing the names of other Lords. It would be troublesome if you mistakenly say something out in public."
--- "Aaagh! I don't care anymore! The guy's a parasite! A fucking boil on the ass-end of this god forsaken kingdom!"
--- "My Lady..." Eris stared at her with mouth agape. "please control yourself from such coarsenesses. It is unbefitting of your noble rank."
--- "Grrrr shut up! Leave me alone!"
--- Being told to shut up reminded her of her service with other Lords - she had been getting rather comfortable with Sylphia, someone who she could occasionally bully and get away with, but with others, she was strictly a servant. She felt an ache in her chest upon hearing these same words from Sylphia, concluding that, in the end, a noble was a noble, and she was nothing more than a servant. She stops being snarky with Sylphia for a while, instead focusing her energy on ruining her by coordinating more and more attacks with Eckhart.
- The bandit attacks continue, but this time, things start going wrong - even without guards on shift, bandit corpses start littering the countryside, near the caravan trail.
- Eckhart whispers to meet with Eris in the street, and they do later that night. She is furious that shit is going down on the arranged ambush spots, and that someone just killed one of her trusted subordinates during that last raid. Eris is frustrated as well, since she is sure that she withdrew the guards from the road ever since the first few raids killed several guards.
- Realize that someone is fucking with them.
- Write another letter to Emi, but in the middle of doing so, she is interrupted, with a call from Sylphia. 
--- She starts writing.

/Dear Emi, I miss you. The new Lord I'm serving turned out to be no different from the others.../


- SCENE:
"Eris," Sylphia said, "is something wrong?"

Taken by surprise, her eyes widened, saying, "What do you mean, my Lady?"

"W-well..." Sylphia scratched her cheek. "It's just that, you've been acting pretty cold lately."

"I don't understand."

"Um..." she crossed her arms. "How do I say this...?" She appeared to rack her brain for a few seconds, before continuing, "You... sort of, um... don't sass me anymore?"

"Uh...Huh?"

"Grmmmm...." Sylphia rolled her head around a couple of times. "I mean, well, you've been acting pretty distant. You used to get really angry with me whenever I slack off, and you also say rude things to me in passing... Well, in retrospect, I sort of like that you don't say rude things to me anymore..." Sylphia looked conflicted as she struggled to say what was on her mind. "A-anyway! You're acting strange! Is there a problem between us?"

/Do I really do those things? I never really noticed myself but, I suppose I did. To think that such things would be unacceptable if it were any other Lord.../ Eris felt, at that moment, a spark in her mind, as if she was coming to a realization. /No, she is, after all, just like those other Lords. Condescending, ungrateful, exactly like them./

Maintaining her composure, Eris simply said, "Of course not, my Lady. I continue to serve you faithfully."

Sylphia sighed. "There you go again."

"Huh?" She tilted her head slightly to one side.

"Normally, I'd expect you to say something like," Sylphia covered her mouth with the back of her hand and then started putting on a fake voice, "'Oh my, it would seem as if my Lady actually has a bit of self awareness.' or, 'No, my Lady, apart from your ritual slacking off of work, your disorganized writing style, and terrible personality, then no, there isn't any problem at all.'"

Eris' mouth gaped open slightly as she watched Sylphia's impression of her.

"Well?" she then asked. "Is there a problem?"

Shaking her head as if coming to just now, she mumbled, "N-no. There isn't..." She felt her chest squeeze tightly. /What is this? Why am I getting pressured over something like this?/

Then, as she was lost in introspection, these words left Sylphia's lips, "Was it because I told you to shut up?"

Like a lightning bolt, her words sent shivers down Eris' spine. She felt conflicted. She knew that the correct answer was no, but the rest of her body was telling her to say yes. As a result, she merely mumbled undiscernable sounds for a few moments, before averting her gaze.

"That was it, huh?"

"N-no."

Sylphia then took a deep breath, and said, "Sorry, Eris."

In that moment, Eris jolted and her gaze jumped right back and landed on Sylphia's eyes. "My Lady," she said, with a low, breathy voice, "a Lord shouldn't apologize to their servant."

"Is that something they taught you as well?"

Eris bit her lips.

"I don't think of you as my servant, you know," said Sylphia, as she looked Eris right in the eyes, "you're my Secretary. And though I didn't mean it," she rubbed the back of her neck, "I mean, I was getting really worked up back then... if I knew it would hurt you like this, I wouldn't have said that."

When she heard those words, Eris felt as if a weight were lifted off her shoulders. /Is this girl truly a Lord? Someone who is outwardly conceited, lazy and narcissistic.../ Gradually, a faded smile appeared on her lips. /But in private, sweet, kind and thoughtful./ 

"No, my Lady," Eris said, "I was not hurt. Nothing my Lady would ever say could ever wound me. However, I was disappointed when my Lady refused to listen to my suggestions."

"Hoh?" Sylphia snicked. "Is that so? Sorry. I'll listen to you more carefully from now on."

"Now that you say that, my Lady," Eris placed her hand on her chin, "I take that you will strive to refine your manner of speech."

"Geh-" Sylphia frowned. "You still remember that...?"

"Of course." Eris smirked.

She sighed. "Fine. I'll try - but don't hold your breath!" Sylphia looked away. "This is just how I usually am..."

"I see." Eris nodded. "Then my Lady should work on maintaining a decent persona while in the presence of others."

"Hey, aren't you implying that my real personality is indecent?"

Eris giggled. "I would never."

Afterwards, Eris went back to her room with a light heart. She intended to continue writing her letter, but found that it was missing from her desk. 

/Wha- where did it-?/

She then noticed that the window beside it was left slightly ajar.

/Oh, for goodness' sake./ She sighed. /I suppose it's fine. I was going to rewrite it, anyway./

Grabbing a new piece of paper, she sat down to the light of her oil lamp and dipped her quill into the ink bottle.

/Dear Emi, I miss you. Don't worry about me - the new Lord I'm serving is kind, and unlike most of the others.../


-- CUT --

- After several days, and the bandit attacks grinding to a halt, Eris is met by the messenger, furious at her failure to deliver either of the Count's requests.
--- "She is proving to be a stubborn little brat. Perhaps her pride would first see Dellwick ruined before she gave any ground."
--- Roderick sneered, "Fine then. We'll have to employ drastic measures. The count stressed that this be our last resort, but now, we have no other options. Before we let this upstart Lady get too full of herself, we must teach her her place directly."
--- "How exactly do you suggest this to be done?"
--- "I want you to kidnap her. Bring her to me, binded, roughed up if necessary, but alive. I will handle the rest."
--- "Kidnap? A Lord? Do you know how risky this is? She has her own bodyguard, you know."
--- "This is the result of your failure. Remember that. Figure it out yourself." Before turning away, he said with a foreboding voice. "God help you should you fail again."
--- Eris gripped her hands tightly together. /So I'm out of options? And no downpayment this time. Damn. This is terrible. Making up for this alone will set me back several years of hard work... No-/ She shook her head. /I can't think like that. I could still redeem myself to the count's good graces. Just no more mistakes.

Please...

God, Devil, whoever is out there who will listen to me, I need your help. 

For Emi./





