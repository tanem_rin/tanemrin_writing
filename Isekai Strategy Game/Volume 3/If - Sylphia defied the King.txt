Pre Note: 
I received a reader comment back in Volume 1, asking, reasonably enough, why Sylphia was acting like a bit of a doormat for the King. It came back to me again today, and I figured it would be the perfect kickoff point for the IF: series.

What is the IF: series, you ask? These will be one-shot chapters that are completely disjoint from the main story, as it shows characters making different decisions than they would have, showing different scenarios for different events, etc.

//But a word of caution//.

As far as I have it planned, this series will mostly contain //Bad End// stories. As such, events will can be more dramatic, dark and violent -or- hilarious and silly, than the main story.

I purposely avoid writing notes /before/ a chapter, but this is just that important: 

//This chapter is very dark//.

You will lose nothing from skipping it.

//You have been warned//.

Sylphia stormed into the room in a bad mood. "Can't you even run your country!?" she screamed. With a scowl painted on her face, she glared at the aged monarch and, in defiance of his rank and the two unknown men sitting on the table beside him, screamed even more, "What the heck are you doing!? You nearly got me killed!"

The three men sat around the table were taken aback from her sudden outburst.

Without even letting them speak, the heroine continued on her tirade. Showing no regard for the King, she released all of her pent-up rage.

One of them, clad in a suit of steel plate, stood up and attempted to calm her down, but was instantly rebuked. 

She went on to complain further about tangential things, including the poorly maintained roads and the lack of a proper sewage system.

It continued for a while, until the little girl finally tired herself out.

She panted.

/Ahh, that feels good. I finally gave this old man a piece of my mind!/

Finally, the king spoke, "Is that all?" 

His cold voice was as sharp as a thousand needles, pricking lightly at the surface of her skin.

Though unnerved, Sylphia maintained her indignant front.

He sighed. "At the very least, I had hoped that you would have the wisdom to realize your circumstances and learn..."

"Huh? What are you-"

"...but it seems you lack even that."

He finally turned his hollow gaze towards her, sending a chill down her spine.

"Or did you not even realize from the very beginning?" He stood up and walked towards her. "Perhaps it is because I, of all people, chose to grant you the respect befitting your title, regardless of any existing merit."

He then stood in front of her, towering over her petite form like a tree would tower over a grown man.

His eyes further narrowed. "In that case, allow me to educate you."

He swung his arm from his side, delivering a swift backhand across Sylphia's cheek.

"Gyah!" she screamed as she fell to the floor.

/Wh-what!?/

Her body trembled. 

Even as tears collected beneath her eyes, she still stove to return the King's glare.

But at that point, even she knew that it was futile.

"You seem to have allowed your title to go to your head, little girl. I wonder if you even realized you pathetic you truly are.

"You are far from the Legendary Heroes, told in the Myths. You could hardly protect a single town from a handful of bandits. Of course I know. The fact that you needed to enlist the help of the town guard to even do anything."

She flinched.

The King's brows furled. "And even in doing so, you fought a hopeless battle. Had Alphonso not been there to save you, then you would surely have perished, along with the town itself."

Her teeth clenched tightly, forming a wide grimace upon her face. 

/Urk! So it was the geezer after all!?/

"And what have you to say for yourself? No humility. No wisdom." The King's voice deepened. "And no power."

She balled her hands into fists to fight the intoletable shivering.

"What use have you, then?" He shook his head. "None. This Kingdom does not need you. You are worthless. Disappear."

Finally, the King passed the little girl and exited the room.

The guards would then come to pick her up by the arms and drag her out of the castle. Along the way, would catch a glimpse her companion, Rika, being escorted by a handful of armed men.

Sylphia was thus thrown out of the castle.

####

Several months later, in the slums of Wyllset...

Sylphia steals a fruit.

She runs into an alley to eat it, but she gets caught.

She gets a beatdown of her lifetime but she's used to it by now.

She has some festering wounds all over her body.

She feels weak.

She feels cold.

She dies.

