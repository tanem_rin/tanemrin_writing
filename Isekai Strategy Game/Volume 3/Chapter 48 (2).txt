Chapter 21
*** Might merge/cut with 3X Sylphia sends bandits into rosteforough.

One day, Sylphia and Eris travelled to Dellwick one more time to finally scout for a location for her future estate.

"Well, I probably want to build the estate by this main road." Sylphia said, as she stood, arms crossed, in the middle of the road cutting through Dellwick.

"My Lady, please get out of the way, you're bothering the passing carts." Eris said.

"Ah," Sylphia leapt out of the way. "that's right, sorry."

Eris sighed. "The problem with that is, there isn't any available space within the village for an estate. On the main road or otherwise. If my Lady insists, then it should be possible to claim a patch of land past the farmlands on the road to Brandonbury."

"Brandonbury...?"

"Duke Hemmingsworth's territory. The one we usually pass through on our journey to Dellwick."

"I-I knew that! Just... needed some time to recall, is all! Anyway, how far would that be from the village center? It should be walking distance, right?"

"Well," Eris adjusted her glasses, "a half an hour's walk."

Sylphia cringed. "You're kidding, right?"

"Think of it as a way to burn the excess fat that you are collecting."

"Ah!" Sylphia yelled, "You said it again! This time, I really won't forgive you, Eris! Take this!" Aiming for the flanks of her waist, Sylphia lunged forward.

However, with a single motion, Eris was able to stop Sylphia in her tracks - by pushing her hand into her face. "What are you doing my Lady?"

"Mmmf!" Sylphia struggled to remove Eris' hand from her face. "Paah!" She gasped for air the moment she was able to swat Eris' hand away. "I'll get you for this! I won't let you keep treating me like a child!"

"But you are a child."

"W-well... wait, I'm not a child!" Sylphia puffed her cheeks out. "I'm supposed to be your master, you know!"

"Pardon me, my Lady." Eris covered partially the smirk on her lips. "But no one will be convinced if you say that while pouting."

"Grrrrr!" Sylphia grimaced.

Later on, they moved on to different areas around Dellwick, in search for a plot for Sylphia's estate. However...

"Uuugh." Sylphia leaned her back against a wall. "I can't believe we walked all over Dellwick and not find any good spot."

"Actually, my Lady," Eris interjected, "we did find two available plots, one being the former reed weaver's shop, and another, vacant plot on the far southeast of the village, but you disliked both of them."

"I told you already - both plots are too small, and neither are defensible without investing in the larger fortification of the village!"

Eris frowned. "Is such a consideration really necessary? My Lady does not need a fortified residence at the moment, and by the time that you do, you should be able to easily afford the fortification of Dellwick."

"You can't be too careful!" Sylphia grumbled. "Tsk, you just don't get it."

Eris sighed. "I am merely advising you to minimize unnecessary expenses."

"Tsk." Sylphia muttered beneath het breath, "Once a miser, always a miser."

Eris furled her brows. "What was that, my Lady?"

"I said nothing! Hmph!" Sylphia turned away from her.

Eris sighed again, this time rubbing her forehead as she shook her head. "At this rate, my Lady will not be able to build an estate at all."

"Gnn..." Sylphia whimpered. "I don't want that, either."

"Then I'm afraid I cannot offer any more solutions for my Lady's predicament."

At that moment, something Eris just said came into Sylphia's mind.

"Wait, Eris, you said earlier that I could claim land for myself, right? How does that work?"

"Hm? Reconsidering an estate between Dellwick and Brandonbury? Very well. It is rather simple. From a law passed by the King to encourage settling in the unpopulated frontiers of the Kingdom, it is possible for any free citizen to claim any unowned patch of land up to the size of 2 acres and establish upon it a homestead."

"Eh? Really!?" Sylphia's eyes sparkled. 

/That sounds really good! Wait... too good, actually./

 She placed her hand on her chin and asked, "Hold on, that sounds too good to be true, right? What's stopping anyone from claiming land in the capitol for themselves? There's a whole lot of open countryside between the city and the nesrby villages."

"Hmph." Eris snickered. "My Lady is quite sharp. Of course, the caveat is that most of the good land is already owned under fief of any one of the Lords, or even the King himself. Thus, the law relly only applies to land that are very far into the frontier, or ones that are unwanted by anyone, for instance, beside an active volcano or near a dungeon."

Sylphia flinched. "Did you say... near a dungeon?"

"Hm? Yes, I did, my Lady." Her eyelids then dropped halfway. "Please don't tell me-"

Sylphia grinned. "Follow me."

The two walked the forest trail out of the village and soon came up to the mouth of the Mythril Cave.

"Heh!" Sylphia smirked as she presented her arms before Eris. "What do you think?"

Eris looked on with indifference.

"The mouth of the dungeon is located on the side of a steep hill. If I built my estate on top of it, I would have clear vision on the countryside surrounding Dellwick!"

"Hmm?" Eris placed her hand on her chin.

"Though it's relatively far from Dellwick, if we upgrade the trail to a paved road, it should be possible even for you to walk back and forth everyday! Apart from that, it will be possible for me to fortify the entry to the Mythril Cave, to make sure no kne gets in or out without my permission!" Sylphia held her arms triumphantly on her waist. "Aaahahaha!" she cackled, "I must be a genius!"

While Sylphia was busy indulging herself, Eris seriously considered the idea. "A hilltop residence? I see..." she rubbed her chin. "I doubt anyone in their right mind would ever lay claim to land right beside a dungeon, so my Lady could probably claim this land as her own."

Suddenly, Sylphia's shoulders dropped. "Hey," she said with a pitiful tone, "did you imply something rude, just now?"

Eris snickered. "I would never."

####

The afternoon sun was still young when Sylphia and Eris returned to their carriage. Their short trip to Dellwick already over.

"I shall be arranging for your claim on that land, my Lady." Eris said, "Please expect more paperwork in the future."

"Geh-" Sylphia winced.

"And please do not delay them. The claims are to be addressed directly to the King, after all."

"Fine, fine..."

"As long as you understand."

The two then spent a moment in silence, when Sylphia changed the subject.

"So, Eris."

"Yes, my Lady?"

"What are you planning to do with the remnants of the bandits here?"

"A-ah..." Eris lowered her head. "With my Lady's permission, I was planning on keeping them in my service."

"Hmm...? Still not done with the banditry business?" Sylphia idly said, whilst staring out the window.

"No, it's not that." Eris clasped her hands on her waist. "There are more uses for outlaws other than banditry."

"Hmmm...?"

"For one, their leader is a close contact of mine." 

"Oh?"

"You recall the person who climbed up my window before we left Dellwick a few days ago?"

"Oh, that bandit!?"

Eris nodded. "In that respect, you could say that I hold a certain amount of influence over the entire group." Eris took a breath. "In that way, the effects of their activities could be moderated."

"What would you say if, for example, I just went in and slaughtered all of them?"

"I think..." Eris but her lip. "It would be wasteful, my Lady. Banditry would not cease from eradicating their group. Dellwick is in a frontier province, after all, and it is close to the border with Rafale. If this group is annihilated, other nearby groups could simply expand and take its place."

"I see... and if I spare them?"

"Then, they would act as a deterrent for other groups to intrude inside Dellwick. At the very least, their attacks upon Dellwick will be predictable, and manageable."

"So they will still be a problem."

"Unfortunately. However-" Eris raised her head and looked Sylphia sharply in the eye. "There is an entirely different use for them, if my Lady is interested."

"Hmm...?" Sylphia rubbed her chin. "Do tell."

"They can be unleashed on your enemies."

#### UNLEASH BANDITS IN ROSTEFOROUGH LAND PLAN

The corners of Sylphia's mouth curled upwards. "That does sound interesting." Sylphia then drew her fan and spread it out, covering her mouth. "Alright, then. I'd like to see how useful a group of bandits could be. In the meantime, I will let you deal with them as you see fit."

"Thank you, my Lady." Eris nodded. "I will see to it that they will be of use to you at any time."

"Mm." Sylphia then returned to staring out of the window. While she did this, she also snuck a peek at Eris from the corner of one eye. Eris seemed quite relieved from Sylphia's decision as she let out a small sigh, before leaning back into the leather seat once more. 

/Strange... is it just me or does she seem to care a lot about mere bandits? Something smells fishy about this.../

Just when Eris had gotten comfortable in her seat and closed her eyes once more, Sylphia opened her mouth. "You said you had a close contact with those bandits?"

Behind her glasses, Eris' eyes opened almost mechanically.

Sylphia shuddered. /Eugh... I don't think I'll get used to how creepy that looks.../

"Yes, my Lady."

"Could you tell me more about that? I just find it strange that you seem so invested in these people that you'd go out of your way to plead their case to me."

"Invested...?" Eris adjusted her glasses. "I... suppose that is correct. Indeed, I am invested in them. I would say... about six or seven years of investment, to be exact."

"Hmm?" Sylphia turned her full attention to Eris. "Tell me more," she smiled, "I'm interested to know what you've been up to all these years."

Eris hesistated at first, but she soon opened up to Sylphia, helped along, in part, by the Sylphia's black mist. "I first became the Secretary of Dellwick seven years ago to replace my predecessor, who had died of old age. We both served the late Marquess Farleigh, the former Lady of Dellwick."

"Oh?" Sylphia leaned in, "Lady of Dellwick? I thought the King owned Dellwick before I did."

Eris nodded. "That is correct. She died in a tragic accident on the road to Rafale several months ago. Normally, fiefdom would be passed to her immediate family, but she was young, and had yet to either marry or birth a child."

"Eh? She doesn't have a family?"

"Not exactly. She had two brothers, both of whom fought for the claim over Dellwick."

"Ah," Sylphia turned to the side and muttered, "Of course they would..."

"However, matters became complicated when the brothers started accusing each other of plotting the death of the Marquess precisely so that they could gain the fief."

"So? In the end, who did it?"

Eris sighed. "Neither one's claims were proven. However, one day, another tragedy occurred."

"Eh? Did they die too?"

"No. Just one of them. Anderson Farleigh had his younger brother, Richard Farleigh killed one day, after being humiliated in a public gathering by the latter."

"Eeeeh? Just like that!? Is he insane!?"

"The brothers neved did get along. In many ways, the eldest sister, Marquess Farleigh, acted as the mediator in their relationship. With her gone..."

"Uwah... well, how did the older brother get caught? He had him killed, right? He didn't kill him himself?"

"About that..." a subtle smirk surfaced on Eris' lips. "it seemed that, just out of utter coincidence, the assassin slipped up and left a clue as to who ordered the assassination."

Sylphia frowned. "Eh? What a useless assassin."

"Indeed." Eris gladly nodded. "And so, with the younger brother dead, and the older brother sentenced to hang, the fief of Dellwick was left without a Lord. Thus, it was returned to the personal ownership of the King."

"Just like that," Sylphia loughed back on her seat, "an entire noble family was wiped off the face of the earth, huh?"

"Indeed." Eris smiled.

"Hm?" Sylphia placed put finger to her lip. "Wait, how does all this relate to the bandits?"

She smirked. "All of it, my Lady."

"Huh?" Sylphia raised an eyebrow. 

"Allow me to continue, my Lady." Eris cleared her throat. "One of the first tasks I was given by the Marquess was to quell the bandits around Dellwick."

"Oho?"

"It was a an exhausting campaign, especially because Dellwick was a very small village back then, even smaller than it is now, and thus did not have the income to support a force large enough to subjugate the bandits." Eris then lowered her head. "But nevertheless, I was ordered to put an end to the banditry no matter what."

"A tough boss, huh?" /I know that feeling.../

"Seeing no end to the campaign by continuing the subjugation, I instead focused my efforts on making contact with the bandits, to possibly direct them to attack elsewhere."

"Eeeeeh? Well that's a creative way to interpret 'put an end to banditry'."

Eris snickered. "No, my Lady. It was the only way. It was either that, or Dellwick goes under, as the subjugation effort forced conscription upon the village. Able farmers were turning to corpses n a weekly basis, and had I allowed it to continue, Dellwick would have fallen to ruin."

Sylphia scratched her head. "T-that sounds familiar..."

"Yes, my Lady." Eris lowered her head. "First and foremost, my position was that of the Secretary of Dellwick, not that of a specific Lord. Therefore, it was my job to place the interest of the village and its people above all else." 

"So, what happened next?"

"I finally managed to meet the leader of the bandits."

"Hoh, your contact?"

Eris nodded. "To my surprise, it was a girl not much older than I was. Eckhart was the name she gave. Just like me, she was an immigrant from the Easter Empire."

"Hm? Immigrant? You? This is the first time I heard of this."

"Ah, yes, my Lady. I gave you my full name, did I not?"

"Mm. Evelyn Canaris, wasn't it?"

"I believed my Lady would immediately notice that it wasn't a Londinian name, and come to the appropriate conclusion."

"Ah-eh-sorry..." Sylphia rubbed the back of her neck. "I'm not too keen on things like those." 

/I'm not from this world, after all./

"I see." Eris adjusted her glasses. "I will keep that in mind from now on. In any case," she continued, "because of our shared origins, we were able to get along rather quickly. Being an immigrant in Londinium is quite harsh, so we had a lot to talk about."

"Huh... I see."

"Ah-" Eris shook her head. "I'm sorry, my Lady, I rambled."

Sylphia smiled. "Don't worry, I enjoy listening to what's on your mind."

Eris' eyes widened. "I..." She then averted her gaze. "thank you, my Lady."

"Hihi." Sylphia grinned. 

/That was a cute reaction... So Eris could be cute from time to time!/

"Ahem." Eris continued, "In short, I was convinced that I could work with her somehow. To begin with, I asked her to divert their actions from Dellwick to the other villages nearby - crossing the border to Rafale in necessary. With that, I thought I would be able to convince the late Marquess to work with them in the same way."

/Eheeeh!? Isn't that exactly the same thing she asked me!?/

Eris' expression turned grim. "However, when it came time to submit my report and proposal, she ripped them apart and threw them in my face."

"Uwah..."

"I had misjudged the late Marquess. I failed to get a good grasp of her personality before acting. I did not expect her to be so upright, and bound by a sense of honor. In retrospect, I should have figured as such, given how much she was respected by the nobility and her brothers. However-" Eris gripped her hands tightly. "She spared no thought for those below her, and demanded the complete annihilation of the bandits, at all costs." 

Eris furled her brows.

"At all costs, she said. But I knew precisely what she meant: At any cost to the people of Dellwick, and to minimize her own."

Sylphia stared at her with mouth agape. "I... didn't know you cared so much about Dellwick."

*****Rewrite to be much cooler, because Eris is a cool cat
("Hm?" Eris looked at her with a baffled expression. "Ah, I'm sorry, my Lady. I did not mean to give you that impression."

"Eh?" Sylphia twitched. "I thoguht it was a good impression, though."

"I..." Eris lowered her head. "do not wish to give my Lady a false impression of myself. What I said... was merely the reason I used to justify it for myself. In truth, I knew that the success of my career hinged on the success of the village that had been entrusted to me. Therefore, my intentions for antagonizing the late Marquess were entirely self-serving.")

"Hmm...?" Sylphia leaned her smug face closer. "Aren't you just playing humble right now?"

"No, my Lady. I am merely offering you an accurate judgement of myself."

Sylphia lounged back. "Ahh, so miss I'm-so-evil actually has an embarrassing soft side..." she shrugged, "who could have guessed?"

"You are welcome to believe that if you wish." she grinned menacingly. "At your own peril." 

Sylphia clicked her tongue. "You're no fun to tease."

Eris chuckled. "May I continued, my Lady?"

Sylphia crossed her legs, placed her elbow the window frame, and held her head up by her palm. "Fine." She then snuck a peek at Eris' smiling face. 

/I've never seen her have this much fun before... that makes be a bit happy./

"Needless to say, I had no choice but to send more people to their deaths. Eckhart's camp is located somewhere in the vast forests between Londinium and Rafale. Even now, I do not know where it is, precisely. Indeed, I believe she would be wise enough to regularly change the location of their camp. Though her occupation is that of a mere bandit, Eckhart is very skilled at what she does, and she has never been caught even once."

"That's high praise, coming from you."

"It is simply the truth. It is no coincidence that she is my most trusted contact." Her expression then turned somber. "I had even considered the possibility of escaping with my sister to join her, if something were to ever happen." She then glanced at Sylphia. "Such as one of my schemes falling through, thus exposing ourselves to mortal danger."

Sylphia winced. "Geh-"

Eris chuckled. "It was not long before I became disillusioned with the Marquess."

"Uhuh?" Sylphia leaned in. "So what did you do?"

Eris' lips then stretched out, forming a wide, devilish grin on her face. "Uhuhu," she chuckled, "I arranged for an accident during one of her trips to Rafale."

"Eh?" Sylphia's heart fell.

"Yes." Eris's eyes narrowed down to thin slits, her pale, gray eyes almost disappearing behind the shroud of her eyelashes. With these eyes, she focused into Sylphia's trembling pupils.

Sylphia felt a cold sweat forming on her forehead.

"Then," Eris continued, "it came to my attention that the fiefdom of Dellwick would then be transferred to one of the late Marquess' younger brothers. They were quite infamous in the Kingdom, my Lady. The older one was a heavy drinker, and a known lecher, while the younger was a limp-wristed brat who depended too much on the patronage of his sister." she held her arms up and shrugged, "I quickly came to the conclusion that leaving Dellwick in their hands would lead to disaster."

Sylphia swallowed her breath. "T-then..."

"Please don't make that face, my Lady I did not do anything to them." She snickered. "But it just so happened that the fool of an older brother approached an assassin under the employ of Eckhart, and she thought, correctly, that I might be interested to know. And so, with a small investment of a few silver coins, I asked the assassin to leave incriminating evidence of his paymaster right at the scene of the crime." Eris then fell into subdued laughter.

"T-that's... frightening..." Sylphia said, fiddling her thumbs on her lap. "Y-you know, you just confessed to killing a Lord, Eris!"

"Hm?" Eris snickered, "If you really intend to turn me in, just the things I've done to you would be enough for me to hang. What are they going to do, execute me twice?"

Sylphia's eyelid twitched. "G-good point..."

/I guess I really shouldn't lower my guard around this one.../

Eris chuckled. "Please be at ease, my Lady. I would not think of ending your life in a similar manner."

"Really...?" Sylphia snuck a suspicious glance at Eris.

"Indeed." She nodded. "Your death ought to be a grand spectacle, worthy of a large audience."

"Hyaaaa!" Sylphia buried her back into her seat, flailing her arms in front of her.

Eris laughed. "I'm joking, my Lady." Gradually, her expression then turned somber, "Where else in this world would I find a master willing to take in even my sister, and promise her an education?"

"Uh-ahh..." Sylphia heaved a sigh of relief.

"Even though I had committed grave treason against you..." Eris hung her head in shame. "I still could not feel at ease, my Lady... It all seems surreal."

"Eris...?" Sylphia attempted to reach out, but held her arm back.

"Why... am I being rewarded?" she gripped her hands tight. "Should I not be punished instead? If it were some other Lord, I would already be hanging by my neck on Dellwick's doorstep, and Emi..." she bit into her lip. "She would have been sold to slavery, or worse still..."

"Stop it, Eris." Sylphia rested her head against Eris' shoulder. "Don't think about that anymore. It doesn't matter. The fact is, I'm your master now. And both you and Emi are under my protection. I won't allow either of you to be hurt anymore. I promise that."

In turn, Eris rested her head upon hers. She she closed her eyes, she said, with a most gentle voice, "Thank you... my Lady."

Thus, the two fell into slumber. Shoulder-to-shoulder, servant and master; Secretary and Lady; each held each other up, even across the rugged path that remained before them. Neither had noticed it, but amid their lively conversation, the sun had already set, and the verdant landscape that rolled past the windows had faded away behind the curtain of night. 

When they woke up, they were still resting on each other's shoulders, as their carriage arrived once more in the castle.




