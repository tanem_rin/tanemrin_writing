
#### REAL LORE ####

- Heroes were always summoned before the Demon King attacked.
- The Elves found a way to do it much earlier through magic.
- The humans, who inherited the elves' magic, also inherited the rituals to summon the heroes as they please, in exchange for a large amount of magic.


- Rafale first summoned Extella months ago when they were repelling Easter's first invasion.
- This reckless move (just the summoning, extella didn't actually fight yet) took the empire off-guard, which forced them to sue for an armistice ASAP, while they summon their own hero to counter-balance it.
- Fearing the heroes of Rafale and Easter, the Grand Duchy and Independent States summoned theirs, followed by Jizzaro.
- Finally, Londinium, who had insisted neutrality for a long time, and tried, with its no-hero stance, to de-escalate the crisis, finally folded, having been surrounded by countries who all had heroes, and summoned Sylphia.
