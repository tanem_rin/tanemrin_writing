

- The empire, reeling against their loss vs the spear maiden, helps Londinium in the war vs. rafale until their successfully recaptured the Flanneries. After which they quickly offered peace. 

- This left a weaker Londinium facing the powerful Rafale, alone.
- Sylphia uses this opportunity to purge the warhawks in the house by letting them lose their armies and thus their prestige in the wars against Rafale.
- Eventually, Dellwick gets attacked, and Sylphia has to defend it. She was supposed to be reinforced by some nobles, but instead, she was left hanging, herself. This move was calculated by Count Christoff. Sylphia was becoming too dangerous at this time. She defends, on her own, against a massive army of thousands with just her garrison.
- She couldn't afford to let the farms get damaged, so she built frontier defenses, where she faced the Raffalian army.
- At this time, she has already perfected the cannon, the musket, the pistol and the grenade. Of which only the grendaes were equipped to her regular garrison.

**** IMPORTANT ****
This means Luna and Klica needs to be met beforehand.

- The desperate battle shows her brilliant use of terrain, her own magic, and explosives. (She learns how to use Tesla Coil here, and becomes pretty fucking OP) Scared by the power of the heroine, Rafale decides to send out their own heroine, Extella. 
- It was a gruesome battle for Sylphia, since Extella just doesn't die. With her buffs, Rika was able to face Extella for a few seconds, but she gets badly injured as a result. Sylphia cries.
- **Sylphia is already retreating, but Extella is pursuing them. Rika protects Sylphie until she gets injured.
- When Rika goes down, Sylphia orders the other soldiers to swarm Extella while they make their escape.
- She's angry, really angry. And she wants to make Extella pay.

- At this point, the Mythril Cave is housing a pretty massive army of siths and kobolds. Many branches have been made, filled with new farms and housing projects.
- 900 sith heavy infantry (tercio formation -  pike and musket)
- 2000 kobold demi-cavalry (swords, pistols, grenades, muskets)
- 20 kobold commandos (non-mythril blades)
- 90 sith heavy assault infantry (swords, grenades, pistols)
- 1 mechanical golem (klica pilot) (HYPER AURA GIRI DA!)
- A bunch of satyrs (living with the kobolds now) (longbows)
- a battery of cannons (manned by Eris' men)

- Against the King's wishes, Sylphia marches them out through Dellwick's streets, armed to the teeth in impregnable new-generation Mythril armor, firearms and everything else. She makes it really fucking grand.
- Without Rika by her side, nothing can contain her rage anymore.
- She wears a death's head cap to battle.
- The battle goes even worse for Rafale than before. With the sudden usage of modern weaponry, and Sylphia shutting down their mage corps all by herself, the Raffalians have no choice but to send out Extella once more.
- Klica is also pissed at Extella because Rika is the first human she came to like.
- As soon as Extella takes the field, Klica jets into battle in her mech. Suppressed by Sylphia's magic and an unending barrage of deadly ball shot, Extella couldn't stop Klica from landing right on top of her.
- Without letting up, Klica then slams Extella around like a ragdoll. Several times, her limbs tore off, but were immediately stitched back to her body via her power.
- Sylphia then gives Klica the signal, who then throws the Spear Maiden into a stack of lit grenades.
- The grenades explode all at once, vaporizing Extella's body.
- But even this couldn't kill her. But it certainly short-circuited her brain from the intense pain. While frothing from the mouth, she begged for mercy, but Sylphia kept stamping her boot on her face, over and over.
- Terrified of what Sylphia just did to their Heroine, Raffalian morale plummeted, and instantly, Extella felt the battle turn against her favor. She was no longer invulnerable.
- What is happening!? That can't be a hero...! That's a demon! How could she do that to Lady Extella! Oh... dear Spear Maiden...!
- Sylphia was about to tear another hole into Extella's head, but she was stopped.
- "Wait!" Extella's trembling voice ripped through the air. "P-p-please..." She twitched. "d-don't kill... me...!"
- "Pfft. What are you babbling about now? You survived this much. Let me have a bit more fun, won'tcha?"
- "N-no!" Her face was red all over and she panted heavily. "This time... I really will die...!"
- "Eh...? What do you have a set number of resurrections or something?"
- "No... it's that..." She bashfully looked away. "...I simply can't be killed while I'm in a winning battle..." 
- "Eh?"
- "But I felt it just now... our side... is now losing! So if you kill me, that will truly be the end of me!"
- "Heeeh?" She put on a sadistic look. "That sounds fine with me, though?"
- "Ahaaahn~!" Contrary to Sylphia's expectations, Extella's face was instead filled with ecstasy. "Y-you fiend...!"
- "Eh?"
- She panted heavily. "P-please don't... please don't kill me...!"
- "I noticed it earlier, but what is wrong with you? Stop panting like an idiot." She slapped her across the cheek.
- "Hyaaaan~!"
- /Eh... this pattern.../
- She slapped her once more across the cheek.
- "Yaan~!" The maiden's knees buckled and twitched from the (shock). Extella than gazed at her with longing eyes. "P-please... don't bully me... in front of my subordinates...!"
- "Heeeh..." She slapped her once more. "And what if I don't?"
- "Hya! Uuuuu... t-then... I might... I might...!"
- "Huh?" One more slap.
- "Oogh! Hya! Hyaa!" Her arms then shot down, covering her crotch. "Ahaaaaaahn~!"
- Sylphia pursed her lips. /Oh my god, this one's a Class-A pervert!/
- With a look filled with ecstasy, the spear maiden finally fell limp on the floor, unconscious.
- "Geez, what the fuck is going on here...?"
- So Sylphia takes Extella prisoner. Taunts the Raffalian army one more time, and then heads back to Dellwick.


