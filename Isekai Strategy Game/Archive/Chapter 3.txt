Arranged in racks around Sylphia, majestic weapons of all kinds displayed themselves, all ready for use. 

"Now, Heroine Sylphia." The king spoke. "Before you are the finest weapons available in this kingdom. Pick any that you desire, and I shall grant them to you."

/Of course... the hero needs a legendary weapon, after all./

She walked around and glanced at the weapons around her.

/They all look powerful. Swords, spears, axes, bows... all really intricate looking and all that./

She grasped the hilt of a sword and attempted to lift it out of its rack. However, as soon as the blade was cleared from the rack, the enormous weight of the sword was immediately transferred onto Sylphia's thin arms.

"Oof!" she grunted.

With a loud clang, the sword quickly met the stone floor and rested flatly on its side.

"I-I'm sorry!" Sylphia lowered her head.

"It is no problem, oh Heroine." The king then turned to one of his servants. "You, return the sword to its proper place."
 
Syphia apologized to the servant as she approached, before moving on to the other weapons.

/Fuck, I've clearly got noodles for arms. Speaking of which... I was never asked to picked the normal attributes like "Strength", "Agility", and all that. How exactly did those stats get distributed?/

She already had a hunch, of course.

/The race and job class, huh? Fairy. Commander. Now if that isn't the most vague pair to find a weapon, I don't know what is.../

The next weapon that she took a shine to was the bow.

/Ah, yes, the bow! The staple of long range combat! As the commander, I'll be relegated at the back anyway, might as well help out with a bow!/

She grabbed the bow by its stem and lifted it off its rack.

/Ah! It's light enough for me to use!/

She pumped her fist.

/I think I've found my partner!/

"King," Sylphia said, "may I test out this weapon?"

"Of course." He then turned to another one of his servants. "Please bring some arrows and a target."

After a few minutes, Sylphia was given a quiver of arrows and, on the other side of the room, a straw target with a red circle was prepared.

/Perfect. Time to show off the goods!/

"Ah-eh?" Sylphia tried to reach the arrows on the quiver on her back, but couldn't quite bend enough to do so. After trying a couple more times, she gave up and took the quiver off, placing it be her leg.

/Shit. I guess I'll need to ask for a waist quiver, then./

Focusing on the task at hand, she shook her head and aimed directly at the target.

/Okay... so... how do I aim this thing again?/

There was obviously no scope or anything on the bow, and there did not seem to be any markers to tell her where to look. 

She clicked her tongue. 

/Maybe... the arrow will serve as the guide?/

She took out an arrow from the quiver and placed it on the bow.

/Ueh? Which side should I place the arrow? Shit, who could have known bows were this complicated!? Fuck it!/

Running out of patience, she simply placed the arrow on a random side and started imitating how she saw bows being used in movies.

"I... just... have to... pull.... right!?"

She released the arrow.

/Where'd it go!?/

She looked around the target but could not find the arrow at all.

"Where did..."

"Um, Miss Heroine..." the servant beside her said.
 
"Hm?"

The servant pointed to something by her feet.

/Ah./

Sylphia picked up the arrow.

/Yeah. Nope. Bows are no go./

The entire afternoon passed, and Sylphia finally came to a terrible conclusion:

/I can't fucking use anything...! Everything needs a certain amount of strength to use! Strength these fucking noodly hands have! If only I picked a male-/

It was as if her brain short-circuited.

/Yeah. No. I would rather die than play a male character./

The king approached Sylphia with concern.

"Did none of the weapons in this royal armory satisfy you, oh Heroine?"

/Geh. He's making it out as if *he* is the one who had the problem. God, I totally misjudged this old man. I'm sorry, old man, you're a pretty cool old man.../

"A-a-ah no, no! The weapons here are quite marvelous." Sylphia lowered her head. "In fact, too marvelous. It seems, in my current state, I am yet unworthy to use any of them..."

/Woo hoo! Fucking Master-class! To think I didn't have to go to drama school to deliver this shit!/

"That is a shame..." The king looked gloomy. "To think that the hero we summoned would be this powerless...."

/Eh? Did he say something actually hurtful, just now?/

"Ah, that's right-" Sylphia said, "perhaps I have some aptitude in magic instead?"

The king placed his hand on his chin. "Perhaps."

He then turned to his last servant.

"Bring me the court mage."

/Shit... This is getting bad. Am I really a hero? If I don't show results soon, I'm fucked!/

She attempted to recall the settings she chose in the character creation screen.

/Eventhough I'm not in the game anymore, it seems that, at the very least, the character I created was transferred into this world. I've got no choice. It's do or die!/

Moments later, a shady guy in a really suspicious looking robe entered the armory. By this time, all of the weapons had already been put away by the other servants, leaving an empty room with a straw target, and nothing else.

"Pleased to meet you, our dear Heroine." The robed man said with an outrageously raspy voice, almost as if he was just putting on airs by doing it. "I look forward to this experiment."

/Experiment...?/

"A-ah..." Sylphia gave a curtsy. "The pleasure is all mine. You will be the one to teach me magic, ye-"

"Alphonso, the Court Mage is here to test your magical aptitude, heroine." The king said, with an abrasive sense of urgency.

/Test...!?/

"Magic is an innate form of understanding of the immaterial world." The court mage said. "In other words, either you have it, or you don't."

Sylphia flinched.

/I don't like where this is going...!/

"It cannot be taught to someone who does not know it," he continued, "though it can be improved, if they know even just a sliver of its truth."

The king then stepped forward. "And there is but a single fool-proof way of assessing magical aptitude."

Sylphia shuddered. "And that is?"

"Self-preservation."

Sylphia's heart dropped.

The next thing she knew, the guards standing by the door had crossed their spears. There was no escape. 

Her heart pumped like crazy. Her legs trembled so much that it felt like her knees were about to give out.

/Shit, shit, shit! If I can't do this, I'll die!/

"Alphonso."

"Yes, sire."

Suddenly, a strange sort of smoke enveloped the court mage's body. It seemed he was also chanting something beneath his breath.

/What should I do!? Do I worm my way out!? Th-they wouldn't harm a cute girl would they!?/

She clenched her fist.

"No... that's not my style." She murmured. "And begging doesn't befit a cute girl like Sylphia...!"

She looked straight at the court mage. "I'll take you on!"

In his moment, it seemed as if the king twitched a little.

/Magic! Magic! How do you cast magic!?/

Sylphia raised her arm and started chanting, herself.

"Hailmarymotherofgodprayforussinners-"

/The hell was I supposed to do!? This is the only fucking chant I know!/

While the strange smoke became much denser around the court mage, nothing seemed to be happening around Sylphia.

/Aaaah! Aaaahh! God! Buddha! Satan! Somebody! Make something happen!!/

Alphonso gradually raised his voice while chanting, and finished by shouting, "I call upon you, Spirit of Flame!"

Immediately, the smoke collapsed around him and turned into a jet of fire that headed right for Sylphia.

"Aaaaaaahh!!" she screamed. "Do something!!"

In the split second before the fire was about to incinerate her, she wished for a bucket of water for some reason. Then, in an instant, a jet of black smoke erupted from her body and transformed into water, drenching her.

"Stop!" the king shouted, and immediately, the jet of flame dissipated, just as it was about to engulf Sylphia.

A large portion of the armory was charred black. In the far center, was the Fairy Heroine, Sylphia, on her knees, all drenched in water, and sobbing profusely.

"So you can use magic, after all." The king approached her. "I am sorry to have doubted you, oh Heroine." He extended his hand toward Sylphia.

"My lord, it seems she is only capable of using the lowest level of magic." Alphonso said.

"I see." The king gazed into Sylphia's eyes. "That's fine. You did say that magic power can grow, even if she only knows a sliver, am I right?"

"Unboubtedly."

"Very well."

The king wiped Sylphia's tears.

"Come now, Miss Heroine, and take your rest."

Sylphia took the king's hand and got back up on her feet.

"We shall discuss the matter of your armaments anther day. You must be tired."

She bowed her head and said, "Yes..."

/The second day of my life as the hero./

/I realize that I am actually worthless.../






