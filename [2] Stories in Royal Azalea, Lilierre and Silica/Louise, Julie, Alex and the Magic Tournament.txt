The stadium had thousands in attendance. Their screams and applause overflowed from the entire venue, and, I'm sure, disturbed the entire neighborhood. This late at night, I'm sure many a grumpy old hag is now cursing at the heavens. No manner of shifting and covering their head under their pillow was going to afford them any sleep now.

Anyway, the fact that this whole thing was staged at this minor stadium was a mistake to begin with. Especially since Halo was taking part.

"Yo! Louise! How are you enjoying the games?" A familiar voice called to me from behind, followed by a rather forceful hit on my back.

"Guh! Oh, it's you." Behind me, a girl with long, flowing red hair, grinned contentedly at me as our eyes met. She was wearing an extremely decent  white dress modestly adorned with (those frilly garland things), which rather elegantly shined against the stadium's lighting. I almost mistook her for someone else. But when I saw that she wore over it a nappy little parka that totally clashed with the dress, I knew. 

Yep. It's her.

"I didn't expect to run into you here, Julie." I said.

"Heheh! I'm a big fan of Mistress Halo, so there's no way I'm missin' this event!"

(Ah, so she is part of the reason the local hags will be losing sleep tonight... And did she say 'mistress'?)

"It was tough, and I had to tell my parents that I had some official business with HQ so they'd give me some spending money to come here, but it was absolutely worth it!"

(Hey, you. You better apologize to your parents.) 

I sighed.

"Geez. You girls are as irresponsible as always." I said, turning my attention back to the center stage.

The battle was starting. The two girls, hardly any more than ants from where I was watching,  stood at opposite ends of the stage and awaited the signal.

Laughing off my comment, she perched herself on the railings right beside me and started poking my shoulder.

"So, hey, hey. I didn't know you were into things like this! Tell me, tell me - who's you're favorite? Mine is Mistress Halo! She is just so cool! Kyaa!" She ran on and on, squealing at her every thought of Halo.

"I already know you like Halo, geez." I sighed. "I don't really like things like this. I'm just here because-"

An incredibly loud buzzer blared in the arena, drowning out all noise for but a second. I could never get used to that loud a noise, and despite hearing it several times already, I couldn't help but get startled by it. And as buzzer died out and the cheers started pouring back into my ears, the battle began.

"Uwooooohhh!!" Julie screamed at the top of her lungs just beside me, but it almost seemed to blend with the cacophony of hysteria, and didn't bother me anymore than the other thousand girls screaming their hearts out.

On the stage below, the little ants started moving. Who were they again? I don't remember. I wasn't actually paying attention. 

The one on the left dashed straight for the one on the right, who was, at the same time, trying to keep her distance. Meanwhile, the commentator's voice blasted through the speaker and gave a close play-by-play of what was happening below. It was a welcome noise. Imagine standing outside a park and looking in, you happen to notice a couple of tiny birds by the fountain, fighting over something on the ground. And you could faintly hear their angry chirping at one another, as they tackle and flap at each other for dominance over that one thing. But you can't actually see what's happening. It's sort of interesting, but you're glued to where you are, and the more you try and figure out what's happening, the more the scene sort of melts away into an incomprehensible blob, like an amateurish shadow puppet show. For us all the way up here, the commentator provided the only way for us to figure out what was happening at all. 

"Oh! [place] Academy is closing fast on [place] School!" The commentator kept referring to the participants by the schools they represented. I suppose that's how it is for these kinds of things - the girls were representing not themselves, but the school that got them in the tournament to begin with. It's no wonder that despite being advertised as an 'Open Tournament', most of the contestants were backed by some school, or company, or some other group.

Crash! Twang! The loud shattering of their magic crashing into each other could be heard, but just barely, above the incessant screaming of the spectators. It looked like the (place) Academy's representative specialized in Self-Augmentation and Defensive Barriers, while her opponent from (place) school was a straight mage, using mostly Combat Magic to keep away from her. It didn't look very good for the (place) School. She kept getting pressured into a defensive by her extremely aggressive opponent.

The arena suddenly shook from a sharp, loud crash, like a very expensive vase meeting its end against a cold stone floor, except heard from a booming loudspeaker. It was just a matter of time. The girl from (place) Academy just shattered the last, toughest shield of her opponent, and she was now moving in for the kill. In a last ditch effort, the girl from (place school) tried to cast an Explosive Sphere at point blank, but with a quick and powerful blow to her gut, the magic spell was cancelled and the poor girl crumpled to the floor. The match was over. 

I should say, I'm impressed. That was a gutsy desperation move. People would normally just guard against the blow and take the loss. Come to think of it, it was a bit too desperate for a minor league like this... Was she trying to show off or something?

"Huah... Huah...! Louise, I'm back!" An exhausted voice behind me said, in between her gasps off breath.

"Oh, Alex." I looked behind only to see her already disheveled appearance, as if she had just finished a marathon. "What happened to you?"

"I'm... huah... showwy... I made a wrong turn from the popcorn stand and... huah.... I ended up on the other side of the stadium..." 

I involuntarily grimaced at her silliness.

"Well, you just missed an entire match." I said.

"Ueeeeh!?" She cried. "I-I didn't miss the (place3) match, did I!?"

"Uh, no. It was (place1) vs (place2)." 

"Oh, thank goodness!" Alex sighed in relief. 

"Hm?" She looked past me. "Who's that young woman beside you? A friend?"

Ah. She doesn't recognize Julie like this. Like, decent. I poked Julie, who was engrossed listening to the announcement for the next bout, and pointed her to Alex.

"O-Ooooh!!! Alex! What a surprise to find you here!" Julie delightedly ran to hug Alex.

"E-e-eeeeeh!?" Alex was startled when an unfamiliar person suddenly hugged her tightly and twirled her around. "L-let me go!!"

"Aww, c'mon, Alex! We ain't seen each other in months! What's a little skinship between friends?"

On that moment, Alex finally recognized her voice and brashness and thick, rural accent. Her eyes widened, and her face contorted. It was as if every ounce of her being did not want to accept that this decent, modest young lady before her was actually -that- Julie.

"Ju-Julie!? Is that you!?"

"What're you talkin' about, of course it's me, silly!"

Oh, that's right - after the Carelia campaign, Alex left the Marines, so she hasn't seen the rest of the squad for a while now. She's kept in touch with me, though...

"The Carelia campaign... huh?" 

I found my mind drifting aimlessly into the past. Seeing things, as they were, back in those days. As if those things happened such a long time ago. I must have looked dazed, or so people tell me, nowadays. The next thing I knew, they were staring at me, as if they were scrutinizing some sort of oddity.

"W-what are you looking at?" I asked. (stuttering slightly as i tried to pretend that i was listening)

"Ah no, uh..." Julie said. "You just went silent again, all of a sudden.

"Huh? No I didn't, you dummy! I mean, we're watching the games, aren't we?" I suddenly started to bluster. "Yeah."

Alex, meanwhile, looked at me as if she was staring at the most pitiful thing in the world. 

"Well, if you say so! Ahahahaha!" But Julie just laughed it off.

Tsk. I accidentally let Alex see me like that. She's always been a sharp eye when it came to things like this. This is why I hesitated to meet her again in person. I'm actually fine, so I didn't want to make her worry about me.

"Geez! I said I'm fine, so there!" I pouted. 

Just as I had finished saying that, the buzzer  sounded again in the stadium. The next fight was about to begin.

The announcer again called out attention to the participants entering the arena. The spotlights focused on a single spot off to the side, as she said, "And now, the match that you've all been waiting for!" The crowd went wild, along with Alex and Julie, as if they knew exactly what was coming next. I certainly didn't.

"-The exhibition match between the graduating class of the Royal University of Sapphire City, and Her Majesty's own Sphere Knights!"

"Eh...?", my jaw dropped. /No way... she can't be that much of an attention whore, can she?/

"First off, representing the Royal University of Sapphire City - ace student of the College of Applied Magistry, Florian Marie Carnelia!"

Alex went wild. "Kyaaaaa!! Sister Ria!!", she hopped and she screamed. I had never seen Alex this excited before. I knew she was close with her sister, but more than that, Alex was also her biggest fan.

"Uueeeeh!? Alex, you're really related to "Diamond Star" Florian Carnelia!?", Julie's face contorted as she said.

"Yes!! Sister Ria is amazing isn't she!?", Alex squealed.

"Yeah! I follow all her matches on the Duel League!" She pumped her fist in the air. "That's incredible! You two are nothing alike!"

"Yeah! She is just so perfect isn't she!?" Alex was so ecstatic that is seemed that she missed Julie's insulting side comment. 

/Heh./ I smirked.

Shorty after the applause started waning, the speakers burst into life once more and the announcer continued, "And, in representi corner-" all of the spotlights trailed over to the other side of the arena, highlighting an young woman bursting with excitement. "



, just as I thought the shouting could not get any lou




























