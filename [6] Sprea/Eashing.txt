Cherry washes her face

Noon. I saw a small pond by the wayside and helped myself to what could be the last bit of fresh water I would find for a long time. A large, smoothed out stone beside the pond resembled a coffee table, on top of which I unconsciously set down Wellington.

I leant over the pond. The water was surprisingly clear. I could easily see the bottom of the shallow pond; no dirt or muck floated above the surface at all. But then, I noticed the water clearly reflected my face, set against a halo of light. I stared into my own weary eyes, boxed inside darkened eyelids, covered in ragged rose pink hair.

Immediately, I felt a deep discomfort welling inside of me (and so) I ran my hands across the water, distorting the image with a swarm of ripples dancing across the surface. 

For a while, I just kept staring blankly into the pond, entranced by the chaotic twisting and turning of my own reflection. No thoughts at all entered my mind. I just stayed still until the image once again slowly reappeared, as the water calmed itself once again.



 If I didn't know any better, I would have drank straight out of it. 










