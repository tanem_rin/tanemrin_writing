The torn landscape was a sight ought left unseen. A single girl strode across the patches of dead land stretching as far into the horizon as the eyes can see. All around her, the scenery unraveled into a patchwork of dead, rotten trees, buried in sickly orange dirt. Their branches darted out in loose spikes, with not a sliver of leaves to their name. The breeze blew across her face with a deep, hoarse whisper. And its cold, cold touch cut deep into her skin. 

"Turn back, turn back.", she could almost hear it snicker. "There is nothing left here."

The girl held a most peculiar object close to her chest - a box with intricate markings of vaguely technological design. She tried to hide it in her coat, wary that a passing misfortune might separate her from it, dooming her entire journey. The wind seemed privy to her anxieties and kept whimsically blowing into her coat, curious as to the nature of this box.

Disaster - they said that she might be able to avoid it if she travelled alone. Disaster haunts the entire Kingdom. It lurks. It hunts. It is especially attracted to crowds. The more people there are, the likelier for disaster to occur. But being alone was no guarantee of being spared from disaster. Far from it.

The girl intended to travel by motorcycle all the way to her destination, but fate had other thoughts. Not too far from her village, from where she started from, yet quite far enough that it was not worth making the trek back, the motorcycle ceased to function. She was dumbfounded. She was not the sort of person to know her way around repairing a motorcycle, to begin with. She could see from the dial that there was still a lot of fuel; nothing in particular seemed odd or broken. Only one thing was for sure.

It was a disaster.

She had intended to call for help, but it turned out that she left her phone at home, of all things. She was now too far away from home, but she was also too far away from her destination. So she walked onwards. She had just checked her map, and there seemed to be a town not too far from where she was. As someone sent on an official mission by The King, she thought that she might be able to use his influence to secure another motorcycle.

She was expecting the worst, however. Many of the towns and villages outside of the capital had long run out of resources or suffered disaster. Either way, they had fallen to ruin. This was especially true of towns further north, where it was closer to The Cavern. Many people believed that the influence of The Great Despoiler had finally leaked out of the Cavern and 'devoured the hearts of men', or so the old timers used to say. Though she had not, herself, believed in any such thing, she was wary enough of the disasters to heed the warning.

She walked and walked, past one nondescript patch of wasteland to the next, when finally, a village came into view.

The girl was already exhausted after a long hike across the wasteland and she was relieved to finally see a sign of civilization. She huffed and she puffed. The strain she had endured up to that point was finally catching up as the thought of relief tottered in her head. Teasing her. Taunting her. She was cautious, but she could not help but feel optimistic, even though optimism had already failed her too many times to count.

She eventually came upon a wide, yet withered farmland just at the edge of town. There a man, tending to the plants, saw her approaching and called out.

"You! Who are ye?", the man shouted from a distance, his voice hoarse and rough as if the wasteland wind ground in his throat as he spoke. 

The girl could make out from the distance that he  wore a (worker's jumper) and he had, slung on his shoulder, a rifle about half his height, resembling the design of old semi-automatics. /A farmer?/, she thought. She raised her left hand as she approached, hiding the box with her right. She made no sudden movements and calmly reassured him that she meant no harm. "I'm a just a traveller.", she said.

"What've ye got there?" The man unslung his rifle, and motioned to the box that the girl was hiding.

"Just a package. I'm supposed to deliver it."

"Yer delivery man now, eh?", the farmer raised his rifle to his hip.

The girl was at a loss for words. She did not know how to both convince him that she was harmless, and to stop inquiring about the box. She would have preferred to keep her Royal sanction a secret until entirely necessary, but she could not think of any other option.

"I'm on official business, sanctioned by the King.", hesitantly, she said. "This package nee-"

"Sh-Wha-! Shut yer yap right there!", the farmer interjected, his face contorted in panic. He lowered his weapon and quickly walked towards the girl.

"Wha-!?", the girl jumped in surprise. The man drew his face close to hers and spoke only in a hushed tone.

"Shhhh! What in blazes is wrong with ye?", the farmer did sternly whisper. "Don't ya be sayin' that yer on anythin' related to the bloody King in these parts!"

"Bloody... King...?", she cocked her head, wondering. /Do people really talk about His Highness so casually in these parts?/

"Come 'ere.", he took her hand and took her into his house, careful in making sure that no one else saw him doing so. 

Inside was his two young children who stared curiously at the new girl from out of town. The girl took off her coat and revealed her head flush with rose pink hair. The children gawked at her as an oddity but it didn't bother her. She was rather used to it. She instead used her odd hair to get along with them.

"Err'ythin about ye's odd as a cow chewin' on a fence post.", the farmer said of her as he handed her a glass of water. "Maybe even odder, act'chally."

"E-ehehe... eh?"

"This is 'bout as much as we can give ya. Hard times n' all."

"No, no. This is plenty. Thank you.", the girl courteously smiled. The water was rather cloudy, and it tasted slightly off, but she made no mention of it. Disaster had struck this village, that much was obvious. She was happy enough that there were nice people who could take her in. Outside, the farmland had mostly withered, and only a few crops in the large open space seemed to be alive. Apart from that, there was also a well from which they could collect water, but not much else. There was no cattle, and no other people.

"Hm?", she noticed something curious. "Is your wife out?"

She saw no other person working the farm and no one other than the children inside the house. She thought this strange, for but a moment, when a painful sensation suddenly struck her. /Uh-oh, I don't think I should have asked that./

The farmer looked glumly toward the children.

"U-uh- you... don't need to answer that..." She covered her mouth, hastily taking back her words in an attempt to make amends, but what was said could not be so easily taken back. "...sorry."

"The same reason why ya' should be out o' this village soon." He pointed to something on her, seemingly unfazed by her insensitive comment. "...with that box."

In her shame, she had unwittingly let them see the box that she was carrying.

"Guh-!", she frantically tried to cover it up, but it was all too late. Unable to hide it any longer, she gave up and showed them the box. It was large and rectangular in shape, slightly larger than one can fit in a single hand. Dazzling and ornate, its designs and markings shined a meek glow of pale blue. It was certainly something that the farmer had never seen in his entire life. He looked at it curiously, and so did the children. The farmer knew one thing about it, however - it looked conspicuous and it looked important, and those were dangerous things to be.

"Bandits, thugs. They come 'round this village on motorbikes er'ry once in a while, and they take whatever they want - food, water...", he then looked to the girl with dead eyes. "...women."

The girl flinched.

"And they really like to take things that look expensive.", he looked cautiously out the window. "And that box - that's exactly the sort'a thing that'd get ya killed fer, lass."

The girl suddenly felt irritable. The thought of violent thugs would regularly come and intimidate good, hardworking folk like them into abject poverty made her blood boil.

"You said they come in by motorbikes?", the girl asked.

"Yeah. They took ev'rythin' with an engine on 'em a long time ago, scrapped ev'rythin' else fer parts."

"Heeeh?" Drawn on the girls face, was a wicked smile, as she said, "I want to meet them."

"What!?" The farmer was flabbergasted. "Weren't ya listenin' ta' anythin' I said!?"

"Mm. I was listening. You said they had motorcycles. Coincidentally, I need a ride to get to my destination in time."

"Bah! Yer crazy if yer' thinkin' o' screwin' with em' gang o' thugs." 

The farmer heaved a deep sigh.

"Look." He continued. "Ye should go. It's a fool's errand ta' be courtin' disaster like tha-"

The farmer froze up.

"Hm?", the girl wondered.

"Shh. D'ya hear that?", the farmer said with a hushed voice.

From a distance, a familiar sound approached. It was not discernible at first, but slowly, the sound grew louder, clearer. It was the unmistakable putting sound of a live engine. There was a lot of them.

"Bloody 'ell froze over - they're 'ere!"

Everyone around the girl panicked. The farmer hurriedly instructed the children to take out the stocks of food, himself running outside to fetch another pail of water.

"Lass!", he turned around. "Ye' should hide yerself! Especially that damned box o' yers!"

Frantically, the whole family readied packs of food and water as an offering to the arriving gang of thugs. Cabinets were flung open, and whatever scraps were packed, emptying the scant food store that they had. 

"Um... what about yourselves?", the girl asked the children, who were busily preparing the offerings.

"Daddy always tells us to give the bad men what they want so they leave us alone." One child answered, while carrying a pair of carrots on his hand.

"He said that if we lie and hide things from them, they'll just get angry and hurt us." The other added, herself pouring water into clay jars.

This downtrodden family, who could barely spare her some water, was about to hand over their entire supply of food and water to a gang of lowlives. What would happen next? They'll just starve? Moreover, it looked like this had happened many, many times before. Such thoughts made her sick to her stomach. 

"Miss traveller, here!", the boy handed her, her coat. "You should hide in that corner by the cabinet!"

But what could they do? If they fought, they would certainly be killed. Though the farmer had a gun, she could tell by his eyes that he has not killed a single man in his life. Perishing the thought, she obediently sat, quietly in the corner, covered herself with her coat and waited against her impulses.

She instead held her ear close to the wall, resigning herself to at least hear what was happening outside. The family's preparation was complete. The farmer went back in and grabbed the offerings that the children prepared.

"Alright, kids. Ye both stay in here, and I don't want ta hear a peep, right?" he held them tightly. "Good, good. Don't worry - they'll be gone soon enough."

Before he went back out, he said one last thing, "Take care o' yer little sister now, lad. I'll be back soon."

"Yes, daddy!"

The girl listened in through the wall. On the other side of the house, the children - brother and sister held on to each other as they waited for the storm to pass.

The engine sounds died. Many people were just outside the house. Footsteps. Cackling. Grunting. What sort of people were these bandits? They certainly did not seem to be the friendly type. Their conversation could not be heard clearly through the wall. She could only hope that it was going well.

"HUH!?" a booming, growling voice screamed.

A loud thump.

"WHATCHA SAYIN THIS IS IT? YE 'OLDIN OFF ON US, OLD MAN!?"

The girl had a very, very bad feeling. The rough voice of the old farmer was hardly audible. He seemed to be meekly explaining why the offering was too little.

"'Oi. Ya better get ya shit straight, or I'll fucking gut ya mate." a different, slithering and nasty voice said.

"'Ey, boss. Din' dis 'ere toff had a lil' girl back when we last came?" yet another, piggish voice, asked.

The farmer, horrified, pleaded that his family be spared, promising as much as twice the usual offering the next time they came around. But to no avail.

"FUCK OFF ME FACE, OLD MAN!"

A loud thud hit the ground.

"IF YE CAN'T GIVE US WHAT YA PROMISED LAST TIME, WHY D'YA THINK I'D BE DUMB ENOUGH TA THINK YA'D GIVE US DOUBLE NEXT TIME!? FUCKIN DUMB GIT!"

Heavy footsteps. They're coming closer.

"BRING 'ER 'ERE."

"A-ah, boss...?" One other, slimy, sycophantic voice said.

"WHAT!?"

"Well, ya know, I'm not really one to be picky, eh? But, I'm really more into the boy, than the girl see? Ya catch me drift?"

"FUCKIN 'ELL, GAGE." 

Everyone suddenly stopped taking for an awkward second. 

"FINE. BRING 'IM FOR THIS POOFTER."

While a single set of footsteps made its way across the wall, and over to the door of the house, the farmer could be heard all the while, his hoarse voice, broken and interspersed with sobbing, still trying to beg for mercy, but no ear could hear his wails. Except for one. 

The girl hiding in the corner ground her teeth. Should she do it? What will happen next? Won't the family be killed afterwards?

The door opened.

"'Ey there lil' kids. Uncle Barry's 'ere ta take ya to da boss now."

The children screamed. There wasno time left to think.

"Wait a second!" The girl emerged from the corner and threw away her coat.

"W-W-W-W-WHA-" Barry, the bandit, jumped back in surprise. "W-Who the fuck are you!?"

"Take me instead."

"No, Miss traveller!", the boy shouted.

The bandit saw her odd pink hair, tied back on both sides, dimly sparkling against the light from outside the window, her exotic dress that he had never seen before, her petite form, and concluded thus: "I might be on to somehtin' 'ere."

"A-Aye.", he struggled to speak. "B-BOSS! YE'VE GOT TO SEE DIS!"

He escorted the girl out of the house and brought her in front of their boss. There were five bandits in total. The boss was a large, muscular man with a large belly. He was flanked by his sycophants, a lanky, sickly looking druggie with bad haircut, carrying a long, lead pipe and a large, piggish man with a few piercings. Behind the girl was Barry, a rather mediocre looking bandit, with a few tattoos, but nothing else, and finally, to the back was a snivelling little lecher, who could not take his eyes off of the girl's body.

"'Oy... Wha've we got 'ere?", the boss closely inspected the girl.

The farmer was laying down on the side, and it looks like he was beat up.

"N-No... ya idiot... I told ya to... shut yer yap and hide...", the farmer, struggling to breathe, forced himself to speak.

"'Ey, 'ey. That's not nice, old man. Why would ya hide somethin' from us?", the boss said as his sycophants cackled around him. "Yer breakin' me 'eart 'ere."

"So." The boss turned to the girl. "What you supposed ta be, Ah?" He snickered. " Don't tell me that old codger's nabbed 'imself a new wife at 'is age."

Barry, the bandit, heckled, "'Oi! That means I might still 'ave a chance!"

The sycophants rolled in laughter. 

"Heh. Heh. 'Specially not someone as... eh, pricey... as you." The stench of his breath as he drew his face closer was a sickening, pungent mix of motor oil and pig urine.

"I'm just a traveller passing through." She looked at him squarely in the eye. In that instant, the boss felt as if a cold needle pierces his spine. He formed a sweat as he felt as if he was being eaten alive by a savage beast.

He stepped back. 

"Boss?", the lanky sycophant said.

"Hmph." He shrugged off the feeling, when he noticed something that the girl was carrying. "Hm? What's that yer carryin' there?"

She carried out with her the intricate box. The farmer, laying on one side noticed this and was mortified.

"Oh, this? It's just a package I'm delivering." She calmly responded.

"What's wrong with this lass?", the farmer thought to himself. "It's like she's beggin' ta get murdered or somethin'!"

"Oh..." The boss stared at it with greedy eyes. "That so?"

There was an awkward silence.

The boss looked at the girl in the eye.

"He has a gun.", a high-pitched voice said, seemingly from nowhere. 

The girl noticed that the boss bandit was drawing a pistol from his hip. The bandits froze and looked around for where the voice came from. The girl took her chance and kicked the gun from his hand, as hard as she could, flinging it off to the distance. She then immediately threw the box behind her, which Barry, the bandit right behind her instinctively caught, while at the same time she jumped, grabbed the boss' head and drove her knee deep into his face.

"GHWA-" The boss lost his footing and fell down.

"BOSS!", the lanky sycophant screeched.

The girl reached into her sleeves and produced a knife, shoving it into the throat of Barry and then grabbed the box back.

"Thank you.", she said in passing.

She then used the knife's handle to control his body while she propped it up with her own - using him as a human shield. The small bandit from behind drew his gun and shot at her, but the bullets only hit Barry's body. She dropped the box, reached up her skirt, then drew her own pistol and fired back at the small man at short range, hitting him twice in the chest. At the same time, the piggish bandit was charging towards her and the lanky one was running away. The girl shot several holes into the large one, but it did not stop him. Finally, she kicked the box as hard as she could into his crotch. The fat bandit crumpled down and did not get up.

Click. Click. She was out of ammunition. The girl could only watch as the lanky one ran into the distance.

"Tsk. That's not good."

The girl took the box and then retrieved her knife from Barry's throat.

The farmer was stunned. It was as if the grim reaper had visited his home and left four bodies in his wake.

The girl walked over to the boss' body and checked his pulse. 

"Tsk."

She drove the knife into his heart and twisted it around.

"W-W-Wha-" The farmer tried to speak, despite his injuries. "What are... ya doin'!?"

"Huh? Oh. He was still alive." She looked at him with plain eyes, and calmly stated it as a matter of fact. "It'll be bad if he wakes up with his face like that."

"What in the world... are ya?" His voice cracked in deathly fear.

"Hmm?" She smiled at him with blood spattered on her face. "I'm just a traveller."

The children watched the whole thing from inside the house. As the dust settled, they ran out and held their father. They cried harder in that moment than any other time in their lives. 

"It's okay. It's okay. Poppa's alright." he tried to console them, but they simply kept crying. For they did not cry out of sadness, but of relief. That their father was still alive, that they were still alive.

"You." The farmer turned to the girl. "You should go. I think ye've done enough damage for me lifetime."

"Huh?" She looked confusedly at him.

"Don't ya get it!?" He shouted at her. "Ya shouldn't 'ave done that... Soon, their friends'll know. And they'll know to come here... And everythin'... Our home. Our farm..." He sobbed. "...Our village... They'll burn everythin' to the ground."

The girl came to the horrible realization. "W-What are you saying?" But she could not accept it. "I-I saved your children, you know?"

"And ya doomed us all."

She lowered her head and tightly gripped her hands.

"Go South." she said. "Take the other bikes. I only need one."

"WE'RE NOT THE ONLY ONES IN THIS VILLAGE, YA KNOW!?", the farmer cried out.

Indeed, from around, people from the other houses gathered. Whispering. Gossiping. She could hear their jeers. 

"What has this crazy woman done!?"

"It's all over for us..."

"What's wrong with her?"

"What the fuck does she think she's doing!?"

The girl stared at the ground, and gripped her hands ever more tightly. She was frustrated. Very frustrated. 

A small hand tugged on her skirt.

From behind her, the daughter of the farmer stood, with teary eyes, and said, with a small, small voice, "I... think you were right... thank you, Miss traveler."

The girl (curled) down and looked into the child's eyes, as tears rolled down her cheeks. She could only muster to say one thing, "I'm sorry." (Maybe add a bit more)

She turned around, and without another word, she took one of the bikes and raced off out of the village. She could not bear to even look at the rear-view mirrors. (Add a bit more)

The wind washed across her face. It was the bitter cold of evening. As she continued to ride north, only the moon and the stars lit her path. In her lonesome, she could not help but recall what she had done. She could not help but imagine: what if she did nothing? Would everything have turned out okay?

Drowning in these thoughts, her eyes welled up in tears once more, and those tears rolled down her cheek, and dripped down, onto the box she carried.

"I thought you were right.", a soft, high pitched voice said.

"Wha-" the girl was startled and stopped the motorcycle. 

"I thought you were right." the voice emanated from the box.

"Y-You... You could talk?" the girl wiped her tears.

"Yes."

The girl averted her gaze. She was confused. So many thoughts entered her head, so many emotions clouded her heart. 

"I... I thought I was right... but..." she said.

"We can't do anything about that anymore.", the box replied.

"Its... a disaster."

"It is."

The girl stared into the clear, night sky. As her eyes traced the dots of the constellations, her thoughts went to the family whose lives she wanted to protect, but perhaps ruined.

"I hope they'll be okay."

"When we get back, maybe you'll meet them again."

"... I hope so."

The girl, with eyes glazed over, looked to the box.

"You... uh, box, do you have a name?"

"Wellington."

"Wellington, huh? I'm Cherry."

"Nice to meet you, Cherry."

Thinking that she had finally gone insane, the girl, Cherry, rode on through the night. There was no time to lose - her journey will not wait.

Disaster.

Disaster haunts the Kingdom. Though it is foolish to think that being alone will spare one from disaster, it is particularly attracted to large groups of people. The farmer and his children took the advice of the girl and fled South using one of the motorcycles that the bandits left behind. Unfortunately, just as he had feared, the bandits arrived many days later, in force, and they, without warning, raped and massacred those who were too stubborn to leave, and those without any means to do so; and in their wake, they set fire to everything.

The great funeral pyre of the village was visible for miles. The girl looked back one night and saw its sickly orange glow. She felt sick the rest of that night and was unable to sleep. Nightmares from that fateful day haunted her and would not let her see rest.

To be continued...

(do a bit more on the goodbye portion.)

cherry vardelcil





