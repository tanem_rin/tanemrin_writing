The Unmarked Forest

The girl, in her lonesome, had long lost track of how long she was travelling. Each step she walked was heavy; each breath escaped as a sigh. She already knew that her journey was pointless. Nothing more than a malicious prank by those who wished to be rid of her. And yet, despite all that, she still took every thoughtless step towards its completion. Perhaps, it was precisely because of what she knew, that she pressed on. Though she always had the inkling that she was thoroughly disliked at her home, what she discovered had finally arrested all of her doubts. After being betrayed by the one last person who she trusted, she had finally lost a place to return to. All she had left was a destination and a package.

"Wellington." she called to her companion.

"Yes, Cherry?"

"How far are we behind schedule?"

"At out current travelling speed, and calculating for the current route, we will arrive at the destination five days late."

"Five days, huh...?"

"...But as I have said, the deadline for delivery does not matter-" Wellington continued. "so long as I arrive."

'...does not matter.'

Cherry smirked, amused by herself at the thought that, all of a sudden, all of the things that she had long thought important suddenly did not matter.

Crack. She felt the crisp crackle of a dead leaf crushing under her boot.

"Hm?" 

She looked at her feet, and, to her surprise, she found that she suddenly was walking on fertile black clay, adorned with a lush carpet of grass.

"That's... weird." She thought to herself. "Wasn't I in the middle of a road not too long ago?"

Looking at her map, there should not even be a forest near her route for miles. She knew that she had been lost in thought for some time, but even so; even if she had wandered off of the road for a short distance then she should still only be surrounded by dirt for as far as the eye can see.

*Pash* She slapped her cheeks, hoping to wake up from what might be a dream. But the dull sting of her now reddened cheeks meant that she was awake.

She looked forward, into the distance. Before her, trees, the bark of each as black as night, surrounded her, menacingly as each stood in a circle around her. Beyond them, she could only see a ghostly pale mist that shrouded everything behind it. She could hardly see farther than a stone's throw away.

Sensing danger, she quickly turned around to retrace her steps.

*Shk* *Shk* 

Leaves crunched under her every step.
 
"W-Wha-!?", to her horror, there was no longer path back. Suddenly, a wall of trees appeared behind her, in a line stretching as far away as the eye can see, their branches woven unto one another like a prickly (fabric/weaving). 

She wiped the cold sweat from her brow. 

"W-What's going on here?", She mumbled to herself; her voice, trembling with anxiety.

Unable to go back, she turned right around. Perhaps, she thought, she could just get across the forest on foot. 

*Shk* *Shk*

The crisp sounds grew duller, thicker.

What she saw behind her sapped the strength from her knees. When she turned around, all of the trees that she thought she saw earlier had moved, as if the forest itself had changed. Every time she would gaze at a different direction, she would find that the trees have moved, and changed. Cherry thought that she must losing her mind.

Cherry snapped. "Ahahah... Ahahaha hahaha... What's going on here!?" 

Her sanity was slipping away. In her despair, she fell to her knees and, while clutching her head, started laughing, uncontrollably.

Gradually, as if from a distance, she started to hear a low buzzing noise. At first, she thought nothing of it, but as the sound started getting louder and closer, so did she become even more nervous. It was as the sound of a locust swarm, flying overhead. She could hear it even if she covered her ears. She could hear it even if she closed her eyes. It was right above her. It was right beside her. It was inside her. She could not escape it - the infernal buzzing.

"Aaaaaaaaah!" She screamed in pain. "What's happening here!?"

"Cherry! Cherry!" Wellington called to her. "What's wrong?"

Cherry was terrified. Her body trembled furiously, as she cocooned herself in her arms. Tears fell from her eyes while she laughed beyond comprehension. In her desperation she called out to the only one who could help her:

"Welling...ton... please, help me!" She cried, in between violent twitches.

"What is happening with you, Cherry!?"

"The... the trees... The buzzing!"

She buried her head into the ground. "Make it stop!"

"Trees? Buzzing? What are you saying? We're in the wasteland, Cherry..."

However, Cherry was unresponsive.

"Are you ok, Ch-" Wellington suddenly cut itself midspeech. "Ah. Cherry, there are people coming towards us."

"Wha... what?" In spite of the pain, Cherry staggered to her feet and tried to look around.

"Cherry, you can ask them for help if you're feeling sick.

However, Cherry drew a pistol and aimed in the distance, while trying to cover her ear with her free hand. "Where!?" 

"Cherry, what are you do-" 

All of a sudden, the tree right in front of her melted away and a figure of a man emerged from it, attempting to grab her.

"Uwah!" startled, Cherry leaped back and fell on the ground. "What was that!?" 

As she gained distance, the figure again melted into the wall of trees, as it had come out.

"What are these... Illusions!?" She thought to herself. "Are these trees are just illusions!?"

"In that case-!" She (quickly got/scurried) to her feet, unslung her rifle and blasted the wall of trees with automatic fire. She saw the images of the trees shimmering as bullets passed through them. (Indeed, she was right, but despite that, she was still unable to know if she hit anything.)

"Tsk!" vexedly, she clicked her tongue.

(Though she knew that everything was merely an illusion, it was still effective in keeping her off-balance and breaking her focus.)

As the smoke cleared, she heard from a distance,  voices, talking to each other, underneath the buzzing. They spoke in technical jargon that she couldn't quite make out, but from their sound alone, she was able to find which direction their voices were coming from.

"There!" 

Cherry continuously fired in the general direction of the voices until her magazine was empty. After that, she heard the voices in panic. 

"Did I get any of them!?"

She tried walking closer, to see if she can make out the enemies through the illusory forest. However, as soon as she stood up, a sharp pain blasted from her thigh, that sent her crashing back to the ground. 

"Gahaaaaaaa!!" She writhed in pain, her voice cracking in agony. 

Cherry was shot. 

She held her injury with her hands, now painted in dark crimson. She squirmed on the ground, trying to find a position where the pain wasn't so intense, but the more she moved around, the more painful her would became. (longer) 

"No...!" 

Her half open eyes glazed over. Tears collected in her eyes and her face clouded over in despair.

"Not again...!" 

Her body started trembling. The fear of being captured once again, being made helpless as she is forced to endure immense suffering, gripped her heart.

"Why does this keep happening to me!?" 

In her grief, she recalled what her senpai told her, and thought,

"Spina was right... I am an unlucky girl.", she sobbed. "Everything that has ever happened to me... to those around me, and why everybody hates me - It's all because I'm a magnet for catastrophe..."

"Everything!"

"And now this...!"

From around her, saw several people emerging from the illusion, coming towards her. They looked to be armed with automatic rifles themselves.

For a moment, Cherry had almost resigned herself to her fate. However...

"No... There's no such thing as luck!" Cherry thought to herself, funneling her frustration into anger. "I'm going to show you, Spina...!"

One of them came forward and grabbed her by the arm.

"I'll show you!"

Cherry pulled the man as he leaned over, tripping him with her leg.

"I'll show all of you!"

She drew the knife she hid in her sleeves.

"I'll carve out my own fate - with my own hands!" She shouted, stabbing him in the neck with her knife, twisting it around to ensure death.

With her one good leg, she sat up and pegged herself to the ground, holding the man's body up with her shoulder. She then drew her pistol and skillfully shot at the other soldiers from a short distance.

Two bangs and two thuds. Cherry shot two more soldiers, each to the chest, throwing them both to the ground. Automatic fire then blared from behind the illusions, hitting the ground around her. These seemed to be skilled soldiers, and they avoided hitting their own - dead or not. She could use that fact to her advantage. 

Though she was well hidden from fire by her human shield, they were still able to scratch her shoulder.

"Guh!" Cherry flinched.

Ignoring the pain, she grabbed the soldier's rifle in front of her and fired back at the direction of the gunfire, emptying the entire magazine. The noise of the crossfire completely drowned out the infernal buzzing, but it was a noise that Cherry was comfortable with.

From the side, she saw one more soldier emerging from the illusion, charging towards her while firing.

"Not so fast!" Cherry taunted him, throwing her rifle over to him. 

The soldier stepped to one side, avoiding being hit by the rifle. Cherry used that opportunity to stand up, and, killing her senses, push her human shield into the incoming soldier. Anticipating her move, the soldier stepped aside again and skillfully maneuvered his rifle around the body to shoot her.

But nobody was there.

Cherry dropped to the ground and stabbed his foot, forcing the soldier down. In between swatting his rifle away and slicing his hand, Cherry used his body as leverage get herself upright.

"Gah! You little-!" the man shouted.

The man struggled and landed an elbow into her abdomen.

"Ghaa-!" Cherry took the full blow and coughed in pain, but at the same time, she anchored herself onto his arm. When she recovered, she pulled him down and stomped on the foot she stabbed earlier.

The man screamed in pain as he fell to the ground, where Cherry simultaneously mounted him on his back, drew another knife from her sleeves, and drove it up his neck, into his brain stem. 

Cherry then quickly grabbed his rifle and looked for her next target.

But there was only silence.

When the smoke cleared. Four bodies were littered around her, and bullet casings dotted the blackened clay of the fake forest.

"There... was only four of them?"

Cherry tried to stand up. Her leg was limp with pain and she tried to stem the bleeding by covering the wound with her hand.

Wellington suddenly crossed her mind. She wasn't carrying her box anymore. She must have dropped it somewhere during the battle, but it must have been somewhere beyond the illusions as she could not find it anywhere nearby.

"Damn... I have to find Wellington." she resolved as she took the first agonizing steps in her search. She was not yet sure if she had taken care of all of the enemies, and so she hesitated to

Search vpr wellington.

in frpnt of her someone wss holdong it.

shot to the chest.

blackout.


 

















