The Contrarian

"Oh my God, Emma!", shouted Marie at the top of her lungs. She picked Emma up, whose entire face was covered in her own blood.

Emma can't see, Marie calls for medic, while she conaoles her. (emma is the medic for their squad)

While this is going on, Angelina and Risa come down. The Confederates are rushing in. Fire bombs start exploding everywhere, setting the watermill ablaze. Marie, Angelina and Risa take the southern approach, holding back several confederates. Black coats flank from the front, engaging them in hand to hand. Marie tries to bayonet one, but is swatted away, she is kicked to the ground, and is shot at, but she kicked clock up quickly enough that she deflected the bullet away from her vitals. She is still shot though but she nevertheless pulls back, pulls her pistol and shoots the black coat point blank a few times.(all in clock up)

she feels sick as soon as she gets out of clock up, but she forces herself up to take cover. voices from the central atrium are telling them to pull back upstairs; they've started breaking through. Marie takes point looking into the atrium; a bunch of black coats have taken cover across the room behind the machinery. They could bot advance closer however as they had fortified the inside portion with a bunch of debris.

Angelina shoots an explosive shell onto the ceiling, partially collapsing it onto the black coats, and creating a thick cover of smoke. With it, They evacuated Bravo from the atrium. They tried to hold the entryway to the stairwell as best they can but in the end, all they could do was delay them. More and more confederates poured in from outside, shooting wildly at eveyrything.

"We can't hold it!", Marie screams as she squeezd herself behind the pockmarked concrete wall.

"Keep shooting! Keep shooting!" Angelina shputed back, holdong her rifle into the doorway and letting out shot after shot without aiming.

- ADD HERE THE FACT THAT IT'S MORNING, SPHERE MAGES COMING FROM THE SOUTHWEST, ENEMIES AROUND THE NORTH -

All of a sudden, Marie's radio crackled to life, and a familiar voice came through a curtain of ststic. "Keep your heads down, girls! This could get messy!"

/That voice - was that... Captai-", before Marie finished her thought, massive explosions rocked the earth beneath them. A blinding blue light sprayed out all over the watermill compound as a deafening, and instantly recognizable sound vibrated through the dusty air.

"Sphere Cannons!" Angelina said, almost excitedly. "Take cover!"

The same thing kept being repeated over the radio as the other squad leaders gave the command to their units.

/Sphere cannons!?/, Marie thought to herself, /That kind of high-level magic means only one thing...!/

Marie, instead of taking cover as prescribed by all level-headed officers above her, leapt on her feet and looked out of the window to the river.

There, she saw it - a brilliant sight that would stay with her to her last moments - streaking across the sheer black night, arc after arc of shining blue Sphere Cannon warheads. She traced their path as they fell to the Earth like shooting stars, except, rather than being swallowed by the horizon, they crashed with force into the area around them. The Confederates not fortunate enough to already be inside the watermill building did not know whether to take cover or to run away, indeed many of them alternated between either as they scurried individually to safety.

A few moments into the shock and awe barrage, several aterliers lit up from across the river, clearly visible despite heir distance against the pitch black.

In the blink of an eye, several shining briges almost ethereal in appearance, materialized over the water's surface,  connecting one bank of the great river vire to the other,

And upon these bridges, the entirety of the 3rd Regiment of the Royal Azalean Marines charged forth. Bayonets fixed and pointed straight forward, they stamped across the bridges made of solid magic led by a familiar group of seven girls, at the very front of which was a particularly familiar little girl with golden locks of hair.

"Sergeant Castell!?" Marie shouted. "And the flunkers!?"

Hearing this, Angelina also stood up against her own advice and looked out the window, followed closely by Risa.

"Reinforcements!?", Risa yelled.

"Yeah", Angelina said. "and they brought Sphere Mages."

"Emma, did you hear that?" Marie said. "We've saved!"

Unable to speak, Emma gave her a thumbs up as she was being treated.

Footsteps then started clambering down the stairwell. Caroline and Cheryl climbed down to meet them along with a lot of the platoon.

"They're here!" Cheryl gave a V-sign. "And they brought the entire regiment!"

Caroline nodded. "And it's time for our counterattack. Let's give them all we've got!"

"Alright!" Marie was pumped up all of a sudden, as all traces of clock up sickness disappeared completely.

Marie and the rest of the squad broke out of the watermill from the south side along with Carol, while Bravo and Delta poured back into the machine room of the watermill to push the confederates back out.

At this point, there was hardly anything left for them to do as most of the enemies had started  routing, and only one or two even bothered returning fire in desperation. When they reached the south exit, they were met by Sergeant Castell, behind whom was the 5th Squad, Epsilon.

"Were we late?" the Sergreant asked. She smiled empathically towards them.

"No", Angelina said. "just in time, Sergeant."

"That so?" She grinned. "Let's get to work, then."

From behind, Marie's eyes met with Julie's, a member of the 5th squad and an acquaintance she made from the marine academy, being that they're from the same province. Julie waved at her immediately, to which gesture Marie replied only modestly. 

Marie's group penetrated the forest cover and formed a secure perimeter around the now devastated watermill without any more significant resistance. The magically guided sphere cannons had sp thoroughly crushed the enemy's morale that they did not bother to delay the marines' advance. Not too long later, Alex, from the 5th squad found the enemy's mortar position, quickly abandoned, leaving several mortar shells behind.

"Let's see..." Marie read aloud the inscription on the abandoned munitions. "Eisen... bach, huh?"

"Eisenbach?" Angelina said.

"You know that name?"

Angelina nodded. "It's a large city in Aria, known for its weapon manufacturers." She bent down and took a closer look, adding, "It's also the name of a company that manufactures weapons from that city." 

"So... it's true then," Marie wondered, "the rumours that these guys - the Confederacy, they beat the Arians all the way from Hale to Izhevsk?"

"Who knows?" Angelina stood up. "Do you think those we chased off just now would have beat the Imperial Guards?"

Marie flinched. "I- uh, I guess not..."

"Something's not adding up here..." Angelina's brows furled.

Later that day, the fighting finally lulled down and the 3rd Royal Azalean Regiment started setting up their field headquarters in the watermill.

"Marie, come over here", said Angelina.

"Yes, boss!" Marie stood up and left the company of the rest of her squad. "What is it?"

"Here." Angelina handed her a stack of papers.

"Huh?"

"You were there when the prisoner escaped, weren't you?" Angelina then handed her a pen. "Write a report."

"Wha- eh?", Marie moaned. "But Emma was-"

Angelina cut her off, "Emma's going home."

"Eh?" Marie froze.

"You're actually surprised?"

"Ah- n-no..." She then looked down. "I just-"

"She'll be fine, but it looks like she'll lose an eye."

"No way..." The guilt that Emma had no time to feel earlier on suddenly pooled up inside her.

"So, anyway, please handle the report." Angelina said in her usual monotone before walking away.

"Yes, boss..."

As she sat down to recall the events as they took place, she remembered the face of the prisoner, i  particular, her wretched smirk as she leapt out the window, leaving a bloodied Emma writhing on the floor. Marie grit her teeth, and swore to herself, "I won't forget this... I'll find you, and you'll pay for what you did to Emma...!"















