﻿（*）
私は私の指を緩める
それをすべて投げ捨てる
それで、ある日
それもメモリにフェードインすることができます

それは傷つくでしょう
それで耐えて、今は耐えなさい
まもなく私の感覚は鈍くなります
そしてそれも記憶に消えることができます

私は眩しいホールであなたに会った
私たちの心が同期している場所。

しかし、その負担は大きかった。
あなたの負担。

私はそれについて何も知らなかった
しかし、あなたはまだ私に抱きしめていました。

しかし、時間が経つにつれて
私たちの心はもはや同じリズムを打つことはありません

（*）

私は混雑した劇場であなたに会った
私たちの思考が同期していたところ

私たちは不条理のカーニバルを見ました
嘲笑は人生の道だった

しかし、その負担は大きかった。
私の負担。

群衆の騒音は私のためにあまりにも大きかった
しかし、私は

しかし、時間が経つにつれて
そして私はもはやこの負担を負うことができませんでした

（*）

私は私の心を失う
それをすべて投げ捨てる
それで、ある日
痛みは消える

もはや痛くない
耐えられるものは何もなく、何もない
そして私の感覚は鈍い
私も消え去ります

(*) Watashi wa watashi no yubi o yurumeru sore o subete nagesuteru sore de, aru hi sore mo memori ni fēdoin suru koto ga dekimasu soreha kizutsukudeshou sore de taete, ima wa tae nasai mamonaku watashi no kankaku wa nibuku narimasu soshite sore mo kioku ni kieru koto ga dekimasu watashi wa mabushii hōru de anata ni atta watashitachi no kokoro ga dōki shite iru basho. Shikashi, sono futan wa ōkikatta. Anata no futan. Watashi wa sore ni tsuite nani mo shiranakatta shikashi, anata wa mada watashi ni dakishimete imashita. Shikashi, jikangatatsu ni tsurete watashitachi no kokoro wa mohaya onaji rizumu o utsu koto wa arimasen (*) watashi wa konzatsu shita gekijō de anata ni atta watashitachi no shikō ga dōki shite ita tokoro watashitachi wa fujōri no kānibaru o mimashita chōshō wa jinsei no michidatta shikashi, sono futan wa ōkikatta. Watashi no futan. Gunshū no sōon wa watashi no tame ni amarini mo ōkikatta shikashi, watashi wa shikashi, jikangatatsu ni tsurete soshite watashi wa mohaya kono futan o ou koto ga dekimasendeshita (*) watashi wa watashi no kokoro o ushinau sore o subete nagesuteru sore de, aru hi itami wa kieru mohaya itakunai tae rareru mono wa nani mo naku, nanimonai soshite watashi no kankaku wa nibui watashi mo kiesarimasu

(*)
	I loosen my fingers
	Cast it away, all away 
	So that one day
	It too can fade into memory

	It will hurt
	So endure it, endure it for now
	Soon my senses will dull
	And it too can fade into memory

I met you at the dazzling hall
Where our hearts beat in sync.

However, the burden was heavy.
Your burden.

I knew nothing about it
But you still held on to me.

But time eventually passed
Our hearts no longer beat the same rhythm

(*)

I met you at the crowded theatre
Where our thoughts were in sync

We watched the carnival of absurdity
Where ridicule was the way of life

However, the burden was heavy.
My burden.

The noise of the crowd was too much for me
But I pressed on

But time eventually passed
And I could no longer carry this burden

(*)

I lose my heart
Cast it away, all away
So that one day
The pain will go away

It no longer hurts
There's nothing to endure, nothing
And my senses dull
And I too can fade away


yubi o yurumete
subete o (nage)sutete, sutenayo
sore de, aru hi 
subete wa kioku ni toozakaimasu you ni

kizutsukudeshou  (itami mo kanjimasudeshou)
sore de taete, ima wa taete
mamonaku kankaku mo nibuku narimasu 
soshite sore mo kioku ni toozakaimasu you ni 

watashi wa mabushii hōru de anata ni atta watashitachi no kokoro ga dōki shite iru basho. 

Shikashi, sono futan wa ōkikatta. 
Anata no futan. 

Watashi wa sore ni tsuite nani mo shiranakatta
shikashi, anata wa mada watashi ni dakishimete imashita. 

Shikashi, jikangatatsu ni tsurete
watashitachi no kokoro wa mohaya onaji rizumu o utsu koto wa arimasen 

(*) 

watashi wa konzatsu shita gekijō de anata ni atta 
watashitachi no shikō ga dōki shite ita tokoro 

watashitachi wa fujōri no kānibaru o mimashita
chōshō wa jinsei no michidatta 

shikashi, sono futan wa ōkikatta. 
Watashi no futan. 

Gunshū no sōon wa watashi no tame ni amarini mo ōkikatta 
shikashi, watashi wa 

shikashi, jikangatatsu ni tsurete 
soshite watashi wa mohaya kono futan o ou koto ga dekimasendeshita 

(*) 

watashi wa watashi no kokoro o ushinau 
sore o subete nagesuteru
sore de, aru hi 
itami wa kieru 

mohaya itakunai 
tae rareru mono wa nani mo naku, nanimonai soshite watashi no kankaku wa nibui 
watashi mo kiesarimasu