The Maelstrom

Hoshino did it. She detonated the (royal azalean) World Cell with a crystal of red strain. Because of this, Sapphire City itself was wiped off the face of the world, and red strain crystals started terraforming outside of Caparice. The extreme energy of the explosion vaporized everyone in the vicinity, except Hoshino who managed to hide in the shadows of existence, similar to Crescent's ability to meld with darkness. At the same time, the extreme psychic shock of the red strain detonation overwhelmed all of the Yuri back in Caparice and most of them went feral and transformed with the red strain that had become like Tiberium.

In this time, Hoshino went into hiding, disappearing without a trace.

Nearly a century passed. Most of the civilized world in 1932 had become a red wasteland. Instead of infecting and transforming plants with red strain, the crystal that created the Yuri instead took a life of its own, growing anywhere and everywhere, sapping the energy of everything it touched, creating frozen wastelands spanning most of the north and central regions. 

The predictable wave of refugees caused by this crisis led to the collapse of all known countries, and all of humanity became essentially nomads, perpetually on the run from the red strain and the feral post-Yuri. 

Not all Yuri immediately became feral. Those who were paeticularly young and thus had not the perfect psychic link with the rest of the Yuri were unaffected. Others, who had an especially strong mental discipline were also able to escape turning feral, though at the expense of a lot of their previous mental faculties. These survivors are not hunted by the ferals, who instinctively know them as one of their own, but they are treated severely by the rest of humanity as the bringers of the end.

The Maelstrom, a dark story of survival in a dying world, eternally on the run from the ever-expanding red strain, follows the intrepid researcher Chie Verdant, a young savant with grest understanding of the sciencies, and, in particular, of the nature of red strain. She, along with her sisters, Lunasica and Sola-Rei Verdant, attempt to eke out a living in the apocalyptic world, living right at the boundary of the strain's expansion. They make a living from salvaging (mostly) abandoned cities which have only started to be assimilated by the red strain.

It is not an easy life, as cities of old have become the only places possible to gather advanced tech and industrial manufacturing, many ruthless salvagers also have the same thing in mind.

More than anything, however, Chie desires to find some way to combat the expansion of red strain, in the hopes of reversing it to reclaim the world from the menace. 

=============================
Endgame

Chie is led to the 'Gardener King', whose scythe was said  to carry souls to the next life. She finds the 'Scythe of the Gardener King', (tentatively) a witch familiar capable of abaorbing energy from anything and storing it within the familiar, for use with anything. She is then tricked by (definitely not) Hoshino to use it on the Ground Zero of the red strain infection, the old dead city of Sapphire. The reaction of the scythe immediately triggered a chain reaction on all existing red strain, releasing their absorbed enerby, transforming them into flying masses of energy, all flowing back to the very source - the World Cell rebuilt, now controlled by Hoshino. Chie was the first victim, her body was slowly vaporized by the balls of energy and she was sucked in, transformed into a mass of energy herself as she lost her life. However, he did not feel dead. Indeed, inside that mass of energy, her consciousness melded with all those of the Yuri who had also finally died with the implosion of all the world's red strain. She understood their pain, their joys, their memories, as she faded into their embrace, all she could think of was her family and how she wished she could be with them, even if only in counsciousness, once more.









