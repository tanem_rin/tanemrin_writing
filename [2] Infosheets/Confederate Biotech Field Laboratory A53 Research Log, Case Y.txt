This is a Classified File admitted into the Archives of the Library of Lilierre. This document was acquired by the Lilican Intelligence Services from an undisclosed source from within the Confederacy. Illegal Possession of this document is punishable by immediate execution. If this is returned to authorities in untampered form and the information included within was not read, the punishment is reduced to at least 20 years in prison, including 240 hours worth of corporal punishment.

Biotech Field Laboratory A53, [REDACTED]
Research Log
Dr. Cfr. [REDACTED], Biological Warfare Specialist, [REDACTED]

4/64/[REDACTED]

0455 Hours

Specimens arrive in cargo dock. Orders received from [REDACTED] to study the biological composition, anatomy, psychology, physiology, and any interesting features, such as weaknesses or strengths of the specimens. Code Black - Specimens Expendable.

0515 Hours

Guards put on high alert, specimens now unloaded. Numbering 6. Small, like children. Slender build, all of them. Hair with sickening red hues as in early reports. 2 visibly shivering. 2 crying. 1 Unconscious (dead?). 4 with visible injries, one of them with several burns.

0533 Hours

One more unloaded, conscious, visibly angry, still small, but seems most dangerous. Will dissect first. Assigned two guards on just that specimen, to be sure.

0645 Hours

Interred specimens into their permanent cells. Several put up a struggle. Idiot guards used shock prods. Could have easily thrown them inside instead. Hope it does not interfere with research. 2 more now have some burns. Would have those guards punished, but there is too much to do. Will instead give them stern lecturing later with the underside of my boot.

0901 Hours

Specimen Y-001

Initial description:

Height, 140 cm.
Weight, 16.1 kg.
Skin, Very Pale.
Hair Color, Pale Red - Pink.
Injuries: Prod burns on waist, scrapes and bruises on arms, torso. 1 possible bullet wound on hip, glancing, light bruising on wrists and ankles from operating table restraints.
Notes: Specimen remains defaint even while in restraints. Will leave guards on duty while observation continues.

1022 Hours

Lab attendant reported anomaly. Some wounds visible from initial records are gone. Some scrapes are gone. Burns have reduced, bullet wound is... closing? Alerted all research staff to immediately observe and record the status of the other specimens. This is very interesting, indeed. 

1050 Hours

My suspicions have been confirmed. 5 specimens looked to be in better condition than initially thought, contrary to my initial observations upon unloading. 1 still unconscious, but still breathing.

1130 Hours

Sent Dr. [REDACTED] to perform tests on specimen's regenerative ability. Indriduced lacerations on left arm, leg and half of torso, Used hammer on right arm, leg and half of torso.

Introduced cloth to mouth to suppress noise.

1200 Hours

Dr. Cfr. [REDACTED] has arrived and will begin the phychological research. Requested 2 specimens. Provided Specimens Y-002 and Y-003. Asked, in return to record the initial status in my stead.

1330 Hours

After long observation, it seems the regenerative ability of Specimen Y-001 is not quite as fast as I had naively hoped, but it is still remarkably fast - definitely several times faster than human regenerative capabilities.

Lacerations have slightly closed, but internal bleeding seems unchanged. Will allow Specimen Y-001 back into cell for a while and prepare the other research topics.

1345 Hours

Dr. Cfr. [REDACTED]'s initial reports have come in. It seems the tests are now underway. I will observe for a bit.

1558 Hours

Initial findings are not very interesting. They are obviously social animals. Research on intelligence involving the shutting door and the one way mirror to the cell of the other specimen was midly amusing, but that was it.

1630 Hours

All specimens returned to cells. Nearing time to close the Lab.

1800 Hours

Daily exit reports as follows:

Specimen Y-001

Retained most injuries sustained on first round of research, with moderate, but still relatively exemplary healing, considering it was not treated with medicine.

Unconscious.

Specimen Y-002

Sulking in corner

Specimen Y-003

Asleep. Burns still visible on body, but markedly less than what I observed in the cargo bay.

Specimen Y-004

Sitting on bed. Won't make eye contact.

Specimen Y-005

Asleep. Or maybe still unconscious. We will wake this specimen soon.

Specimen Y-006

Alert, Sitting on bed, looking defiant. Will be the next sbject for tomorrow's experiments. 

Specimen Y-007

Crying, still. Note an interesting abilty to cry for extended periods of time. Amusing for a while, but guards are starting to complain. May prioritize for experiments to hasten disposal.

4/65/[REDACTED]

0514 Hours

Entered the holding cells, and the specimens were talking to one another. I must admit to this failure, and will rightly suffer my punishment when this report reaches the [REDACTED]. Had the guards beat the all of the specimens. One asked if they should beat Specimen Y-005, which has been unconscious since arriving here. Decided that the time to wake it was come and commanded them to proceed.

0608 Hours

Specimen Y-006, in the room next to Specimen Y-005 was shrieking loundly (speaking?), pointing to the cell of Specimen Y-005. 

It seemed that Specimen Y-005 was not responding to the beating. I commanded them to stop to check on its status. Breathing, very faint. Nearing death, I would surmise.







