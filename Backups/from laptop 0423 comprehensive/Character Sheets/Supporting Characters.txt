########################
###### Londinium #######
########################


#### Dukes ####
Duke Saunders of Ealdshire - Fat old man who plays nice with Sylphia. Corrupt, pockets some of the money that is supposed to go to improving the infrastructure of Ealdton

Duke Hemmingsworth of Brandonbury - An intellectual?

Duchess Elizabeth of Brimmingham - The crown princess of Londinium. Her beloved younger sister, Victoria, died in her watch. This made her rather protective of the childlike heroine, Sylphia.

Duke Velosa of Castell - Father of Luna. An integrationist.





#### Marquis ####
Marchioness Farleigh - Old lady that was killed by Eris

#### Counts ####
Count Gerard Christoff of Morterdale - Dandy old man with greying beard and dangerous look. Opportunist at heart with a powerful military behind him. Plays nice with Count Rosteforough, but actually wants to topple him, so he can be the sole military power of Londinium.

In the Londinian Civil War, he was part of the Aristocratists who switched sides, in favor of the current King. An opportunist at heart, his loyalties extend no farther than his own benefit.

He commands the Knights Azure, a powerful cavalry based army that forms the backbone of Londinium along with the 4th Linden.

True to his nature, he took Countess Charlotte as wife, not only because of her beauty, but because of her political connections, including being the godchild of Duke Hemmingsworth.

Count Martin Rosteforough of Lindenshire - Old man with large belly; army.

In the Londinian Civil War, he led the 4th Linden to crush one of the royalist armies. With the current King grabbing power before he could make it to Aubury, he laid down his arms immediately after the annihilation of a fellow integrationist's army.

Because of his early surrender, and the influence he had on his largely undiminished army (something Londinium needed after the destruction wrought from the civil war), he was allowed to parley, and, at the cost of a few of his fiefs, allowed to serve the new King with his army intact. Ever since then, however, his and his family's influence had severely diminished.

Though a skilled tactician, he isn't very well liked due to his personality.

Countess Charlotte Adventine - A youthful yet old lady (30's), daughter of the Adventine Mage Family and godchild of DukeHemmingsworth. Almost as opportunistic as Christoff, fell in love with him due to his personality and success. Does her best to support him, but looks forward to his death in wealth, so that she inherits all of his possessions.



#### Barons ####

Baron Michel Ingram of Seventh Arc - Headmaster of the Seventh Arc Academy, head of the one of the great mage clans of Londinium. Integrationist.


#### Aster's Team ####

Lil - Youngest recruit. Emotionless killing machine. Her skill is without question, but her experience is lacking. Scared of nothing, except Aster, who she calls 'Boss'. Thinks of Kiya as her older sister and Garm as her older brother. 

Personally scouted by Aster from her orphanage, being known as a problem child. Though she gets teased by her Boss a lot, she actually holds her respect and has high hopes for her as she grows up in the profession.

Likes stars, because looking up at the night sky was one of her few means of entertainment when she was in her lonesome at the orphanage.

Kiya - Aster's assistant. Adept at handling poisons and the machinery to deliver such poisons. She's a very good shot with the crossbow. Responsible older sister type to everyone, except Aster, who she treats as a close friend. Calls her 'Lady Aster' anyway to set a good example.

Owns the "Clay Mask" artifact, which allows her to shapeshift her face.

Garm - Big, quiet man. Gets the job done. He keeps to himself, though shows a bit of affection for Lil, who he trained. Calls Aster 'Boss', passing the habit to Lil.

#### Adventurer's Guild ####

Christa - 

Ruth

Ceressa



#### Dellwick ####

Shirley Ann Elroy - Granddaughter of the Elder of Dellwick. Granted scholarship into Academy of Brandonbury at a young age because her mother was magically gifted, plus, because of her mother's valiant service in the the duke's mage corps and her father's service in the regular army. She's a gifted mage who will eventually learn earth magic.

Has a bit of Raffalian blood.

She will be schoolmates with Emi in Chapter 4.

Dante de Salvatore - Fat merchant. Trader for Sylphia's Mythril.


#### Academy of Brandonbury ####
Shams - Luna's close friend in the alchemist's college. Part of her inner circle with Stella, but has a lot of friends, herself. A kind, diligent student, with some talent in Alchemic theory. From Zahar Eldin (province/branch/etc) of Jizarro

Chara ref. is Bergamot. With glasses.

Stella - A strict girl from the warlock's college. Childhood friends with Luna, who also came from Castell. Not particularly talented, but hangs in there.

Will be the first Luna will hook into practising Magic for Sylphia.

Chara ref. is ? (Caffelatte?)

Luna's other friends - comprised of the below average members of the student body. They look up to Luna's ability to get passing grades without trying, as well as her happy-go-lucky attitude, a rarity in the academy.

########################
######## Rafale ########
########################


##### Senators #####

Lilienne le Forzette <la-for-zett> - is nearing her 30's (29), and she had just had her daughter when her husband died, leaving no son to inherit his name. She is a laid-back mom type but with a psychotic streak.

Just wants to bring down the uppity senators who condemned her husband by forcing him into a war he voted strongly against, and seemingly as punishment, had placed him in charge of a bloody battle that claimed his life. They think she doesn't know because she's just a woman who rose to the seat of senator due to the graces of her husband becoming a hero.




Noemie Liese Crotale - is merely 22 years old and the family is currently searching for a fiance' for her. She is a serious girl who does no jokes. A total psychopath, like Eris. Goth loli waku.

Is aiming to install an autocracy in Rafale like the one in Easter, centered around her, of course.

Delusions of grandeur, except she is competent enough to make her delusions reality.

Wants peace with Easter to conquer the weak Londinium.

Very few like her, but many respect/fear her.
