"By the way, Miss Heroine, how is Dellwick coming along? I do not wish to think that I tasked you with something you could not handle...

After all, as small a village as it may be, Dellwick is still an important center of agriculture. We can't afford to allow disaster upon it."

