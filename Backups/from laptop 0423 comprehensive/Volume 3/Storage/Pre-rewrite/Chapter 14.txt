Chapter 14 - My Village
*General revision notes:
- Add scene with Cecil delivering Sylphia's new clothes. (next chapter?)
- Might split in two; insert Cecil's scene somewhere.

The first thing next morning, Sylphia, Rika and Eris left the inn. It was no more than a short carriage ride before they finally arrived at Dellwick.

They entered through the same dirt road as they did the last time, past the same fields, now flush with ripe bounty, across the same central road filled from side to side with villagers lugging about carts and baskets of fresh wheat, soybeans, and many others all over the village.

Rika looked in awe of what she saw around them. "Sylphie, Sylphie!" She patted Sylphia on her thigh. "This place feels different, doesn't it?"

There was a palpable cheer in the air that contrasted greatly with the somber mood that she was greeted with when she first visited Dellwick. The mood was such that, for the moment, Sylphia managed to forget her anxiety.

"Mm." Sylphia nodded. /This is probably be their first harvest since I lowered their taxes, huh?/ 

"Ahaha!" Rika stuck her head out and waved to a group of children running after their carriage behind them. ("They look so happy now!")

"Is that so?" Sylphia smiled. /Who wouldn't be happy to have lower taxes? I guess the ones doing the taxing, but it's not as if I'm doing it out of altruism, anyway.../

While she lost herself in thought, she noticed Eris sitting silently in her corner of the carriage. She had been like that ever since they arrived in the village. /What's she thinking about...?/

"Hey, Eris," Sylphia said, "what are your thoughts?" 

Like a golem coming to life, Eris opened her eyes, turned her pupils towards Sylphia and said, "Of what, my Lady?"

"Dellwick. In general."

"The tax reform should reduce unrest, but it will also reduce the available administrative funds, as well as attract wrongdoers to the village."

"Hmm?" Sylphia cocked her head. "Attract wrongdoers, you say? How so?"

"Now that the Lord's wealth is distributed to the villagers, they will soon become targets for those who desire that wealth for themselves."

"Ahh..." Sylphia nodded. 

/She makes a good point. If the citizens become wealthy, then it becomes profitable to steal that wealth from them. Unlike Lords and Nobles, who can afford personal protection, they can only be as safe as I am willing to invest in their safety./

Syphia then crossed her arms and lounged back, "Then it just became more important to organize a better guard more quickly. Mm," She nodded again, "it was a good idea to come here after all."

Without saying a word, Eris simply adjusted her glasses and closed her eyes once more.

Sylphia snuck a glance at Eris. /Ugh, I can't get a bead on her reactions... does she hate me or something?/ With a sigh, Sylphia resigned herself to watching the scenery pass by. /'Different plans'... I don't want to think about it, much less believe it, but I should at least consider the possibility.../

####

They soon arrived at their stop right in the center of the village. There, a man in light leather armor approached them.

"Miss Eris, good morning," he said, nodding to her, "I was not expecting your arrival." His eyes soon turned to Sylphia and Rika. "These ladies are...?"

Eris waved her hand towards Sylphia, and then to Rika. "This is the new Lord of Dellwick, Lady Sylphia. And this is her escort, Knight Evrika Carlisle." 

"Eh!?" In an instant, the man tensed up in salute. He struggled to roll words off his tongue, "A- m- err- my Lady!"

Sylphia chuckled. "It's fine, relax. It's my first time visiting Dellwick..." She then turned to one side and mumbled to herself, "...as Lord, that is." She then extended her hand and said, "please to meet you, um..."

"Ah! Gray, ma'am!" he said, before taking her hand and kissing the back of her fingers.

/Gnnnnngggh!/ Sylphia cringed as his lips coming into contact with her skin sent shivers down her spine. In spite of this, she managed to keep her body from jerking back, and she calmly withdrew her hand. /What the hell!? Do these people not know how to shake hands!? Geez, I need to wash my hands, pronto!/ While she screamed internally, she kept a cheerful facade, through sheer willpower and subtle gnashing of teeth. "Ahh, Gray. Very nice to meet you. Yes."

"Thank you, Lady Sylphia." He grinned as he scratched the back of his head. "To be honest, I had not expected the Lady to be so young."

"Hihi, I get that often." Sylphia smiled while she withdrew her hands behind her back, feigning a cute pose, but actually wiping her hand on her dress. 

"Aha, no doubt!" Gray's eyes then fell on Rika. "It is a pleasure to meet you as well, Miss Carlisle."

"Ehehe," Rika giggled, "no need to be so formal with me. I was a guard just like you just a few months ago!"

"Ohh!" He gleamed in excitement. "From a town guard to a Lord's bodyguard! It's an honor to meet someone who climbed their way to the top!"

"Ahaha, no, no, I'm not that amazing..." She waved her hands in front of her. "Anyway, you can just call me Rika."

"Ahem," Eris cleared her throat, "shall we get on to business, my Lady?" 

"Huh? Ah, Mhm. I guess it's pretty urgent, after all." Sylphia turned to Gray. "Hey, so, what are you anyway? A guard? Anyway, bring me to the village elder. I've got some important business to discuss with him."

"A- yes, my Lady! I am the Captain of the Guard," he saluted, "I'm sorry for taking up your time! Please follow me."

####

While they trailed behind him, Sylphia noticed a sharp gaze pointed at her from the the corner of her eye. She glanced at the source for but a moment, when the man, wearing a hooded cloak, turned around before disappearing into an alleyway.

/Hmmmm...? That doesn't seem to be one of my fans./

"What is it, Sylphie?" Rika leaned over and followed her gaze.

/It wasn't my imagination after all. I'm starting to attract some unwanted attention. Maybe spreading my name across the kingdom isn't such a good idea, after all.../

"It's nothing," Sylphia said, "let's go."

As they passed the last row of shops, a distant voice called out to Sylphia. With her nerves still on edge, Sylphia turned around with a hand on her blade, and another on Sebastian. 

A familiar man in baggy robes waved his hand as he came towards them. "Miss Sylphia! Oho, it is you, after all!"

"Oh," Sylphia lowered her guard, "you're... the guy from the Merchant Guild."

"Dante, Miss Sylphia..." he moaned.

"Ahh, yeah. Sorry, there's a lot on my mind right now..."

"Ohoho, finally getting stuck in with your Lordly duties, eh? That's good! That's good! I do not mind at all."

Eris tapped Sylphia's shoulder. "My Lady, you've had some business with Mr. Dante before?" She raised her brow at her.

"Huh? Oh, yeah, yeah. It was just before I became Lord of Dellwick, don't worry about it."

"I see..."

"Hey," Sylphia said, "I'm just going to have a quick chat with him, I'll catch up with you guys."

"Eh? Are you sure, Sylphie? I can wait with you, if you want."

"No, no, don't worry." She pushed Rika on with Eris. "It'll just be real quick."

"Okay..." Rika waved at her as they went ahead. "Bye!"

"Bye." Sylphia waved back with a smile.

As they disappeared into the next corner, Sylphia turned to Dante. "So, how are we doing?"

"Very, very well, Miss Sylphia. Oh- or is it Lady Sylphia now?"

"I don't care." She shook her head. "Now, I trust you've been fair with the exchange rate I negotiated with you?"

"In the name of my honor and of the guild, Miss Sylphia."

Sylphia smirked. "And the profits?"

Dante intertwined his fingers. "Exceeding our wildest expectations."

Sylphia covered her devilish grin while she giggled. "Good! Good!" She reached up on her tippy toes and patted him on the shoulder. "I knew I could count on you, Dante."

Dante bowed. "Your praise is wasted on me. I know whose genius it was that allowed a mere trader such as myself to partake in this fortune."

"Hihihi," Sylphia spread her fan to cover her mouth, "go ahead and keep praising me. I'm still not budging on the price."

"Agh!" Dante scratched his head. "You're killing me, Miss Sylphia!"

They shared a good laugh one last time.

"Well, just keep up the good work." She folded her fan with a flick of her wrist. "I will be making a visit soon, so expect another bump in quality - of course, with another bump in prices."

"Oho! If it were another trade, I'd go for quantity over quality, but in this case -" he rubbed his hands while licking his lips, "quality is what makes the buck!"

"Good, good! Well then," Sylphia waved him goodbye, "I have another meeting to attend to. Please keep up the good work."

"Of course, Miss Sylphia!" He saw her off with a bow as he disappeared behind the market crowd.

Sylphia smirked. /Well that's one problem solved./

She then ran in the direction she last saw the others and caught up after a short while.

####

Gray led them to a nearby house, not particularly different from the other houses that lined the main road of the village. It was a simple one-storey home with a thatched roof and a few windows. The only thing that somewhat distinguished it from the houses beside it was the flower pots hanging from its front windowsills. He walked on over to its door and gave it a good knock.

"Elder!" he raised his voice to the door, "The Lady, Sylphia, is here to see you! Eld-"

As he raised his arm for another big knock, the door swung open and hit him in the nose.

"Ughfu-" He recoiled backwards and covered his face.

"What is it, Gray?" Out came a girl who gave the guardsman a sharp glare. "I keep telling you, Grandpa's still asleep at this time."

"Ah, Ann!" Gray said, with a pinched voice, "Sorry, I know, but this time, it's really urgent!"

"You keep saying that..." the girl's eyes, and, consequently, her glare, wandered to the people behind him. As she met eyes with Eris, she scowled, "what's the secretary doing here? Tax day should still be a few weeks ahead."

Similarly, Eris returned her a glare, but did not say anything.

/Uwah, she's not well liked around here... I should defuse this situation./

"A-ahh, she's with me." Sylphia flailed her arms around. As soon as the grabbed the girl's attention, she cleared her throat and said, "I'm Sylphia, the new Lady of Dellwick."

####Revision notes:
- Make Ann's reaction different, maybe have her stare at how cute Sylphie is.

Ann's eyes widened. "...Huh?" She glanced at Gray for a moment, but when she was given a nod, she pushed the door wide open and lowered her head. "I-I'm sorry! I didn't realize you were the new Lady of our village!"

"Ah, no, no, it's fine...." Sylphia said, waving her hands in front of her. /Not this again. these people are way too jumpy./ 

"Please come in. I'll wake grandpa up."

The girl led them into their living room, a modestly decorated space, featuring a pair of wooden benches arranged opposite one another with a long coffee table in between. Light and air entered freely into the open windows on both sides of the house, and at the far back, a small brick fireplace stood in wait for the seasons following the harvest.

"Well, I should be returning to my duties now." Gray said, as he was about to leave the room.

However, Sylphia stopped him, saying, "Hold on. You said you were the Captain of the Guard, right?"

"Uh, yes. I am."

"It's better if you stay here. This concerns you as well."

"I-I see..." With that, Gray stood by near the door as they waited for the elder to arrive.

After a short while, the girl returned with an old man in tow. His back was hunched forward, its weight supported by his hand, grasping on a steady cane, which he swung in front of him for every couple of steps he took. 

/Woah, they weren't kidding when they said he was the village elder.../

"So, you are Lady Sylphia," the elder spoke with a strained voice, "I have heard.. many rumours about you as of late."

"Grandpa, let's take a seat first." Ann said.

"Huh?" He turned his head. "Yes, yes..."

She leaned down and supported her grandfather with her shoulder as she sat him down, slowly, onto the bench opposite Sylphia and the others.

"Good morning, Elder," Sylphia said. "I assure you, none of the bad rumors are true, and all of the good ones are."

"Bhah!" the elder gave a hearty laugh that developed into a fit of coughing. 

"Grandpa, don't strain yourself." Ann patted him on the back.

"Ahem. Sorry, it was just..." he hacked, "a long while, since I met someone with such a... unique first impression." 

Sylphia covered her mouth and chuckled. /This might take some time... tsk, I've got a lot of stuff I wanted to do today, too. I probably should have shoved this meeting last./ 

He looked directly at Sylphia. "Though, I'm sure Lady Sylphia must be busy, before we begin, I first wish to express my gratitude." He lowered his head slightly. "Thank you."

"Hm?" Sylphia cocked her head. "What for?"

#####Revision notes:
- Too much talk about the taxes, tbh; rather cringy.

"It isn't just grandfather," Ann said, "everyone in this village is finally able to breathe a sigh of relief, thanks to our Lady lowering our taxes..."

"Ahh..." She placed her elbow on the armrest and rested her head against her hand. "That. I should make it clear that I did not lower your taxes for your own sake." 

She glanced at each of them - Ann, the Elder and Gray.

"First of all," she continued, "I wanted to see how the citizens of Dellwick would use their wealth, should they finally come to own it." Her eyes narrowed onto Ann. "Should you squander it in any way," Then to the elder. "if you fail to use that wealth to improve your village," And finally, onto Gray. "if you disappoint me, then I will be claiming it back."

Ann shuddered. "T-then, what should we do, my Lady."

"Well, that's why we're gathered here today." Her lips stretched into a sharp smile as her eyelids narrowed even more.

"I... see..." Ann hung her head and clasped her hands on her chest.

"Mm." Sylphia shifted forward in her seat. "Let's begin, then. I have received reports - that banditry is on the rise around this village. Is this true?"

"I-" Gray hung his head. "I'm ashamed to say that this is true."

Sylphia frowned. "Give me the situation, Guard Captain."

"Yes, my Lady." Gray gave a salute and then proceeded to share all of his information on the bandits. 

His brief continued for a short while with everyone listening in attention.

"So," Sylphia said with a hint of frustration in her tone. "we're lacking in guards and equipment, and there's a high probability that a bandit camp has been established near the village."

"Yes, my Lady..." He hung his head.

/Geez, why did this place get so fucked all of a sudden? Or was it already like this when the King was managing it? Bandits... Speaking of which, those ruffians Eris was talking down to in Eddingset... so they were bandits, huh...? God, the plot thickens more and more./ 

"Gray..." Ann looked distraught. "this is the first time I've heard of this..."

"Don't let it get to you, Ann," Gray said, "the bandits won't dare show their face here as long us we're around."

"But for how long, I wonder?" Sylphia interjected. "For now, the bandits are keeping themselves to shaking the merchant caravans along the road, but what next? Soon, the bad reputation spread and the merchants will learn to avoid the village, at least without significant guards. Afterwards, what do you think will happen? The bandits will just call it quits and go home?"

"I..." He balled his hands into fists. "I'm sorry, my Lady." 

Sylphia sighed. "I'm not reprimanding you. Not yet, at least. But why was I not given all of these details until I came personally? You should have known that these bandits are becoming a significant threat."

"It was a failure on my part. I hadn't thought that far and underestimated the threat."

"What's done is done," she slipped her fan out and pointed it at him, "just know that I expect better from you next time. If you wish to retain your position, that is."

He stiffened up. "I understand, my Lady."

"Very well." Sylphia then tilted her head to the side. "Eris, work with him. Whatever he needs - manpower, equipment, procure it. And you, Rika, you have some experience guarding a much bigger town, and you were trained by a Royal Knight, so they should learn a useful thing or two from you."

"You got it!" Rika said.

"Royal Knight...!?" Gray looked up in awe of Rika. "It will be an honor to learn from you, Miss Carlisle!"

"Ehehe," she scratched her head, "I told you, just call me Rika."

"My Lady," Eris adjusted her glasses. "may I ask..."

"Hm?" Sylphia shifted in her seat. "What is it?"

"How am I going to finance these procurements?" She gazed sharply at Sylphia out the corner of her eye. "You have not received any tax money yet, and, as far as I know, don't have any reserves from which to pull it from."

"Geh-" Sylphia winced.

"On top of that, of the tax money that you will collect, a portion will have to be set aside as your obligation to the King. Of what will remain, another portion will be used to pay for upkeep. After that, I cannot imagine there will be enough left to hire and equip more than a handful of men."

Sylphia struggled to keep herself composed despite the setback. 

"A-anyway, just help him out with whatever you can for now. Rearrange the shifts if you have to, just so we can maximize what we already have."

Eris sighed. "Yes, my Lady."

/Uwah... between a bandit infestation creeping in, a guard unable to protect the village, and a lord unable to pay for more guards, this village is in deep shit... I need to figure a way out of this mess./

In that moment, Sylphia's thoughts went to the Mythril Cave.

/Uuuugh... I really don't want to those resources for Dellwick. God only knows what will happen if I start making siths patrol the streets. If kobolds and siths never got along, I couldn't bear to imagine what will happen if humans were thrown into the mix... though, if it's just money.../

"Ahem," she cleared her throat, "Anyway, onto the next topic," Sylphia said, "as to why I lowered the taxes in the first place."

Ann and the village elder looked intently at Sylphia.

"To be honest with you, I don't know what life in a small farming village is like. And yet, I am made to rule over exactly that. So the problem is," she leaned forward and looked directly to the two of them, "I don't know the first thing that will improve the village and the lives of my citizens." 

The two were taken aback with her straight faced admission.

"So," she leaned back once again, propping her head up on her hand, "I thought it might be a good idea to let you yourselves sort it out. I will let you keep most of the wealth generated from your farms and businesses. In return, apart from a small tax, I want you to improve the village in a few ways-" 

Sylphia counted off her demands with her fingers, "One, increase food production. Two, improve local businesses. Three, grow the village as you see fit. On my part, I will do whatever I can to help you along those lines and I will guarantee your protection, as long as you pay your dues."

For a while after she finished talking, both of them looked blankly at her, as if they were still waiting for her to say something. Then, Ann, opened her mouth and, as though she was unsure of herself, asked, "I-is that it?"

"Hm?" Sylphia rolled her eyes left and right for a second, as she tried to recall if she left something out. "Yeah."

/Was I asking for too much? Is it even a good idea to ask them to do it themselves in this day and age?/ Becoming unsure, herself, Sylphia asked, "Is something wrong?"

"Eh? N-no... um..." Ann averted her gaze.

"Are you sure, my Lady?" The elder spoke, with a voice doubly strained by age and by anxiety, "That you want to entrust this much to us?"

"Well, as I said, I can revoke your new privilege whenever I want," Sylphia then smiled, "but as much as possible, I want you to grow Dellwick into the village you want it to be." Her smile widened to a warm grin. "This is your home, after all."

Holding her hands to her chest, Ann looked downwards as her lips slowly unfurled into a crooked smile.

"My Lady..." the elder said, "your words warm this old man's heart. I promise to do what I can... so that our... humble, little village... can live up to your expectations."

"Mm. That's right." Sylphia gleamed brightly at them.

"Lady Sylphia." Gray said. "I did not see how much importance you placed in our village. Please forgive me! In return, I promise that I will double - no, triple my efforts!" He glanced at Ann. "For our home..."

Sylphia giggled. "Good answer. I'll be counting on you, Guard Captain."

"Thank you, my Lady!" he stood in salute.

Thus, the meeting finally ended. 

A short while later, Sylphia, Rika, Eris and Gray bid their farewells to Ann and the Elder. 

"Lady Sylphia, um..." Ann took her hand and gave it a light squeeze?

"Hm?"

Ann hesitated. "...thank you again." With that, she lowered her head and let go with Sylphia's hand.

"Don't thank me." Sylphia patted her on the head. "Save it for yourselves, when you've made something with your own hands."

Ann held back her sobbing, as her grandfather patted her on the back.

Meanwhile, the Elder simply gave a nod to Sylphia, and pulled Ann back into the house.

"Well then," Sylphia turned around and faced Eris and Rika, "I'll leave you two with Gray, then."

"Huh?" Rika jolted. "What do you mean, Sylphie?"

"There are still a few errands I need to run, but you two don't need to come along for those, so you should start helping out with the security problem as soon as possible."

"Eeeh?" Rika groaned. "But I'm supposed to protect you..."

"Geez, we're inside the village, Rika." She reached up and patted her on the shoulder. "There isn't any danger here."

"But..."

Sylphia then leaned in and whispered, "My only next stop is the Mythril Cave, so it's completely safe."

"Mmmmmm...!" Rika furled her brows and puffed her cheeks out at Sylphia. 

"Okay?" She winked.

Riks sighed. "Okay... Please be careful."

Sylphia grinned. "That's a good girl." She then rubbed Rika on the head.

"Geez, it's not fair..." Rika grumbled, "you always get your way, Sylphie..."

"Sorry, Rika. Next time, I promise I'll listen to your request."

"Really?"

"I promise."

Finally, Rika smiled. "Okay."

"Take care of Eris for me."

"I will!"