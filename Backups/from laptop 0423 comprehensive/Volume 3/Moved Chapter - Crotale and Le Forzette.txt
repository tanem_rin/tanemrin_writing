
Senators la Forzette and Senator Crotale are walking side by side to the senate hall.

La Forzette is a widow, of the last war with Easter, while Crotale is the young family head of the Crotale clan, whose last head, her father also died from the same war. At the moment, Lady Crotale is filling in for her baby brother who is still too young to take the helm. Plus, she has proven herself as a very capable young lady since graduating from (Capital, maybe saint's name) Academy.

*Lady Crotale (rattlesnake) is merely 22 years old and the family is currently searching for a fiance' for her. She is a serious girl who does no jokes. A total psychopath, like Eris.
*Lady la Forzette is nearing her 30's (29), and she had just had her daughter when her husband died, leaving no son to inherit his name. She is a laid-back mom type but with a psychotic streak.

*This is all well and good, but what is their end-game? They will sell Rafale to Sylphia.
---> After the crushing and unexpected defeat of the spear maiden (who becomes Sylphia's companion somwhow), Sylphia struck deep into Rafale, catching their armies, as well as Londinium's nobles off-balance.
---> Sylphia needs to have developed a powerful logistics corps at this time.
---> Sylphia sends out independent companies of siths, kobolds with mounted earth lizards to capture Rafale's inner territories in the world's first implementation of blitzkrieg.
---> She also leverages the new technologies she created with Klica, as well as Klica herself, with her powerful mecha.
---> She has the support of the world's only truck fleet, animated by Klica's magic.
---> The sudden surge of more modern weapons, plus tactics designed to defeat the age's armies, broke Rafale.
---> Her very own mage corps (made by the Academy's flunkers) would keep her main unit safe, as mages only get deployed in large formations to be effective. (Sort of like a shoving match, with barriers vs magics). Sylphia acts as their magic battery, plus they get Alphonso's magic ring design.

---> Sylphia also needs to establish Autarchy within Dellwick and the surrounding villages (which she will soon take under her wing). The village (then town) produces the food, the Mythril Cave produces the arms.

NO TURNING BACK CROSSROADS:
Rafale will surrender not to Londinium, but to Sylphia. Will she then honor her pledge to the King and deliver him Rafale? Will she grant them independence? Or will she accept Crotale's plan to install an Autocracy in Rafale (with her as the viceroy in the country, of course).

What does Rika say about this? - she's a kind girl, so she won't easily forgive betrayal, if Sylphia doesn't have a good reason. She thinks badly of the nobility who betrayed Sylphia, though. It's not like she has any loyalty towards the King himself, although she respects him well enough as the kind old guy who let him stay with Sylphia and made her a knight (with the salary increase to come with it).

What does Extella want? - she pretends all kind and gentle, but all she really wants is to experience battle. She severely looks down on traitors, however, especially those who condone them. Sylphia will have been betrayed by her reinforcements, and so she will have a negative view of Londinium as a whole.

What will Letty say? - She just doesn't want Sylphia to get into trouble. She'd want to let the King handle it, so Sylphia can just go home (<- naive)

What about Klica, Luna? - Klica doesn't care about the affairs of humans. Although she preferred any course of action that would lead to more bloodshed between them (Schadenfreude - she is actually a softie, who develops a liking towards Rika and Luna, the annoying girls in Sylphia's entourage). She keeps her guard when Eris is around, but doesn't like nor dislike her.

- Luna wouldn't like Sylphia to fight with her father, and the mage corps are all Londinian rich kids, i.e. nobles. So they wouldn't want to fight Londinium.


*They just talk about the heroine of Londinium and wonder how theirs is doing.

*Just a note: Though Rafale is a republic, the sense of mandatory equality within the higher ranks has made some of the male senators a bit more stringent when it came to the ladies, especially when they believe it is undeserved, such as La Forzette's case.
The commoners are pretty egalitarian, though. (Proles)
They have their philosophers maintaining the republic.






The two share a conversation on their way to the senate building, which involves talking about the heroine of Londinium.

"The hero of Londinium is making waves as of late." Her cold, uninterested eyes (Crotale)

""



/Rafale has been a republic since the days of my ancestors, but I feel like this egalitarian blight had only taken root much recently./





"The hero of Londinium is making waves as of late," a woman murmured to the young lady beside her. 

"I've heard," she replied with a seemingly disinterested voice.

The two walked a leisurely pace up the (sakanobori/hilltop path). The woman rested a parasol upon her shoulder, whilst leading, gracefully, her lone attendant. Meanwhile, the young lady posessed an entourage of three; one maid walked beside her, holding her parasol for her, while the others walked followed slightly behind.

The young lady continued, "It seems they had their hero join the kingdom's nobility."

"Ah, indeed. How absurd!" 

Her expression darkened. 

/Though I understand the Magistrates extended a similar offer to our heroine, and was promptly refused./ 

/The fools. What did they expect the Spear Maiden to do, had she accepted? Does she know about passing judgement or amending laws?/

/This morbid fascination with the other-worlders is unhealthy./

The woman sighed. "But, ahh, let us hold off on this topic. Lest we grow tired of it even before it is raised in the senate." Quickly switching gears, the woman then took on a bubbly tone, and asked, "So, tell me, Noemie, have you settled on a fiance now?"

The young lady scowled at her. "/Senator/ Crotale you mean. Please remember that we are on official business today, /Senator/ Le Forzette."

"Aww, don't be so cold, we're still a long way from the Senate Hall, are we not?"

"That is no excuse," she then turned away and muttered, "and no, I have not."

"Oh...? You didn't like the young master of the Michelard house?"

"...no."

"No?"

"He is uninteresting."

The woman sighed. "That's the fifth suitor you've turned down... are you truly aiming to (scorch/weed) through all the noble houses before settling on someone?"

Her eyes narrowed.

"I can't help it if all the men in this country are uninteresting."

"Uninteresting, uninteresting!" The woman pouted. "That's always your excuse! What does that even mean? What does a man have to be in order to be interesting to you?"

The sudden outburst took the young lady by surprise, causing her to flinch. 

"Why are you getting so upset about this?" she asked, "It doesn't matter to you if I never find a spouse."

"It does!" The anxious woman placed her palm over her cheek. "I worry about you... at this rate, you might never feel the joy of having a child..." Her lips then widened, forming a lovestruck smile, "Eheheh, did you know my dear Juliana is learning to count now? She is absolutely adorable!"

/This doting mother.../

"You speak as if I am entering my forties..."

"But that's just it. You're at the ripe age of 22, and yet you look like a delicate girl who has never felt the touch of a man. What sort of "




The same could be said for the hero of Ardeal. Having heard nothing from them, I could only assume this is also true for Easter, Hellas and Jizzaro./

/I had always thought that the Legendary Heroes were too free-spirited to be brought into such chains... but perhaps there are those who differ?/

"At a time when every other nation has been keeping their heroes a tightly kept secret, they are doing the exact opposite." The woman shook her head. "How curt."

/The power of the legendary heroes... it could be said that one has the power to challenge the Demon King - the very same one that could, without effort, lay waste upon all of Terra Nova. By extension, this means that a hero should also possess the same power, should they be so inclined... That makes them an asset of strategic importance. Thus, due prudence is to keep them a secret for as long as possible./















/Then why?/

/Are the they fools?/

/No. I've met the King of Londinium. And that man is in no way a fool./

/But if that is the case, then what/

"They underestimate us..."

/Or they/


Who knows... a misguided sense of awe? Or is it merely the logical extension of their egalitarianism? The Spear Maiden is from a world far, far different from our own. It would not surprise me if some of them believed her to be a goddess, brought down from heaven/


#####