BEFORE THIS - REVEAL THAT ERIS WAS RECEIVING PAYMENT FROM COUNT ROSTEFOROUGH.

Sylphia found out from the bandits that Eris was the one who organized them and had them attack Dellwick at presicely the right time when the guards were rotating.

Sylphia finds that Eris is trying to increase in power to protect her little sister. She invited that sister behind Eris' back and played with her until Eris came back. 

She then threatens Eris


< INSERT THIS WHEN SYLPHIA IS ABOUT TO FIGHT BACK AGAINST WHOEVER IS FUCKING WITH HER >

2) They travel to dellwick after a couple of days to make the announcement of the changes she's making. 

Reveal the mythril crafts scheme Sylphia set up in the Mythril cave. Rings, necklaces, whatevers, made of Mythril, plus weapons and armor made of inferior mythril plate sold at exuberant prices. Keep it a secret from Eris. As far as she knows, the monster tribes are uncivilized barbarians. (eventually reveal when Sylphia is able to trust Eris more)

She's now making boatloads of money, since the kobolds and sith store most of the proceeds as tribute to the gods (Sylphia mostly, since Letty doesn't care about money)

Sylphia runs into Letty in the Mythril cave, rolling around with some Kobolds, reveal that she has come to like stroking their fur. Moreover, she is often approached by the kobold and sith smiths for advice on how to improve their production efforts. Sylphia suggests they start scaling up, since they are able to arm every kobold and sith now, and start building machinery - specifically, siege weapons.

Reveal that the monsters have finished buiding the high density residences that Sylphia ordered, and living space is no longer a problem even with smaller villages. Next, Sylphia orders they build a road and enlarge the opening to the mythril cave, as well as build a gatehouse and fortifications in the entry, just in case.

Reveal that there is a cult of kobolds strangely attracted to Sylphia, specifically the survivors of the big battle they fought. They insisted they wanted to serve Sylphia exclusively as the cult of the Goddess of War - from them she thus formed her Kobold Commando group, who follows her stealthily even outside of the cave.




2) The count is there to insist that Sylphia continue supplying his men with food, as the new lord of Dellwick. 

Why?

Because I, count Rosteforough marshall the largest of all the armies in the entire kingdom. You could even say that I am the pillar upon which this great kingdom relies.

To begin with, why would you marshall so many men when our country is neither at war, nor are you able to support it yourself?

"You ask so many questions, and you reveal yourself to be a naive little girl." "It is no secret that the Kingdom of Rafale has been harboring... ambitions... espcially along the frontier of our kingdom. Despite being a tiny, backwater of a nation, they boast a standing army comparable to the Easter Empire."

That was not what I was asking.

The count scowled at her. 

"What I was asking," her eyes narrowed into thin slits as she focused her gaze into his eyes, "Honorable Count Rosteforough, is why do you insist on marhsalling those men yourself, when you are unable to pat for their upkeep?" She placed her elbows on the table and intertwined her fingers, resting her chin upon them. "If I may, good sir, I would like to offer my proposal to you - one that should completely solve your food problems."

His nose, lips and brows (crumpled/tightened/twisted) together as he said, "Is that so? Tell me."

"It just so happens, that while the honorable count has the honor of fielding the largest army in the kingdom, I hold the dubious honor of having the smallest. Namely, none." She stood up and waved her hands between them as she spoke, "That is to say, I have no mouths to feed. So my proposal is actually rather simple - if you transfer under my command the soldiers you could not support, then that solves your problem!" She snickered, "And mine, coincidentally."

A nerve bulged on the Count's head. "Grrrrr.... ah!" He snarled as he bashed the table with his fist. "You wish to take advantage of my predicament to bolster your own force!? And to have the gall to spout such nonsense to my face!"

Sylphia shrugged. "I would never think of it." With a mocking tone, she then said, "Why, isn't it a given to answer nonsense with nonsense?" She looked at him with a condescending grin.

"You.." he inhaled audibly through his nose, "how dare you speak with that tone to me! You are nothing but a jumped up little girl with no education, no name and no dignity!"

"Now, now, dear Count." Let's just calm down and talk seriously for a moment. She leaned in an sharpened her gaze. "Nothing is free in this world." With a snap of her finger, Eris placed a piece of paper on the table, which Sylphia slid over to the Count.

"What is this!?" He nabbed the paper and read the names listed within. "What is the meaning of this?"

"But just because something has a cost, doesn't mean it's automatically money."

"W-wha... Count Christoff... Count Embley...!? You shall explain yourself!" 

"Why, I simply wish to know more about them. I think I've developed an admiration for them you see..." Her lips curled up into a mischievous grin. "Anything you can tell me. Everything. The more salacious, the better." She paced around the room. "I happen to know that the good Count is placed centrally within the kingdom, especially because of your military might. A man such as yourself must know some... things."

"You fool, and why should I tell you anything, rather than have you hang for treason!?"

"Ah- I wouldn't do that, if I were you." With another snap of her finger, Eris placed a stack of documents on the table. Sylphia browsed through them, narrating their contents, "Let's see... embezzlement... promoting banditry... kidnapping, which one time led to murder, ooh, that sounds pretty bad." she glanced at the Count as blood slowly drained away from his face. "Extortion? Theft - and from the King's own collection! Aha! Someone's been a naughty boy..." 

"W-w-what are you saying!?" He stammered, "I-I have no idea what you're talking about!"

Sylphia giggled. "Of course you don't." She approached him, stood on her toes and whispered into his ear, "And if you do as I say, neither will I." As those words leaped from her tongue, a stream of black haze flowed into the count's ear.

The count swallowed his breath.

As she stepped away, Sylphia twirled around with a giggle. "So?" she said with a light whimsy, "What will it be, my dear Count?" She tilted her head as she radiated a bright smile.

"I was wrong about you..." He fixed his coat. "You are no mere little girl." He gazed into Sylphia's eyes with dark eyes, "You are a demon, wearing the skin of a little girl!"

Sylphia giggled. "Okay? So, Count Rosteforough..." Her eyes narrowed as her lips widened into a sharp grin. "Will you make a deal with this little devil?"

He rolled his fingers into a fist and said, with a pained voice, "I will."

