Chapter 41 - Interlopers

The great forest of Ealdenshire marked the northeastern border of Londinium. Its ancient trees had seen countless generations come and go, kingdoms built and ruined. The forest also marked the world's longest continuous border, spanning north to southeast. Three countries, The Republic of Rafale, the Easter Empire and the Grand Duchy of Ardeal, claimed adjacency to Londinium through it. 

Nimble as a cat, a masked woman lifted herself over a sturdy tree branch and sat down. She crossed her legs, swaying them back and forth as she looked out beyond the forest, where a fertile plain formed the only border between the Kingdom and the Empire.

For a long time, this territory had been contested by Rafale and Easter.

Her face lit up with delight.

/Whew, Looks like they're getting ready for something fun./

Even at that distance, distant specks came to the fore when clumped together into orderly formations. Square blocks marched around the camp, warping its shape for just a moment as it turned, before regaining its original shape. It somehow reminded her of jelly.

For a moment, she took off her mask to rub the sweat off her forehead. She muttered, "I just came back and there's already something to do. Busy, busy!"

As she settled down in her seat, another woman's voice came from below, "The Raffalian camp ought to be within view from here." 

A series of tremors followed. Travelling up the trunk, they grew stronger and stronger until, off the corner of her eye, she saw a dark red cloak whipping up. 

When the shaking settled down, the masked woman replied, "Yeah, I see it." She then snuck a glance past the tree trunk between them, and added, "Oh, by the way, Kiya..."

The woman joined her from the opposite branch. Instead of sitting, though, she opted instead to crouch on the branch, allowing the crossbow slung on her shoulder to dangle on her hip.

This one, too, wore a mask, but one that was broken in half. And when she turned to look, she cast a steely gaze from a single, expressionless eye.

"Yes, Lady Aster?"

"...were you able to deliver their secondary mission before they left?"

"Yes. While observing the village for any sign of loose monsters, Lil and Garm should be able to track down the Mythril dealer along the way."

"They know what our little heroine looks like, right? I don't want them putting a scratch on our VIP."

"It's unlikely for them to ever cross paths, seeing as the heroine is based in the Royal Palace. But they do know what she looks like, and they know to protect her, if need be."

"I see, I see. Heheh!" She grinned. "I'm glad you're so reliable."

"Mm." Kiya simply nodded. "Leaving that aside, our own mission..."

/Ahh... she's not really the type who responds to compliments. Hm? Actually.../

Her thoughts turned to the rest of her team.

/Come to think of it, none of them are.../

Having lost herself in thought, she missed Kiya's briefing and would have to ask her to repeat herself. 

The latter did not appreciate this.

####

A young girl pressed her back against the bark of an old Elm tree. Strands of bleached silver swept across her eyes as a soft breeze blew past, rustling the dense foliage around her. The late afternoon was cold in Northmarch, the province that encompassed several small settlements including Dellwick and Eddingset, but thanks to her cloak, it felt rather comfortable.

Hushed voices, all of adult men, mingled from behind the tree and joined each other in conversation.

"Man... I have a bad feeling about this," a man said as he let out a sigh. 

"Tsk, what, you're getting cold feet /now/?" 

"No, I just..."

"We get paid /and/ get a cut of the loot! Who could pass this up!?"

Yet another man joined the conversation. "I hear ya'. I'm thinkin' o' joinin' this outfit after this."

"I just... I don't like the rumors washing around Dellwick lately... they say this forest is... haunted. "

"Haunted? You still suck your mum's tits or something? There's no such thing."

"Sod off. It's just what I've head from the guys in the tavern. They say demons go around in the night, asking people, 'are you a bandit'? And if you say that you were, they'd chop your head off!"

"What kind of stupid story is that? Don't tell me you actually believe that."

"I... well."

"Dumbass. It's obviously a kid's fairytale. It's so absurd! Why would you even admit to being a bandit in the first place?"

"Err..."

"Jackass, that's just rumor-mongering to keep us away from this outfit. They just want to keep more for themselves, you got it?"

"Well, that makes sense... I hope you're right."

"I am right. Now shut up, the forward's here."

The soft rustle of leaves followed a man's shallow panting.

"I saw 'em," said a hushed voice from behind the tree, "the three wagons. Four guards, maybe more."

"Right," said a rough voice, particular in that it was absent in the previous conversation.

Her gaze turned to one side.

Beyond the treeline, a rhythmic canter, interspersed between grinding wheels, drew closer and closer.

Killing her voice, the girl muttered, "...big brother."

The sound of parting leaves grew louder, denser. This time, it was accompanied by the soft, leathery scratch of swords being drawn.

"Now boys," the rough voice added, "let's get closer."

Hearing this, she leapt up. 

Her padded clothes made little noise as she ascended straight up the tree trunk. With the precision and agility of a jungle cat, she climbed onto a branch and observed the situation below.

A group of seven armed men skulked beneath the foliage as they advanced closer and closer to the open road beyond. Each man carried a sword and a small shield. This, on top of their hide and leather armor, made them look closer to soldiers than bandits.

But bandits, they were. 

And as soon as the wagon train came around the bend, the ground quaked with a loud crash. The shock propagated from the ground and up the tree such that even the girl felt it in her feet.

Suddenly, the once serene forest erupted in violence.

"Woohoo!!" the bandits cried. 

"They fell fer it! Get 'em!"

They screamed and howled, working themselves into a frenzy. Leaving any pretense of stealth, the outlaws stamped and slashed at the greenery that once hid them as they carved a straight path to the caravan train of 3 wagons.

Frightened, the nesting birds fled the forest canopy for the sky. Though this proved troublesome for the girl who found herself in their way, the uproar above and below also masked her every movement.

Without hesitation, she leapt from tree to tree, grabbing and swinging on the dense network of branches. Leveraging her lithe limbs and petite form, she soon overtook the bandits and prepared her own ambush.

She drew a pair of daggers from both sides of her waist. One with a thin, razor-sharp edge, and the other, a thick, straight edge with intermittent notches. 

And thus, she waited.

The wild men streamed forth like a pack of wild dogs, barking and howling as they pursued their prey.

But soon, they entered her killzone.

Like a domino tipping on its side, she dove off the tree branch. 

She set her sights on the one farthest back. 

Quiet as a falling leaf, she slid her blades down the back of his neck. 

The crackle of shattering bone resonated within her fingers.

And with no more than a lingering grunt, the man fell dead on his knees, leaving his companions unaware of the pressing danger.

After a quick glance left and right, she set her eyes on her next victim -  a thin man whose armor did not cover his lower back.

She dashed in.

One after another, three more fell to her blade, none the wiser to her presence until all was too late.

But as their numbered sheared off, so did the number of footsteps.

Consequently, the rustle of the grass beneath her own feet finally gave away her presence.
#### POLISH HERE ####
However, by the time her fifth target came to realize this, she was already within arm's reach. And so, without hesitation, she plunged the thick blade into his back, piercing his liver.

"Guaaaack!" The man let out a blood-curdling scream.

His end was not as painless as the first. The serrated edge of the knife sawed through tissue as the girl pulled it back out of his body, sending him crumpling face-first into the ground.

And as the man drew his last gasp at her feet, the girl laid her eyes on the next kill. Her face showed no emotion. They never did. From the moment she dropped from the tree, to when she tore her blade back out from the man's shredded body, her eyes were completely empty. 

All that filled mind was how next to kill. Efficiently.

"Who the hell!?" yelled the man closet to her, "Hey, we've got company!"

His tied beard swung around as he faced the girl. Then, his expression took the journey from furious, to confused, and, finally, amused all in the span of a second.

"The hell?" he sneered, "It's just a little girl. I'll take care o' this." 

Alerted by her presence, the three remaining bandits stopped their attack on the caravan and instead drew closer.

"Ya made your last mistake, missy." 

However, his words would not shake her.

"Rrrrraaaaaaaagh!"

Thus, the brutish man raised his sword and charged in shield-first. 

She realized that she was at a disadvantage against the man with his shield, however...

Her eyes narrowed.

Instead of retreating, she blasted forward, meeting the man head-on.

"Gaaahahaha! You idio-"

As he swung his sword down, the girl would catch his blade within the notch on hers.

"Ngh-!"

Controlling his blade, the girl stepped aside and parried his strike. 

Once he had passed her, she then put her weight over his existing momentum, pulling his sword, and his entire body downwards. Unable to withstand his own momentum, his knees buckled and the man came crashing down.

"Guuuuaaaah!"

Then, with a lightning-fast motion, she then sliced back and forth at his flank. Her curved dagger bit deep with little resistance, and produced fine cuts each time. 

They passed each other in but a single moment.

And in that short time, that hulk of a man fell to the ground, clutching his hemorraging wounds.

But the girl was not finished.

Before the next man's face even contorted in terror, she had already launched her attack. She pulled a few throwing knives from a strap on her thigh. By the threes, she launched them, imitating the motion she observed her boss doing.

Just as he was about to scream in terror, the man instead let out a baffled, "Huh?"

All but one of missed its mark, and the one the did hit landed on the blunt side, leaving nothing but a slightly sore patch of skin.

"W-what?"

Displeased, the girl grunted.

Taking advantage of the man's confusion, she she lunged at him at top speed. 

"Eeeek!" The man raised his shield.

But in his panic, his shield arm flinched so high that he covered even his eyes. Abusing his mistake, she swept low. Unleashing a flurry of cuts across his legs, he forced the men to his knees.

"Gaaaaaah! You- fucking-!" With bloodshot eyes, the man unleashed an attack in desperation.

He swung his shield across, bashing it into the girl's arm.

She clenched her teeth.

"Oooaaaaah!" With a piercing scream, he lifted his sword over his shield. He then pointed it down for a thrust, but before he could bring it back down, his throat was already severed.

With her light blade, the girl simply beat him to the punch. Slipping her arm past his small, rawhide shield, she plunged her blade into his neck, now placed conveniently within reach, and sliced it open on her way out.

Soon, his dead weight fell on her tiny arm, staggering her.

"Kuh-!" 

Alarmed at her vulnerable state, she swept left and right in search for the last bandit, so that she may figure out how to defend herself from his attack. 

However, the attack never came.

"Gyaaaaaaahaaaaa!" his voice cracked as he let out a squeal. "Somebody, help meeee!"

Instead, the last man instead dropped his clunky weapons and fled into the forest.

Her eyes flickered.

Finally, she pushed the corpse off of her and flicked the blood slick off of her weapons. And as the adrenaline rush calmed down, her hands thembled. 

She muttered, "That was... lucky..."

The thought of pursuing him, crossed her mind, but at the end of the day, she would only be doing unpaid work. And as a professional assassin, she was taught never to do any work for free.

Thus, she turned her eyes back towards the caravan, where a battle was already underway.

She muttered, "Big brother... good luck..."

####

"Aaagh!" A piercing scream echoed against the treeline surrounding the road.

A screeching voice followed as the caravan guard fell, "Hihihihiiii! One down!" An outlaw with a nose ring then drove his sword into the woman's breastplate, ending her life.

"Bah! What a shame!" another of the bandits sneered, "Can't make use of 'er now!"

"Eeeeeek!" A merchant, holed up in his wagon, cried, "We were already outnumbered to begin with, and now...!"

/Hm. The quality of these caravan guards are low. It's hard to imagine how a mythril dealer could secure such valuable goods with suck lack of talent./

Among the guards stood a large, cloaked man. His already dark skin appeared almost like a shadow beneath his sand-yellow mantle. Furthermore the massive, curved blade he wielded was something rarely seen around Londinium.

"You! The merc from Jizzaro!" a caravan guard called to him, "Protect the guys repairing that wagon, no matter what!"

His dark, gloomy eyes shifted towards the man in a chain mail. Then, placing himself between the armed thugs and their prize, he said, "Understood."

"We don't need to beat them... if we can just get the wagons out-!"

"Kee!" A bandit lunged at the man, interrupting him. "You talk too much, boy!"

"Guh-!"

While his fellow caravan guards desperately fought, the dark man simply looked on. Before him stood two bandits, one of whom had just killed a lady guard and his eyes were wrought with bloodlust. 

And yet, they refused to attack. 

They faced, after all, a titan of a man. He stood far taller than any of them, and from whatever silhouette they could see from his mantle, his entire body was packed with bulging muscles thicker than even their armor.

"Bro..." a bandit said to the other, "this guy's bad news..."

**His monologues STINK of exposition. Do it better.

/I joined as a caravan guard so I could inspect the cargo when no one was looking... but so far, I've found nothing; and all I've been doing is fight bandits day in and day out./

He clenched his fist.

/I've lost count of how many caravans I've infltrated. And yet, there is still no sign of any strange material that could be mythril./

A low groan escaped his nose.

/We need to produce results, or the boss will be displeased.../

The bandits flinched when he lifted his blade.

/..But for now, I must fulfill the job my cover entails./

He focused his hollow gaze on the one closest to him. 

Almost immediately, the man's eyes turned white in terror. Perhaps knowing what was coming, he raised his shield and readied his weapon.

He took a deep breath.

"Ha!" He exploded forward. 

Even with his opponent on-guard, he simply blasted forward with the massive blade rested on his shoulder. His intention was simple and straightforward.

/Crush through./

/Blocking this blow with such a flimsy shield will just break your arm. Your mistake was not running away when you had the chance.../


As he raised his sword, however, he noticed shadow on the ground, drawing close.

He thus planted his feet on the ground early, drawing on his excessive strength to pull his sword back and swing it around the side.

As a result, the curved, steel blade just barely scratched the man's shield before continuing its cleaving motion sideways. 

"Bwah-!" A second bandit screamed.

It tore through the man's leather armor as though it were jelly, sending blood spatter flying in a wide arc between them.

"Shit!" Seizing his chance, his first target then thrust his sword at his chest.

But with nothing more than light clash, attack whiffed.

With the swiping motion of his arm, he deflected the attack with his metal bracer, a piece of armor hidden beneath his cloak, revealed only as the sword ripped the cloth over it.

His opponent was thus unbalanced. However, after delivering that massive strike, his heavy sword was far behind him, and no amount of strength would be enough to recover in time for a second strike.

Thus, he released his grip from the blade, allowing its great momentum to yank itself out of his hand. Next, he drove his already raised elbow into the bandit's face and pushed himself forward with all the remaining strength in his legs.

"Hrngh!" His brows knotted. And as he released a voice from deep within his diaphragm, he smashed his elbow squarely into the man's nose. 

The bandit's head jerked back from the impact, but it was not quite over. Pulling his sword-hand forward, he clutched the man's face. His iron grip dug into the skin, securing the head firmly in his hand.

"Holy gods...!" One of the bandits shrieked in terror.

And finally, with all his body weight, he pushed forward and slammed the bandit into the ground. 

The shockwave kicked up a sheet of dirt that took a good long moment to settle.

The dark-skinned man exhumed a deep, gurgling breath. 

Within that very same moment, the scene settled into quiet.

Fighting stopped around the caravan train and all eyes turned to him.

And as the dust finally settled, two men laid dead on his feet.

Even as sweat collected over his brows, he looked left and right for the next target.

However, after his crushing demonstration, the remaining bandits simply tucked tail and ran.

"Gyaaaah!" one screamed as he disappeared into the distance, "What is with this guy!?"

"This is fuckin' ridiculous!" yelled another, "I'm out!"

He let out a sigh relief.

/It's over... just a few more miles and we're back in Dellwick./

He wiped the sweat on his forehead. 

**His monologues STINK of exposition. Do it better.

/This caravan job is getting us nowhere. We haven't found any traces of the mythril dealer. And following the trade route like this is keeping us away from the village, so we can't observe it for monster activity.../ 

/I could send Lil by herself to investigate, but with the intensity of these bandit attacks, it'a too dangerous for me to operate alone.../

"Oh... dear gods," the merchant finally disembarked from his perch on the wagon, "we made actually it."

The dark man spared only a passing glance for his employer, before turning away to retrieve his sword.

On his way, however, his eyes caught a glimpse of a young girl with silver hair. Her dark cloak broke her silhouette, making her hard to distinguish from the surrounding foliage. 

Without even knowing if their eyes met or not, he nonetheless gave her a passing nod.

And to his slight amusement, the girl gave a nod of her own, before disappearing back into the forest.

"Hmph," he shared with himself a low snicker.

/There were six bandits who attacked our carriage from this side./

/With a road like this, cutting in the middle of a forest, it would be unusual not to take advantage of the terrain and strike only from one direction./

His gaze sharpened.

/Six men, all by herself... if she were weren't around, this caravan would have been overwhelmed./

/As expected of our little prodigy... perhaps in twenty years, she will surpass even the boss./

As he thought this, the merchant approached him. 

"That was splendid," said the old man, "how you defeated two men on your own!" 

But the man's words rang hollow in his mind. 

Because while they looked at him in awe for such a pale feat, just a few yards away, a tiny girl, a fraction of his age, displayed true genius, only to then disappear into the shadows.

Holding this to mind, he replied, "I was just doing my job." 

But the merchant was further elated. "No, no! Really, I must thank you! In fact, please, allow me to give you a bonus!" 

He then pulled a small object from his pocket. 

"No, I don't need-" But just as he was about to refuse the reward, a streak of light caught his attention.

It flashed from across a glass bead inset within the intricately worked metallic plate. This familiar trinket laid on the man's palm.

"This brooch..." said the merchant, "you were looking at it the other day, weren't you? With that girl..."

"Hm?" He reflexively put up his guard. "So you noticed... And more than that, you remember?"

"Hee hee, despite how I look, I've got an eye for faces... Is what I'd like to say, but even an old man like myself won't easily miss a unique visitor like yourself. You don't see a Jizzarian merc with a Londinian girl very often."

His eyes narrowed.

The merchant then placed his hand on his chin and scratched the underside of his jaw. "But I see... a merc, huh? I don't see why you'd come all the way to Londinium for that, seeing as Jizzaro is the prime place for your lot... but I won't pry." 

The man's aging eyes then turned to the middle distance. 

"I'm sure there's a long story behind it. Every merc has one." As he said this, he turned his gaze back at the dark-skinned man. "But at the very least, even these old eyes can see - you must care about that girl, don't you?"

/Care.../

He lowered his head in contemplation. 

/I... suppose so./

Affirming it within himself, he finally nodded. 

"Mm," he said, "That girl... is family."

The old man smiled and placed the brooch in the mercenary's hand. "Then give it to her."

"...I will."

#### ZATSU #####

The sun had long set, casting the shadow of night over the forests of Northmatch.

"Shit! Shit! what's going on!?" He panted

"I knew it...! This job was too good to be true!"

"I gotta get out of here! My mom is-"

From out of the darkness, a strangely high pitched voice called out to him.

"Are you a bandit, meow?"

"Wha-!? Who's there!?"

"Are you a bandit, meow?" The voice simply repeated.

/Shit shit shiiiiiit! The rumours...! They're actually true!?/

"Nya... I said, are you a bandit, meow?"

From out of the darkness, a pair of glowing eyes appeared, almost floating.

"Eeeeeek!" He threw away his weapons. "No! Please! I'm not a bandit! I quit! I'm going to make an honest living from now on!"

"Oh... okay, meow."

The eyes seemed to almost float, and as the thing walked by, he saw an incredibly faint silhouette of an upright cat surrounding the eyes. But more than that, he noticed that in its 'hand', it held a jet-black metal - a blade of some sort, and it dripped a thick, crimson liquid dripped down from it.

/Eeeek! It can't be...!/

The creature soon disappeared into the darkness.

But in its wake, it left a trail of blood.

His heart pounded.

Every instinct in his body told him to run, but somehow, a macabre curiosity nagged him forward.

He stood back up (he fell on his ass earlier).

He followed the trail deeper into the forest, and it ended behind a dense shrub, whose tiny leaves were almost washed over with blood.

He swallowed his breath.

A cold sweat ran all over his body.

He mustered all of his courage, pushing himself forward to peek what was behind the bush.

And then...

...he turned around.

/No.../ He snorted. /I don't need to know./

With snot dripping down his nose, resolved in himself.

/Mom... please wait for me. I'm going to hold a real job from now on...!/

After a few days, the bandit attacks around Dellwick had almost come to an end. A sense of normalcy and peace returned to its once uneasy streets, and trade wagons once more rolled freely across its central road.

At the same time, the rumour of ghost roaming the forests of Northmarch spread far and wide, and, partly because of the subject matter, not only bandits, but even hunters and lumberjacks started to avoid going there altogether.