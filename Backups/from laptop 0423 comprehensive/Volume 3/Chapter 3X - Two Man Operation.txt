Chapter 3X - Two Man Operation

CHANGE:
- The King already knows about the Mythril being sold and he just wants to scout out what the exact situation in the Cave.

The King will receive reports of Mythril products being sold in major cities, outside of the Capital. Reports in Brandonbury, Ealdton.

Plan 1: King's PoV, get report, react, send agent.
Plan 2: Agent's PoV, assignment already received from the King, ambush Dante's caravan.

Dante's caravan is large and well protected, part of the caravan trains emcompassing several merchants. That way, high protection is afforded for a fraction of the cost. To ambush the caravan and catch Dante with incriminating evidence, an accident is made to happen, splitting the caravan at a river crossing.

The old lady (not really old lady) joins the caravan as a guard while a teammate shadows her. At the set time and place, an accident is made to happen, unsettling the baggage. Dante is extra careful, so the lady needs to exert herself just a bit to uncover the hidden Mythril. 

(Aster's team has connections to the security procurement of the merchants guild)

When the Mythril is uncovered, they confront Dante and force him to leak his supplier. He refuses at first, but quickly crumbles once the daggers are drawn. He reveals Dellwick as his source, and after a bit more convincing, tattles on Sylphia as his supplier. They get angry at first, thinking he's lying (she's the heroine after all), but after promising him he'll pay if he was lying, they leave him alone.

REPERCUSSIONS:

Chapter change: add dialogue in C29 about her mentioning mythril.

Sylphia's ending talk with the King will involve her getting cornered into admitting she found LOTS of Mythril in the cave.



=== THINK ABOUT WHY ===



Before this, add -ANOTHER- chapter with a calm talk between him and Alphonso about what to do about Sylphia's implicit betrayal. The King jokingly mulls her execution.





